## Before run the test live:

- [ ] Update Selection ID's, in src/test/resources/data/Selections.csv and event ID for Page navigation in src/test/resources/data/Events.csv (git push)

- [ ] Start Jenkins 54.72.121.117, Grafana 52.17.29.227, InfluxDB 52.48.207.126. And for deposits the PaymentStubNew 52.50.168.86. Also required number of job runners, the AWS injectors named SportsLiveGatling

- [ ] Set logging logback from <root level="OFF"> to <root level="INFO"> (git push)

- [ ] Run smoke tests in Jenkins and check logs for successful login and betplacements (example: 06:33:23.656 [INFO ] Bet receipt: IBSSLIP=R|{} 0 -1 0 2729300898 O/17565159/0007681/F For Account: NFTTESTCPSPQM63 - logging info)

- [ ] Change back logging to <root level="OFF"> (git push)

- [ ] Inform Live Load chat and wait green light to start test from team.

#### to run Simulation, add Maven job --> clean install -DsimulationClass=MobileBetPlacement 


## Bet Placement

- pass the system property `betType` to either `single`, `double` or `treble` if nothing is set then `single` is the default

for doubles and trebles the data is fed from `multi-event-selections.csv`

In the current state the selections are only used once per test; 
 - for doubles it requires 2 selections from different events
 - for trebles it requires 3 selections from different events
