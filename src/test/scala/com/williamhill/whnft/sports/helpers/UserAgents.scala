package com.williamhill.whnft.sports.helpers

import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder

/**
 * Created by Juri Boiko on 08/03/2017.
 */
object UserAgents extends Simulation  {

  val injectorPrivateIP = sys.env.get("SSH_CONNECTION")

  def setInjector(UAString: String): String = {
    if(injectorPrivateIP.isDefined) UAString + " wh-injector/" + injectorPrivateIP.get else UAString
  }

  def setAgentFeedNative (session : Session) : Session = {
    val userAgent = session("UAStrings").toString()
    var userAgentCode : String = ""

    if (userAgent.contains("iPhone")) {
      userAgentCode = "Mozilla/5.0 (iPhone; CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko)"
    } else {
      if (userAgent.contains("iPad")) {
        userAgentCode = "Mozilla/5.0 (iPad; CPU OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko)"
      } else {
        if (userAgent.contains("tab")) {
          userAgentCode = "Mozilla/5.0 (Linux; Android 9; SM-T290) AppleWebKit/537.36 (KHTML, like Gecko)"
        } else {
          userAgentCode = "Mozilla/5.0 (Linux; Android 9; SAMSUNG SM-A563F) AppleWebKit/537.36 (KHTML, like Gecko)"
        }
      }
    }
    session.setAll(Map(
      "userAgentCode" -> setInjector(userAgentCode))
    )
  }

  def setAgentFeedMobileWeb (session : Session) : Session = {
    val userAgent = session("UAStrings").toString()
    var userAgentCode : String = ""

    if (userAgent.contains("iPhone")) {
      userAgentCode = "Mozilla/5.0 (iPhone; CPU iPhone OS 12_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/77.0.3865.93 Mobile/15E148 Safari/605.1"
    } else {
      if (userAgent.contains("iPad")) {
        userAgentCode = "Mozilla/5.0 (iPad; CPU OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1 Mobile/15E148 Safari/604.1"
      } else {
        if (userAgent.contains("tab")) {
          userAgentCode = "Mozilla/5.0 (Linux; Android 9; SM-T290) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.89 Safari/537.36"
        } else {
          userAgentCode = "Mozilla/5.0 (Linux; Android 9; SAMSUNG SM-A563F) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/10.0 Chrome/71.0.3578.99 Mobile Safari/537.36"
        }
      }
    }
    session.setAll(Map(
      "userAgentCode" -> setInjector(userAgentCode))
    )
  }

  def setAgentFeedDesktop (session : Session) : Session = {
    val userAgent = session("UAStrings").toString()
    var userAgentCode : String = ""

    if (userAgent.contains("Mac")) {
      userAgentCode = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Safari/605.1.15"
    } else {
      if (userAgent.contains("PC")) {
        userAgentCode = "Mozilla/5.0 (Windows NT 10.0: Win64: x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36"
      }
    }
    session.setAll(Map(
      "userAgentCode" -> setInjector(userAgentCode))
    )
  }

}
