package com.williamhill.whnft.sports.helpers

import com.typesafe.config.ConfigFactory
import com.williamhill.whnft.sports.requestTrafagarObjects.BetSlip.BetType
import com.williamhill.whnft.sports.simulations.SimulationSettingsTrait
import io.gatling.core.session.Session

object Common extends SimulationSettingsTrait {

  def isEmpty(x: String) : Boolean = x != null && x.trim.nonEmpty

  def getConfigFromFile(configFileName: String, configName: String) : String =
  {
    val conf = ConfigFactory.load(configFileName)
    conf.getString("properties." + environmentUnderTest + "."+configName)
  }

  def prop(key : String) : Option[String] = {
    if (sys.env.contains(key))
      Some(sys.env(key))
    else None
  }

  def prop[T](key : String, defaultValue:  T) : T = {
    if (sys.env.contains(key)) {
      defaultValue match {
        case defaultValue: Int => sys.env(key).toInt.asInstanceOf[T]
        case defaultValue: String => sys.env(key).toString.asInstanceOf[T]
        case defaultValue: Boolean => sys.env(key).toBoolean.asInstanceOf[T]
      }
    }
    else
      defaultValue
  }

  def userLoggedIn(session: Session): Boolean =
    (session.contains("custLogin") && !session("custLogin").as[String].isEmpty
      && session.contains("sslLogin") && !session("sslLogin").as[String].isEmpty
      && session.contains("custAuth") && !session("custAuth").as[String].isEmpty)

  def getBetType(): BetType.Value = {
    sys.env.get("betType") match {
      case Some("double") => BetType.Double
      case Some("treble") => BetType.Treble
      case _ => BetType.Single
    }
  }

  def isInPlay(): Boolean = sys.env("inPlay").equals("true")

}

