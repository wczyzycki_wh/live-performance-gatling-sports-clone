package com.williamhill.whnft.sports.requestTrafagarObjects

import com.typesafe.config.ConfigFactory
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import com.williamhill.whnft.sports.helpers.Common

/**
  * Update by Juri Boiko on 21/12/2018.
  */
object Logout {
  
  val headers = Map(
    "Accept-Encoding" -> "gzip, deflate",
    "Origin" -> "https://sports.williamhill.com",
     "User-Agent" -> "${userAgentCode}")

  val securebaseURL : String = Common.getConfigFromFile("environment.conf", "securebaseURL")
  val authURL : String = Common.getConfigFromFile("environment.conf", "authURL")

  def logout =
    doIf(session => userLoggedIn(session)) {
      exec(http("GET /bet?action=DoLogout - ${platform}")
      .get(securebaseURL + "/bet/en-gb")
      .queryParam("action", "DoLogout")
      .queryParam("target_page", securebaseURL + "/bet/en-gb")
      .headers(headers)
      .disableFollowRedirect
      .check(status.is(301)))
      .pause(1, 5)
      .exec(http("GET /cas/logout - ${platform}")
      .get(authURL + "/cas/logout")
      .queryParam("service", securebaseURL + "/bet/en-gb")
      .disableFollowRedirect
      .check(status.is(302)))
      .pause(1, 5)
    }

  def userLoggedIn (session: Session) : Boolean =
    session.contains("custLogin") && !session("custLogin").as[String].isEmpty
}    