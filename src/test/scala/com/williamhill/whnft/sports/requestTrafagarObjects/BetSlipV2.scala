package com.williamhill.whnft.sports.requestTrafagarObjects

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.requestTrafagarObjects.BetSlip.BetType
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

object BetSlipV2 {

  val transactHost: String = Common.getConfigFromFile("environment.conf", "transactHost")
  val betSlipUrl = "/betslip/api/bets"
  val legsJmesPath = "bets[?betType == '[betType]'].legs[*].{legSort: legSort, selectionIds: selectionIds}|[count]"
  val selectionsJmesPath = "selections[].{selectionIds: selectionIds, price: prices[0]}"

  val headersBetSlip = Map(
    "Accept-Encoding" -> "br, gzip, deflate",
    "Origin" -> "https://sports.williamhill.com",
    "Accept" -> "application/json",
    "Content-Type" -> "application/json",
    "User-Agent" -> "${userAgentCode}"
  )

  def makeBet(betType: BetType.Value): ChainBuilder =
      exec(
        addToBetSlipV2(betType),
        placeBetV2(betType)
      )

  def addToBetSlipV2(betType: BetType.Value): ChainBuilder = {
    exec(addCookie(Cookie("cust_login", "${custLogin}")))
      .exec(addCookie(Cookie("cust_ssl_login", "${sslLogin}")))
      .exec(addCookie(Cookie("cust_auth", "${custAuth}")))
      .exec(addCookie(Cookie("CSRF_COOKIE", "${csrf_token}")))
      .exec(http("Add to bet slip V2 - ${device}")
        .post(transactHost + betSlipUrl + "/buildSlip")
        .queryParam("lang", "en")
        .body(ElFileBody("addToBetSlip.json")).asJson
        .headers(headersBetSlip)
        .check(status.is(200))
        .check(header("x-csrf-token").saveAs("token"))
        .check(jsonPath("$.betslipUid").saveAs("betslipUid"))
        .check(jsonPath("$.bets[0].betUid").saveAs("betUid"))
        .check(jmesPath("bets[?betType == '" + betType.toString.toUpperCase + "']").not("[]"))
        .check(jmesPath(legsPath(betType)).saveAs("legs"))
        .check(jmesPath(selectionsPath(betType)).saveAs("selections"))
      ).exitHereIfFailed
      .pause(1, 2)
  }

  def placeBetV2(betType: BetType.Value): ChainBuilder = {
    exec(addCookie(Cookie("CSRF_COOKIE", "${csrf_token}")))
      .exec(addCookie(Cookie("SESSION", "${session}")))
      .exec(addCookie(Cookie("cust_login", "${custLogin}")))
      .exec(addCookie(Cookie("cust_auth", "${custAuth}")))
      .exec(addCookie(Cookie("cust_ssl_login", "${sslLogin}")))
      .exec(
        http("Place bet v2 - ${device}")
          .post(transactHost + betSlipUrl)
          .header("content-type", "application/json;charset=UTF-8")
          .header("x-csrf-token", "${token}")
          .header("authority", "transact.williamhill.com")
          .header("accept-encoding", "gzip, deflate, br")
          .header("scheme", "https")
          .header("Accept", "application/json")
          .queryParam("lang", "en")
          .body(ElFileBody("placeBet.json")).asJson
          .check(status.is(200))
          .check(jsonPath("$.status").exists.saveAs("status"))
          .check(jsonPath("$.betDelay.delay").saveAs("birDelay"))
          .check(jsonPath("$.birReqId").optional.saveAs("birReqId"))
      ).doIf(session => session.attributes("status").equals("delay")) {
      pause("${birDelay}")
        .exec(confirmInPlayBet)
    }
  }

  def confirmInPlayBet: ChainBuilder = {
    exec(
      http("Get Bet Delay status")
        .post(transactHost + betSlipUrl + "/getBetDelayStatus")
        .header("Content-Type", "application/json")
        .header("x-csrf-token", "${token}")
        .header("authority", "transact.williamhill.com")
        .header("scheme", "https")
        .header("Accept", "application/json")
        .header("sec-fetch-site", "same-site")
        .header("sec-fetch-mode", "cors")
        .header("user-agent", "${userAgentCode}")
        .queryParam("lang", "en")
        .body(StringBody(
          """
            |{"birReqId": "${birReqId}"}
            |""".stripMargin)).asJson
        .check(status.is(200))
        .check(jsonPath("$.status").is("confirmed"))
    )
  }

  def legsPath(betType: BetType.Value): String =
    betType match {
      case BetType.Single => legsJmesPath.replace("[betType]", betType.toString.toUpperCase).replace("|[count]", "|[0]")
      case BetType.Double => legsJmesPath.replace("[betType]", betType.toString.toUpperCase).replace("|[count]", "[]|[0:2]")
      case BetType.Treble => legsJmesPath.replace("[betType]", betType.toString.toUpperCase).replace("|[count]", "[]|[0:3]")
    }

  def selectionsPath(betType: BetType.Value): String =
    betType match {
      case BetType.Single => selectionsJmesPath.replace("selections[]", "selections[0:1]")
      case BetType.Double => selectionsJmesPath.replace("selections[]", "selections[0:2]")
      case BetType.Treble => selectionsJmesPath.replace("selections[]", "selections[0:3]")
    }

}
