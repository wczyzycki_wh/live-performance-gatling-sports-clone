package com.williamhill.whnft.sports.requestTrafagarObjects

import com.williamhill.whnft.sports.helpers.Common
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._
import org.slf4j.LoggerFactory

object BetSlip {

  val securebaseURL: String = Common.getConfigFromFile("environment.conf", "securebaseURL")
  val myaccountURL: String = Common.getConfigFromFile("environment.conf", "myaccountURL")
  val betAmount = Common.getConfigFromFile("environment.conf", "betAmount")

  val headersEventspartial = Map(
    "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")

  val headersBetSlip = Map(
    "Accept-Encoding" -> "gzip, deflate",
    "Origin" -> "https://sports.williamhill.com",
    "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,q=0.8",
    "Access-Control-Allow-Headers" -> "X-WH-Wrapped,User-Agent",
    "Content-Type" -> "application/x-www-form-urlencoded; charset=UTF-8",
    "User-Agent" -> "${userAgentCode}")

  val headersStatements = Map(
    "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "Accept-Encoding" -> "gzip, deflate, br",
    "User-Agent" -> "${userAgentCode}")

  val headersOpenBetStatement = Map(
    "Accept" -> "application/json, text/plain, */*",
    "Accept-Encoding" -> "gzip, deflate, br",
    "User-Agent" -> "${userAgentCode}")

  val headersBetSlipRender = Map(
    "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
    "Accept-Encoding" -> "gzip, deflate, br",
    "User-Agent" -> "${userAgentCode}")

  def getUrl(url: String, eventID: String, sport: String) =
    url.replace("${sportName}", sport).replace("${eventID}", eventID)

    def placeBet(betUrl: String, betType: BetType.Value, numberOfBets: Int): ChainBuilder = {
      repeat(numberOfBets) {
        exec(session => {
          if (!session.contains("csrf_token"))
            session.set("csrf_token", "empty")
          else session
        })
        exec(flushSessionCookies)
          .doIf(BetType.Single.equals(betType)) {
            exec(
              addToBetSlip(betUrl),
              actualBetPlacement(betUrl, betType)
            )
          }.doIf(BetType.Double.equals(betType)) {
          exec(
            addToBetSlip(betUrl, 1),
            addToBetSlip(betUrl, 2),
            actualBetPlacement(betUrl, betType)
          )
        }.doIf(BetType.Treble.equals(betType))
        {
          exec(
            addToBetSlip(betUrl, 1),
            addToBetSlip(betUrl, 2),
            addToBetSlip(betUrl, 3),
            actualBetPlacement(betUrl, betType)
          )
        }
      }
    }

  def addToBetSlip(betURL: String): ChainBuilder = addToBetSlip(betURL, "")

  def addToBetSlip(betURL: String, n: Any): ChainBuilder = {
    exec(addCookie(Cookie("cust_login", "${custLogin}").withDomain(".williamhill.com")))
      .exec(addCookie(Cookie("cust_ssl_login", "${sslLogin}").withDomain(".williamhill.com")))
      .exec(addCookie(Cookie("cust_auth", "${custAuth}").withDomain(".williamhill.com")))
      .exec(addCookie(Cookie("CSRF_COOKIE", "${csrf_token}").withDomain(".williamhill.com")))
      .exec(http("Add to bet slip") // https://sports.williamhill.com/slp/en-gb
        .post(securebaseURL + "/" + betURL + "/en-gb")
        .formParam("action", "GoAddLegSSL")
        .formParam("leg_sort", "--")
        .formParam("price_type", "L")
        .formParam("lp_num", "${Num" + n + "}")
        .formParam("lp_den", "${Denom" + n + "}")
        .formParam("hcap_value", "0")
        .formParam("bir_index", "")
        .formParam("ew_places", "")
        .formParam("ew_fac_num", "")
        .formParam("ew_fac_den", "")
        .formParam("ev_oc_id", "${SelectionID" + n + "}")
        .formParam("combi_sel", "Y")
        .formParam("blockbuster_id", "-1")
        .formParam("switch_tab", "1")
        .formParam("aff_id", "${betCode}")
        .formParam("csrf_token", "${csrf_token}")
        .headers(headersBetSlip)
        .check(status.is(200))
        .check(regex("\"bet_uid\": \"(.*?)\"").saveAs("betUid"))
        .check(headerRegex("Set-Cookie", "CSRF_COOKIE=(.*); path=/").saveAs("csrf_token"))
        .check(headerRegex("Set-Cookie", "IBSBET=(.*); path=/").saveAs("ibsbet"))
        .check(headerRegex("Set-Cookie", "IBSLEG=(.*); path=/").saveAs("ibsleg"))
      .check(headerRegex("Set-Cookie", "IBSGRP=(.*); path=/").saveAs("ibsgrp"))
      .check(headerRegex("Set-Cookie", "IBSSLIP=(.*); path=/").saveAs("ibsslip")))
      .pause(1, 5)
  }

  def actualBetPlacement(betURL: String, betType: BetType.Value): ChainBuilder = {
    val inplay : Boolean = sys.env.get("inPlay") match{
      case Some("true") => true
      case _ => false
    }
    exec(flushSessionCookies)
      .exec(addCookie(Cookie("CSRF_COOKIE", "${csrf_token}").withDomain(".williamhill.com")))
      .exec(addCookie(Cookie("cust_login", "${custLogin}").withDomain(".williamhill.com")))
      .exec(addCookie(Cookie("cust_auth", "${custAuth}").withDomain(".williamhill.com")))
      .exec(addCookie(Cookie("IBSSLIP", "${ibsslip}").withDomain(".williamhill.com")))
      .exec(addCookie(Cookie("cust_ssl_login", "${sslLogin}").withDomain(".williamhill.com")))
      .exec(addCookie(Cookie("IBSLEG", "${ibsleg}").withDomain(".williamhill.com")))
      .exec(addCookie(Cookie("IBSGRP", "${ibsgrp}").withDomain(".williamhill.com")))
      .exec(addCookie(Cookie("IBSBET", session => buildIbsSlip((session("ibsbet")).as[String], betType)).withDomain(".williamhill.com")))
      .exec(http("Place bet " + betType.toString)
        .post(securebaseURL + "/" + betURL + "/en-gb")
        .formParam("action", "DoPlaceBet")
        .formParam("dont_show_again", "")
        .formParam("uid", "${betUid}")
        .formParam("csrf_token", "${csrf_token}")
        .formParam("blockbuster_id", "-1")
        .formParam("target_page", securebaseURL + "/" + betURL + "/en-gb?action=PlayIfrSlipSSL")
        .disableFollowRedirect
        .headers(headersBetSlip)
        .check(status.is(301))
        .check(checkIf(!inplay){headerRegex("Set-Cookie", "IBSSLIP=.*O\\/\\d+\\/\\d+\\/\\w").saveAs("receipt")})
        .check(headerRegex("Set-Cookie", "CSRF_COOKIE=(.*); path=/").saveAs("csrf_token"))
      )
      .doIf(inplay)(
        pause(3, 5)
          .exec(session => {
//            session.set(
//              "receipt",
//              "IBSSLIP=.*O\\/\\d+\\/\\d+\\/\\w".r.findFirstMatchIn(
//                Common.getCookieValue("IBSSLIP", "http://.williamhill.com", session)
//              )
//            )
            session
          })
      )
      .pause(1, 5)
      .exec(session => {
        LoggerFactory.getLogger("Execute bet against selection: " + session("SelectionID").as[String]).info("logging info")
        LoggerFactory.getLogger("Bet receipt: " + session("receipt").as[String] + " For Account: " + session("Username").as[String]).info("logging info")
        session
      })
  }

  def render(endpointUrl: String): ChainBuilder =
  {
    exec(
      http("BetSlip Rendering - " + "${platform}")
        .get(endpointUrl)
        .headers(headersBetSlipRender)
        .check(status in (200, 304))
    )
  }

  object BetType extends Enumeration {
    val Single, Double, Treble = Value
  }

  def buildIbsSlip(ibsBet: String, betType: BetType.Value): String = betType match {
    case BetType.Single => ibsBet.replace("0|0|SGL|1|W|0|", "0|0|SGL|1|W|" + betAmount + "|")
    case BetType.Double => ibsBet.replace("|DBL|1|W|0|", "|DBL|1|W|" + betAmount + "|")
    case BetType.Treble => ibsBet.replace("|TBL|1|W|0|", "|TBL|1|W|" + betAmount + "|")
  }

}
