package com.williamhill.whnft.sports.requestTrafagarObjects

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.requestTrafagarObjects.BetSlip.{BetType, actualBetPlacement, addToBetSlip}
import io.gatling.core.Predef._
import io.gatling.http.Predef._

/**
  * Update by Juri Boiko on 21/12/2018.
  */
object BetSlipCashIn {

  val securebaseURL: String = Common.getConfigFromFile("environment.conf", "securebaseURL")
  val doBetPlacement : Boolean = sys.env.get("placeBet") match{
    case Some("false") => false
    case _ => true
  }


  def placeBet(betURL: String, numberOfBets: Int, ajaxURL: String) =
    repeat(numberOfBets) {
      exec(session => {
        if (!session.contains("csrf_token"))
          session.set("csrf_token", "empty")
        else session
      })
        .exec(flushSessionCookies)
        .exec(flushHttpCache)
        .exec(flushCookieJar)
        .exec(
          addToBetSlip(betURL),
          actualBetPlacement(betURL, BetType.Single),
          loopAjaxGetBet(3, ajaxURL),
          getAjaxOpenOtherBets(ajaxURL),
          cashoutPlacedBet(betURL))
    }


  def loopAjaxGetBet(times: Int, ajaxURL: String) =
    repeat(times)(
        exec(getAjaxOpenBets(ajaxURL))
    )


 def getAjaxOpenBets (ajaxURL: String) =
   tryMax(3) {
     exec(flushSessionCookies)
       .exec(addCookie(Cookie("cust_login", "${custLogin}").withDomain(".williamhill.com")))
       .exec(addCookie(Cookie("cust_auth", "${custAuth}").withDomain(".williamhill.com")))
       .exec(addCookie(Cookie("cust_ssl_login", "${sslLogin}").withDomain(".williamhill.com")))
       .exec(addCookie(Cookie("CSRF_COOKIE", "${csrf_token}").withDomain(".williamhill.com")))
       .exec(http("Ajax Open bets")
         .post(securebaseURL + "/" + ajaxURL + "/en-gb")
         .formParam("action", "GoOpenBetsSSL")
         .formParam("blockbuster_id", "-1")
         .formParam("csrf_token", "${csrf_token}")
         .check(status.is(200))
         .check(checkIf(doBetPlacement){regex("\"bet_id\": \"(.*?)\"").saveAs("bet_id")})
         .check(checkIf(doBetPlacement){regex("\"cr_date\": \"(.*?)\"").saveAs("cr_date")})
         .check(checkIf(doBetPlacement){regex("\"cashout_value\": \"(.*?)\"").saveAs("cashoutValue")})
         .check(regex("\"num_open_bets\": \"(.*?)\"").saveAs("open_bets_num_bets"))
       )
       .pause(1, 3)
   }

     def getAjaxOpenOtherBets (ajaxURL: String) =
       exec(http("Ajax Open bets")
         .post(securebaseURL + "/" + ajaxURL + "/en-gb")
         .formParam("action", "GoOpenBets")
         .formParam("blockbuster_id", "-1")
         .formParam("csrf_token", "${csrf_token}")
         .formParam("cr_date", if (doBetPlacement) "${cr_date}" else "")
         .formParam("bet_id", if (doBetPlacement) "${bet_id}" else "")
         .check(status.is(200))
         )
       .exec(http("Ajax Open bets")
         .post(securebaseURL + "/" + ajaxURL + "/en-gb")
         .formParam("action", "GoRefreshOBets")
         .formParam("blockbuster_id", "-1")
         .formParam("csrf_token", "${csrf_token}")
         .formParam("open_bets_num_bets", "${open_bets_num_bets}")
         .formParam("cashout_accept", "-1")
         .check(status.is(200))
         )
       .exec(http("Ajax Open bets")
         .post(securebaseURL + "/" + ajaxURL + "/en-gb")
         .formParam("action", "GoOpenBets")
         .formParam("blockbuster_id", "-1")
         .formParam("csrf_token", "${csrf_token}")
         .formParam("open_bets_num_bets", "${open_bets_num_bets}")
         .formParam("cashout_accept", "-1")
         .check(status.is(200))
         .check(headerRegex("Set-Cookie", "CSRF_COOKIE=([^;]+)").exists.saveAs("csrf_token"))
       )
     .pause(1, 5)

 def cashoutPlacedBet(betURL: String) =
   tryMax(3) {
     exec(flushSessionCookies)
       .exec(addCookie(Cookie("cust_login", "${custLogin}").withDomain(".williamhill.com")))
       .exec(addCookie(Cookie("cust_ssl_login", "${sslLogin}").withDomain(".williamhill.com")))
       .exec(addCookie(Cookie("cust_auth", "${custAuth}").withDomain(".williamhill.com")))
       .exec(addCookie(Cookie("CSRF_COOKIE", "${csrf_token}").withDomain(".williamhill.com")))
       .exec(http("Cashout Placed Bet")
         .post(securebaseURL + "/" + betURL + "/en-gb")
         .formParam("action", "DoCashOutBet")
         .formParam("bet_id", "${bet_id}")
         .formParam("cashout_price", "${cashoutValue}")
         .formParam("target_page", securebaseURL + "/" + betURL + "/en-gb?action=PlayIfrOpenBetsSSL")
         .formParam("csrf_token", "${csrf_token}")
         .formParam("partialCashout", "")
         .disableFollowRedirect
         .check(status.in(200, 301))
         .check(bodyString.saveAs("BODY"))
         .check(headerRegex("Set-Cookie", "CSRF_COOKIE=([^;]+)").exists.saveAs("csrf_token"))
       )
   }
     .pause(1, 5)


}