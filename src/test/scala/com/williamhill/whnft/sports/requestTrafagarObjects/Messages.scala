package com.williamhill.whnft.sports.requestTrafagarObjects

import com.williamhill.whnft.sports.requestTrafagarObjects.Login.securebaseURL
import io.gatling.core.structure.ChainBuilder
import io.gatling.core.Predef._
import io.gatling.http.Predef._

object Messages {

  val inboxHeaders = Map(
    HttpHeaderNames.AcceptEncoding -> "gzip, deflate",
    HttpHeaderNames.Origin -> securebaseURL,
    HttpHeaderNames.UserAgent -> "${UAStrings}",
    HttpHeaderNames.Accept -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
    HttpHeaderNames.AcceptLanguage -> "en-GB,en-US;q=0.9,en;q=0.8"
  )

  def goToInbox : ChainBuilder =
    exec(addCookie(Cookie("cust_login", "${custLogin}")))
    .exec(addCookie(Cookie("cust_ssl_login", "${sslLogin}")))
    .exec(addCookie(Cookie("CSRF_COOKIE", "${csrf_token}")))
    .exec(http("View Messages - Desktop")
        .get(securebaseURL + "/acc/en-gb?action=GoInbox&pm=POSTMESSAGE")
        .headers(inboxHeaders)
        .check(status is 200)
        //Status is always 200, even if user is not logged in, so added extra check to ensure the user is in the 'Messages' landing page
        //If this step fails, it may mean the user actually has a message!
    )


}
