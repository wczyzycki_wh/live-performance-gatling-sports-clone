package com.williamhill.whnft.sports.requestTrafagarObjects

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.helpers.RandomGen
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

import scala.util.Random

/**
 * Update by Juri Boiko on 21/12/2018.
 */
object SearchFunctionality {

  val baseUri: String = Common.getConfigFromFile("environment.conf", "baseURL")
  val sportsBookSearchbaseUri: String = Common.getConfigFromFile("environment.conf", "sportsBookSearchURL")
  val baseSportUri: String = baseUri + "/betting/en-gb"
  val headersAll = Map(
    "Accept" -> "*/*",
    "Accept-Encoding" -> "gzip, deflate, sdch",
    "Accept-Language" -> "en-GB,en-US;q=0.8,en;q=0.6,it;q=0.4",
    "Connection" -> "keep-alive",
    "Origin" -> baseUri,
    "Referer" -> baseSportUri,
    "User-Agent" -> "${userAgentCode}"
  )

  val searchUrl: String = Common.getConfigFromFile("environment.conf", "searchURL")
  val top10Searches : Array[String] =
    Array(
        "None",
        "Big brother",
        "Ufc",
        "Olympics",
        "Premier league",
        "Big",
        "Irish",
        "Irish lottery",
        "premier",
        "Lottery"
    )

//  def top10QuerySearch = Random.shuffle(top10Searches.toList).head
  def top10QuerySearch = top10Searches(0)
  def randomQuerySearch = RandomGen.getRandomAlphaNumerics(10)

  def makeRealisticSearch =
    exec(session => {
      session.set("searchUrl", searchUrl.replace("[searchTerm]", top10QuerySearch))
    })
    .exec(http("Top 10 Searches - ${platform}")
    .get("${searchUrl}")
    .headers(headersAll)
    .check(status.is(200)))

  def generateCacheMisses =
    exec(session => {
      session.set("searchUrl", searchUrl.replace("[searchTerm]", randomQuerySearch))
    })
    .exec(http("Random cache miss search - ${platform}")
    .get("${searchUrl}")
    .headers(headersAll)
    .check(status.is(200)))

  def sportsBookSearch(query : String = "horse") : ChainBuilder =
    exec(http(s"Search Sports Book for : '$query'" )
        .get(sportsBookSearchbaseUri + s"?query=$query")
        .check(status is 200)
        .check(jsonPath("$..results[*]").exists)
    )
}