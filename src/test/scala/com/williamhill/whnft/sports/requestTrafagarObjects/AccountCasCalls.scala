package com.williamhill.whnft.sports.requestTrafagarObjects

import com.williamhill.whnft.sports.helpers.Common
import io.gatling.core.Predef._
import io.gatling.core.body.StringBody
import io.gatling.http.Predef._
import org.slf4j.LoggerFactory

/**
  * Update by Juri Boiko on 21/12/2018.
  */
object AccountCasCalls {

  val authURL : String =Common.getConfigFromFile("environment.conf", "authURL")
  val baseURL : String =Common.getConfigFromFile("environment.conf", "baseURL")
  val securebaseURL : String =Common.getConfigFromFile("environment.conf", "securebaseURL")
  val myaccountURL : String =Common.getConfigFromFile("environment.conf", "myaccountURL")
  val streamingHost : String =Common.getConfigFromFile("environment.conf", "streamingHost")
  val transactHost : String =Common.getConfigFromFile("environment.conf", "transactHost")

  val headersCasJSON = Map(
    "Accept-Encoding" -> "gzip, deflate",
    "Accept" -> "application/json",
    "Accept-Language" -> "en-US,en;q=0.9,et;q=0.8,ru;q=0.7,es;q=0.6",
    "Origin" -> baseURL,
    "User-Agent" -> "${userAgentCode}")

  val headersCas = Map(
    "Accept-Encoding" -> "gzip, deflate",
    "Accept" -> "*/*",
    "Accept-Language" -> "en-US,en;q=0.9,et;q=0.8,ru;q=0.7,es;q=0.6",
    "Origin" -> baseURL,
    "User-Agent" -> "${userAgentCode}")


  def streamingCasCall =
    exec(session => {
      LoggerFactory.getLogger("Account amount balance: " + session("custLogin").as[String]).info("logging info")
      LoggerFactory.getLogger("Account amount balance: " + session("sslLogin").as[String]).info("logging info")
      LoggerFactory.getLogger("Account amount balance: " + session("custAuth").as[String]).info("logging info")
      LoggerFactory.getLogger("Account amount balance: " + session("csrf_token").as[String]).info("logging info")
      session
    })
      .exec(addCookie(Cookie("cust_login", "${custLogin}")))
      .exec(addCookie(Cookie("cust_ssl_login", "${sslLogin}")))
      .exec(addCookie(Cookie("cust_auth", "${custAuth}")))
      .exec(addCookie(Cookie("CSRF_COOKIE", "${csrf_token}")))
    .exec(http("streaming Cas Call")
    .get(authURL + "/cas/login")
      .headers(headersCas)
      .queryParam("cust_login", true)
      .queryParam("service", streamingHost + "/login/cas?target=%2Fapi%2Fstreams%2Fevents%3FresponseFormat%3DIDS")
      .queryParam("gateway", true)
        .check(status.is(302))
    )

  def casAoaOrchestrate =
     // exec(flushSessionCookies)
      exec(addCookie(Cookie("cust_login", "${custLogin}")))
      .exec(addCookie(Cookie("cust_ssl_login", "${sslLogin}")))
      .exec(addCookie(Cookie("cust_auth", "${custAuth}")))
      .exec(addCookie(Cookie("CSRF_COOKIE", "${csrf_token}")))
    .exec(http("Cas aoa orchestrate")
      .get(authURL + "/cas/login")
      .headers(headersCasJSON)
      .queryParam("cust_login", true)
      .queryParam("service", myaccountURL + "/aoa/orchestrate")
      .check(status.is(200))
      .check(jsonPath("$.serviceTicket").saveAs("serviceTicket"))
      .check(jsonPath("$.next").saveAs("next"))
    )
      .exec(http("CAS myaccount aoa OPTIONS")
        .options("${next}")
        .headers(headersCas)
          .check(status.is(200))
      )
  exec(flushSessionCookies)
      .exec(http("CAS myaccount aoa POST")
        .post("${next}")
        .body(StringBody("{\"locale\":\"en-gb\",\"services\":[\"pluscard\"]}")).asJson
        .headers(headersCas)
        .check(status.is(200))
      )


  def casClientCallback =

   //   .exec(flushSessionCookies)
      exec(addCookie(Cookie("cust_login", "${custLogin}")))
      .exec(addCookie(Cookie("cust_ssl_login", "${sslLogin}")))
      .exec(addCookie(Cookie("cust_auth", "${custAuth}")))
      .exec(addCookie(Cookie("CSRF_COOKIE", "${csrf_token}"))) //https://sports.williamhill.com/bet/en-gb?action=GetSslXLoginForm&LastLogin=1
    .exec(http("Cas Client callback")
      .get(authURL + "/cas/login")
      .headers(headersCas)
      .queryParam("cust_login", true)
      .queryParam("service", myaccountURL + "/callback?client_name=CasClient")
      .check(status.is(302))
      .check(headerRegex("location", "(.*)").saveAs("location"))
    )
      .exec(http("Cas Client location")
        .get("${location}")
        .check(status.is(302))
      )

  def transactCasCall =
    exec(session => {
      if (!session.contains("csrf_token"))
        session.set("csrf_token", "empty")
      else session
    })
      .exec(flushSessionCookies)
      .exec(addCookie(Cookie("cust_login", "${custLogin}")))
      .exec(addCookie(Cookie("cust_ssl_login", "${sslLogin}")))
      .exec(addCookie(Cookie("cust_auth", "${custAuth}")))
      .exec(addCookie(Cookie("CSRF_COOKIE", "${csrf_token}")))
    .exec(http("CAS transact betslip Call")
      .get(authURL + "/cas/login")
      .headers(headersCas)
      .queryParam("cust_login", true)
      .queryParam("service", transactHost + "/betslip/login/cas?target=%2Fapi%2Fbets%2Foffers")
      .queryParam("gateway", true)
      .check(status.is(302))
    )

}