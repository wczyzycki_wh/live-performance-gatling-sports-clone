package com.williamhill.whnft.sports.requestTrafagarObjects

import com.williamhill.whnft.sports.helpers.Common
import io.gatling.core.Predef._
import io.gatling.http.Predef._

/**
 * Update by Juri Boiko on 21/12/2018.
 */
object PageViews {

  val headersAll = Map(
    "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
  //  "x-wh-srv-from" -> "SBAPI",
    "User-Agent" -> "${userAgentCode}")

  val headersNative = Map(
    HttpHeaderNames.Accept -> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    HttpHeaderNames.UserAgent -> "${userAgentCode}",
    "x-wh-bet-code" -> "800",
    "x-wh-wrapped" -> "YES",
    "x-wh-added-headers" -> "YES",
    "x-wh-hiddenlive" -> "YES",
    "x-wh-appversion" -> "10.1")

  val headersFragment = Map(
    "Accept" -> "text/html, */*; q=0.01",
    "Accept-Encoding" -> "gzip, deflate, sdch",
    "User-Agent" -> "${userAgentCode}")

  val securebaseUri: String = Common.getConfigFromFile("environment.conf", "securebaseURL")
  val eventUrlPartial : String = Common.getConfigFromFile("environment.conf", "eventPartialURL")
  val marketFragmentUrl : String = Common.getConfigFromFile("environment.conf", "marketFragmentURL")
  val eventUrl: String = Common.getConfigFromFile("environment.conf", "eventURL")
  val numBets: String = Common.getConfigFromFile("environment.conf", "NumTopBets")
  val sportIds: String = Common.getConfigFromFile("environment.conf", "SportIds")
  val categoryStreamingLogs: String = Common.getConfigFromFile("environment.conf", "categoryStreamingLogsURL")
  val connDetailsStreamingLogs: String = Common.getConfigFromFile("environment.conf", "connDetailsStreamingLogsURL")
//  val offerClubUrl: String = Common.getConfigFromFile("environment.conf", "offerClubURL")
  val intlFootball : Map[String, String] =
    Map(
      "ru-ru" -> "Футбол",
      "el-gr" -> "Ποδόσφαιρο",
      "de-de" -> "Fußball",
      "sv-se" -> "Fotboll",
      "ja-jp" -> "サッカー"
    )

  def getMarketFragment =
    exec(session => {
      val r1 = new scala.util.Random(session.hashCode())
      val marketId = session("markets").as[Vector[String]]
      val markUrl = marketFragmentUrl.replace("[sportName]", "football").replace("[eventID]", session("EventID").as[String]).replace("[marketID]", marketId(r1.nextInt(17)))
      session.set("fragmentUrl", markUrl)
    })
    .exec(session => {
      println(session("fragmentUrl").as[String])
      session
    })
    .exec(http("Open Market Fragment - ${platform}")
      .get("${fragmentUrl}")
      .headers(headersFragment)
      .check(status.is(200)) // check not cached
    )
    .pause(1,5)

  def getFootballHighlights =
    exec(http("Open Football Highlights - ${platform}")
    .get(securebaseUri + "/betting/en-gb/football")
    .headers(headersAll)
    .check(status.in(200, 304))
  //  .check(regex("<title>Bet on Football at William Hill - Football Betting</title>").count.is(1))
      )
    .pause(1,5)

  def getFootballHighlightsNative =
    exec(http("Open Football Highlights - ${platform}")
      .get(securebaseUri + "/betting/en-gb/football")
      .headers(headersNative)
      .check(status.in(200, 304))
      //  .check(regex("<title>Bet on Football at William Hill - Football Betting</title>").count.is(1))
    )
      .pause(1,5)

  def getFootballHighlightsPartial =
    exec(http("Open Football Highlights partial - ${platform}")
    .get(securebaseUri + "/betting/en-gb/football.partial")
    .headers(headersAll)
    .check(status.is(200))) //  .check(regex("<title>Online Betting from William Hill - The Home of Betting</title>").count.is(1))
    .pause(1,5)

  def getFootballMatches =
    exec(http("Open Football Matches - ${platform}")
    .get(securebaseUri + "/betting/en-gb/football/matches")
    .headers(headersAll)
    .check(status.is(200)))
    .pause(1,5)

  def getFootballMatchesNative =
    exec(http("Open Football Matches - ${platform}")
      .get(securebaseUri + "/betting/en-gb/football/matches")
      .headers(headersNative)
      .check(status.is(200)))
      .pause(1,5)

  def getFootballMatchesPartial =
    exec(http("Open Football Matches partial - ${platform}")
    .get(securebaseUri + "/betting/en-gb/football/matches.partial")
    .headers(headersAll)
    .check(status.is(200)))
    .pause(1,5)

  def getFootballCompetitions =
    exec(http("Open Football Matches competitions - ${platform}")
    .get(securebaseUri + "/betting/en-gb/football/competitions")
    .headers(headersAll)
    .check(status.is(200)))
    .pause(1,5)

  def getFootballCompetitionsNative =
    exec(http("Open Football Matches competitions - ${platform}")
      .get(securebaseUri + "/betting/en-gb/football/competitions")
      .headers(headersNative)
      .check(status.is(200)))
      .pause(1,5)

  def getFootballCompetitionsPartial =
    exec(http("Open Football Matches competitions partial - ${platform}")
    .get(securebaseUri + "/betting/en-gb/football/competitions.partial")
    .headers(headersAll)
    .check(status.is(200)))
    .pause(1,5)

  def getEvent =
    exec(http("Open Football Event - ${platform}")
    .get(eventUrl.replace("[sportName]", "${Sport}").replace("[eventID]", "${EventID}"))
    .headers(headersAll)
    .check(status.in(200,304)))
    .pause(1,5)

  def getEventNative =
    exec(http("Open Football Event - ${platform}")
      .get(eventUrl.replace("[sportName]", "${Sport}").replace("[eventID]", "${EventID}"))
      .headers(headersNative)
      .check(status.in(200,304)))
      .pause(1,5)

  def getEventPartial =
    exec(http("Open Football Event partial - ${platform}")
    .get(eventUrlPartial.replace("[sportName]", "${Sport}").replace("[eventID]", "${EventID}"))
    .headers(headersAll)
    .check(status.is(200)))
    .pause(1,5)

  def getInplay =
    exec(http("Open in-play - ${platform}")
    .get(securebaseUri + "/betting/en-gb/in-play/all")
    .headers(headersAll)
    .check(status.is(200)))
    .pause(1,5)

  def getInplayNative =
    exec(http("Open in-play - ${platform}")
      .get(securebaseUri + "/betting/en-gb/in-play/all")
      .headers(headersNative)
      .check(status.is(200)))
      .pause(1,5)

  def getInplayPartial =
    exec(http("Open foot in-play partial - ${platform}")
    .get(securebaseUri + "/betting/en-gb/in-play/all.partial")
    .headers(headersAll)
    .check(status.is(200)))
    .pause(1,5)

  def getHorseRacingAntePost =
    exec(http("Open Horse Racing ante-post - ${platform}")
    .get(securebaseUri + "/betting/en-gb/horse-racing/ante-post")
    .headers(headersAll)
    .check(status.is(200)))
    .pause(1,5)

  def getHorseRacingAntePostPartial =
    exec(http("Open Horse Racing ante-post part - ${platform}")
    .get(securebaseUri + "/betting/en-gb/horse-racing/ante-post.partial")
    .headers(headersAll)
    .check(status.is(200)))
    .pause(1,5)

  def getHorseRacingSpecials =
    exec(http("Open Horse Racing specials - ${platform}")
    .get(securebaseUri + "/betting/en-gb/horse-racing/specials")
    .headers(headersAll)
    .check(status.is(200)))
    .pause(1,5)

  def getHorseRacingSpecialsPartial =
    exec(http("Open Horse Racing specials part - ${platform}")
    .get(securebaseUri + "/betting/en-gb/horse-racing/specials.partial")
    .headers(headersAll)
    .check(status.is(200)))
    .pause(1,5)

  def getHorseRacingMeetings =
    exec(http("Open horse in-play - ${platform}")
    .get(securebaseUri + "/betting/en-gb/horse-racing/meetings")
    .headers(headersAll)
    .check(status.is(200)))
    .pause(1,5)

  def getHorseRacingMeetingsPartial =
    exec(http("Open horse in-play part - ${platform}")
    .get(securebaseUri + "/betting/en-gb/horse-racing/meetings.partial")
    .headers(headersAll)
    .check(status.is(200)))
    .pause(1,5)

  def getTopBetsApps =
    exec(http("Open Top Bets Apps - ${platform}")
    .get(securebaseUri + "/betting/en-gb/apps/top-bets")
    .headers(headersAll)
    .check(status.is(200)))
    .pause(1,5)

  def getTopBets =
    exec(http("Get Top Bets - ${platform}")
    .get(securebaseUri + "/betting/en-gb")
    .queryParam("action", "GetTopXSeln")
    .queryParam("Type", "json")
    .queryParam("NumBets", numBets) // "NumBets" : "10"
    .queryParam("SportIds", sportIds) // TODO: Update sportsIds "SportIds" : "1,5,6,8,9,11,42,43,24,19"
    .queryParam("Locale", "gb")
    .queryParam("Page", "0")
    .headers(headersAll)
    .check(status.in(200, 304)))
    .pause(1,5)

  def getStreamingLogs =
    //TODO: Include the login call in the request
    getEvent
    .exec(http("Get Category streaming logs - ${platform}")
    .get(categoryStreamingLogs)
    .headers(headersAll)
    .check(status.is(200)))
    .pause(1,5)
    .exec(http("Get ConnDetails streaming logs - ${platform}")
    .get(connDetailsStreamingLogs)
    .headers(headersAll)
    .check(status.is(200)))
    .pause(1,5)

  def getIntlHome =
    foreach(intlFootball.toSeq, "intlFootball") {
      exec(session => {
        var kV = session("intlFootball").as[Tuple2[String, String]]
        session.setAll(Map(
          "language" -> kV._1,
          "intlSport" -> kV._2
        ))
      })
      .exec(http("Open ${language} Home - ${platform}")
      .get(securebaseUri + "/betting/${language}")
      .headers(headersAll)
      .check(status.is(200))
      .check(regex("<p(.*)>${intlSport}</p>").exists))
    }
    .pause(1,5)

  def getIntlFootballHighlights =
    foreach(intlFootball.toSeq, "intlFootball") {
      exec(session => {
        var kV = session("intlFootball").as[Tuple2[String, String]]
        session.setAll(Map(
          "language" -> kV._1,
          "intlSport" -> kV._2
        ))
      })
      .exec(http("Open ${language} Football Highlights - ${platform}")
      .get(securebaseUri + "/betting/${language}/${intlSport}")
      .headers(headersAll)
      .check(status.is(200))
      .check(regex("<h1(.*)>${intlSport}(.*)</h1>").exists))
    }
    .pause(1,5)

  def getVirtualWorld =
  exec(http("Open Virtual World - ${platform}")
    .get(securebaseUri + "/betting/en-gb/virtual-world/horses/flats")
    .headers(headersAll)
    .check(status.is(200))
  )
    .pause(1,5)

  def getStaticBetting30 =
    exec(http("Open betting 30")
      .get("https://static.williamhill.com/sport/betting-30/")
      .headers(headersAll)
      .check(status.is(200)))
      .pause(1,5)

  def getGrandNational =
    exec(http("Open Grand National")
      .get("https://static.williamhill.com/events/grand-national/")
      .headers(headersAll)
      .check(status.is(200)))
      .pause(1,5)

  def getAuthRSP =
    exec(http("Open Auth RSP")
      .get("https://auth.williamhill.com/rsp/")
      .headers(headersAll)
      .check(status.is(200)))
      .pause(1,5)

//  def getOfferClubFlow =
//    exec(addCookie(Cookie("cust_login", "${custLogin}")))
//    .exec(addCookie(Cookie("cust_auth", "${custAuth}")))
//    .exec(addCookie(Cookie("cust_ssl_login", "${sslLogin}")))
//    .exec(http("Open OfferClub page - ${platform}")
//    .get(offerClubUrl)
//    .check(status.is(200))
//    .check(regex("""<input type="hidden" name="ticket" value="(.*)">""").saveAs("unique_id")))
//    .exec(http("whauth")
//    .post("https://offerclub.williamhillbet.com/whauth")
//    .formParam("ticket", "${unique_id}")
//    .check(status.is(200)))
//    .exec(http("Click join - ${platform}")
//    .get(offerClubUrl + "Home/Join")
//    .check(status.is(200)))
//    .pause(1,5)
//    .exec(http("Click bet now - ${platform}")
//    .get("http://sports.williamhill.com/bet/en-gb/betting/y/5/cp/727/Football.html?intcid=offCB-uki-spo-OfferClub-oth-con-all-ptl-soc-040816")
//    .check(status.is(200)))
//    .pause(1,5)

  //TODO:Check if the following endpoints are hit:
  //http://w.sports.williamhill.com/fragments/racecard/en-gb/horse-racing/OB_EV9005157
  //http://sports.williamhill.com/mbt_ajax/en-gb
  //GET   https://myaccount.williamhill.com/callback
  //GET   https://myaccount.williamhill.com/api/statements/bet
  //GET   http://whdn.williamhill.com/cms/maintenance/sports/
  //GET   http://w.sports.williamhill.com/fragments/eventEntity/en-gb/horse-racing/OB_EV8960724/1.json
  //GET   http://w.sports.williamhill.com/fragments/eventEntity/en-gb/football/OB_EV8960724/1.json
  //GET   http://w.sports.williamhill.com/betting/en-gb/horse-racing/specials.partial
  //GET   http://w.sports.williamhill.com/betting/en-gb/horse-racing/OB_EV8960724.partial
  //GET   http://w.sports.williamhill.com/betting/en-gb/horse-racing/ante-post.partial
  //GET   http://w.sports.williamhill.com/betting/en-gb/football/OB_EV8960724.partial
  //GET   http://sports.williamhill.com/slp/en-gb/custom/initAPI.js
  //GET   http://sports.williamhill.com/betting/en-gb/football/matches/competition

}