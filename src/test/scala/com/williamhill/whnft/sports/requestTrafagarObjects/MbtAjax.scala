package com.williamhill.whnft.sports.requestTrafagarObjects

import com.williamhill.whnft.sports.helpers.Common
import io.gatling.core.Predef._
import io.gatling.http.Predef._

/**
  * Created by Juri Boiko on 25/04/2019.
  */
object MbtAjax {
  
   val headers = Map(
    "Accept-Encoding" -> "gzip, deflate",
    "Origin" -> "https://sports.williamhill.com",
     "User-Agent" -> "${userAgentCode}")

  val securebaseURL : String = Common.getConfigFromFile("environment.conf", "securebaseURL")

  def getAjaxOpenBets(ajaxURL: String) =
      exec(flushSessionCookies)
        .exec(addCookie(Cookie("cust_login", "${custLogin}")))
        .exec(addCookie(Cookie("cust_auth", "${custAuth}")))
        .exec(addCookie(Cookie("CSRF_COOKIE", "${csrf_token}")))
        .exec(http("Ajax Open bets - ${platform}")
          .post(securebaseURL + "/" + ajaxURL + "/en-gb")
          .formParam("action", "GoOpenBets")
          .formParam("blockbuster_id", "-1")
          .formParam("csrf_token", "${csrf_token}")
          .check(status.is(200))
          //Commented the line below as the redirect is not setting the CSRF_COOKIE
          //However all usages of this method are preceded by the Login method, so the cookie has already been set
          //.check(headerRegex("Set-Cookie", "CSRF_COOKIE=([^;]+)").exists.saveAs("csrf_token"))
        )
        .pause(1, 5)


  def getAjaxGoBets(ajaxURL: String) =
    exec(flushSessionCookies)
      .exec(addCookie(Cookie("cust_login", "${custLogin}")))
      .exec(addCookie(Cookie("cust_auth", "${custAuth}")))
      .exec(addCookie(Cookie("CSRF_COOKIE", "${csrf_token}")))
      .exec(http("Ajax Go bets - ${platform}")
        .post(securebaseURL + "/" + ajaxURL + "/en-gb")
        .formParam("action", "GoBets")
        .formParam("blockbuster", "-1")
        .formParam("csrf_token", "${csrf_token}")
        .check(status.is(200))
        .check(headerRegex("Set-Cookie", "CSRF_COOKIE=([^;]+)").exists.saveAs("csrf_token"))
      )
      .pause(1, 5)

    }

