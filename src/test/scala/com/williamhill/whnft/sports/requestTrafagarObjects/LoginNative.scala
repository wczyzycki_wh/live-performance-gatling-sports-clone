package com.williamhill.whnft.sports.requestTrafagarObjects

import com.williamhill.whnft.sports.helpers.Common
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._
import org.slf4j.LoggerFactory

object LoginNative {

  val securebaseURL: String = Common.getConfigFromFile("environment.conf", "securebaseURL")
  val myaccountURL: String = Common.getConfigFromFile("environment.conf", "myaccountURL")
  val baseTFURL: String = securebaseURL + "/betting/en-gb"
  val authURL: String = Common.getConfigFromFile("environment.conf", "authURL")
  val v2SessionTicketsURL: String = Common.getConfigFromFile("environment.conf", "v2SessionTicketsURL")
  val v2AccountsMeURL: String = Common.getConfigFromFile("environment.conf", "v2AccountsMeURL")
  val apiKey: String = Common.getConfigFromFile("environment.conf", "apiKey")
  val secret: String = Common.getConfigFromFile("environment.conf", "apiSecret")
  val nativeAuthURL: String = Common.getConfigFromFile("environment.conf", "nativeAuthURL")
  val whapiHost: String = Common.getConfigFromFile("environment.conf", "whapiGib2Host")
  val whapiHostUrl = "https://" + whapiHost
  val myAccountUrl: String = Common.getConfigFromFile("environment.conf", "myaccountURL") // securebaseURL
  val casLoginURL: String = Common.getConfigFromFile("environment.conf", "casLoginURL")

  val headersCas = Map(
    "content-type" -> "application/json",
    "User-Agent" -> "${userAgentCode}",
    "Host" -> authURL.replace("https://", ""))

  val headersCasHTML = Map(
    "Accept" -> "application/xhtml+xml",
    "User-Agent" -> "${userAgentCode}",
    "Host" -> securebaseURL.replace("https://", ""))

  val headersV2 = Map(
    "apikey" -> apiKey,
    "content-type" -> "application/json",
    "apisecret" -> secret,
    "User-Agent" -> "${userAgentCode}",
    "Host" -> whapiHost,
    "Connection" -> "keep-alive",
    "Cookie" -> "wh_device={\"app_version\":\"9.1\",\"is_native\":true,\"device_os\":\"ios\",\"os_version\":\"11.3\",\"is_tablet\":false}"
    )


  val headersLogin = Map(
    HttpHeaderNames.AcceptEncoding -> "br, gzip, deflate",
    HttpHeaderNames.UserAgent -> "${userAgentCode}",
    HttpHeaderNames.Accept -> HttpHeaderValues.ApplicationJson,
    HttpHeaderNames.ContentType -> HttpHeaderValues.ApplicationFormUrlEncoded,
    HttpHeaderNames.AcceptLanguage -> "en-gb",
    HttpHeaderNames.Connection -> HttpHeaderValues.KeepAlive,
    "Host" -> "auth.williamhill.com",
    "Cookie" -> "wh_device={\"app_version\":\"9.1\",\"is_native\":true,\"device_os\":\"ios\",\"os_version\":\"11.3\",\"is_tablet\":false}; CasUsername=${Username}"
  )

  val headersCasJSON = Map(
    "Accept-Encoding" -> "gzip, deflate",
    "Accept" -> "application/json",
    "Accept-Language" -> "en-US,en;q=0.9,et;q=0.8,ru;q=0.7,es;q=0.6",
    "Origin" -> securebaseURL,
    "User-Agent" -> "${userAgentCode}")


  def login(numberOfAccountCalls: Int) =
    exec(http("GET V2 Session InternalTicket - ${platform}")
      .post(whapiHostUrl + v2SessionTicketsURL + "?fields=location,ticket,expiryDateTime,temporaryPassword,temporaryPasswordUrl")
      .headers(headersV2)
      .header(HttpHeaderNames.ContentType, HttpHeaderValues.ApplicationFormUrlEncoded)
      .body(StringBody("{\"username\":\"${Username}\",\"password\":\"${Password}\"}")).asJson
      .check(status.is(200))
      .check(jsonPath("$.ticket").saveAs("tgtTicket"))
      .check(jsonPath("$.location").saveAs("location")))
      .exec(http("GET V2 Session Tickets - ${platform}")
        .get("${location}" + "/serviceTicket")
        .queryParam("target", nativeAuthURL)
        .queryParam("fields", "location,ticket")
        .headers(headersV2)
        .header(HttpHeaderNames.ContentType, HttpHeaderValues.ApplicationFormUrlEncoded)
        .check(status.is(200))
        .check(jsonPath("$.ticket").saveAs("stTicket"))
      )
      .pause(0,1)
    /*  .exec(http("Cas auth share")
        .get(authURL + "/cas/share")
        .queryParam("target", nativeAuthURL)
        .queryParam("ticket", "${stTicket}")
        .queryParam("cust_login", "true")
        .header(HttpHeaderNames.UserAgent, "${UAStrings}")
        .header(HttpHeaderNames.Accept, HttpHeaderValues.ApplicationXhtml)
        .check(status.in(302, 200))
        //  .check(headerRegex("Set-Cookie", "CASTGC=(.*); path=/").saveAs("CASTGC"))
        //  .disableFollowRedirect
      )

     */
      .exec(session => {
        LoggerFactory.getLogger("Check for Username: " + session("Username").as[String] + " logged in").info("logging info")
        session
      })
      .pause(0,1)

 /*     .exec(session => {
        session("gatling.http.cookies").validate[CookieJar].map { cookieJar =>
          val custLoginCookie = cookieJar.get(Uri.create(securebaseURL)).find(_.getName == "cust_login")
          val sslLoginCookie = cookieJar.get(Uri.create(securebaseURL)).find(_.getName == "cust_ssl_login")
          val custAuthCookie = cookieJar.get(Uri.create(securebaseURL)).find(_.getName == "cust_auth")
          val CSRF = cookieJar.get(Uri.create(securebaseURL)).find(_.getName == "CSRF_COOKIE")

          session.setAll(Map(
            "custLogin" -> custLoginCookie.orNull.getValue,
            "sslLogin" -> sslLoginCookie.orNull.getValue,
            "custAuth" -> custAuthCookie.orNull.getValue,
            "csrf_token" -> CSRF.orNull.getValue)
          )
        }
      })

  */
      //    .repeat(numberOfAccountCalls)(exec(getBalance))
      //    .doIf(session => !userLoggedIn(session)) {
  //      exec(session => {
  //        println("Username: " + session("Username").as[String] + " not logged in successfully.")
  //        session
  //      })
      //    }
      //   .repeat(numberOfAccountCalls)(exec(getAccountFields))
      //    .repeat(1)(exec(getAccountFields))
      .pause(1, 2)

  def getBalance =
    exec(http("GET WHAPI Account Balance - ${platform}")
      .get(whapiHostUrl + v2AccountsMeURL + "/balance")
      .headers(headersV2)
      .header("apiTicket", "${tgtTicket}")
      .check(status.is(200))
   //   .check(jsonPath("$.balance").saveAs("balanceAmount"))
    )
      .pause(0, 1)
      .exec(session => {
     //   LoggerFactory.getLogger("Account amount balance: " + session("balanceAmount").as[String]).info("logging info")
        session
      })

  def getAccountFields =
    exec(http("GET WHAPI Account Fields - ${platform}")
      .get(whapiHostUrl + v2AccountsMeURL + "?fields=firstName,lastName,title,email,accountNum,street1,street2,street3,postcode,city,country,countryCode,rewards.status")
      .headers(headersV2)
      .header("apiTicket", "${tgtTicket}")
      .check(status.is(200)))
      .pause(0, 1)

  /*  .exec(http("GET Account Balance - ${platform}")
        .get(whapiHostUrl + v2AccountsMeURL + "/balance?fields=currencyCode,withdrawableFunds,availableFunds")
        .headers(headersV2)
        .header("apiTicket", "${tgtTicket}")
        .check(status.is(200)))
      .pause(1, 5)
    */

  def getAccountPayments =
    exec(http("GET WHAPI Account Payments  - ${platform}")
      .get(whapiHostUrl + v2AccountsMeURL + "/payments")
      .headers(headersV2)
      .header("apiTicket", "${tgtTicket}")
      .check(status.is(200)))
      .pause(1, 2)

  def userLoggedIn(session: Session): Boolean =
    session.contains("custLogin") && !session("custLogin").as[String].isEmpty

  def loginNativeGetCas =
    exec(
      http("GET CAS login auth - ${platform}")
        .get(casLoginURL)
        .queryParam("service", nativeAuthURL)
        .headers(headersLogin)
        .check(status.is(200))
        .check(bodyString.saveAs("responseBody"))
        .check(jsonPath("$.form_defaults.lt").saveAs("lt"))
        .check(jsonPath("$.form_defaults.executionKey").saveAs("executionKey"))
        .check(headerRegex("Set-Cookie", "JSESSIONID=([^;]+)").exists.saveAs("JSESSIONID"))
    )

  def loginNativePostCas(numberOfAccountCalls: Int) =
    exec(http("GET Session Tickets CAS - ${platform}")
      .post(casLoginURL)
      .queryParam("service", nativeAuthURL)
      .formParam("username", "${Username}")
      .formParam("password", "${Password}")
      .formParam("_eventId", "submit")
      .formParam("rememberUsername", "0")
      .formParam("keepMeLoggedIn", "1")
      .formParam("executionKey", "${executionKey}")
      .formParam("execution", "${executionKey}")
      .formParam("lt", "${lt}")
      .headers(headersLogin)
      .check(status.is(200))
      .check(jsonPath("$.serviceTicket").saveAs("serviceTicket"))
      .check(jsonPath("$.next").saveAs("next"))
    )
      .exec(http("GET Session InternalTicket CAS - ${platform}")
        .post(authURL + "/cas/share")
        .disableFollowRedirect
        .header(HttpHeaderNames.UserAgent, "${UAStrings}")
        .header(HttpHeaderNames.Accept, HttpHeaderValues.ApplicationXhtml)
        .headers(headersCas)
        .queryParam("target", nativeAuthURL)
        .queryParam("ticket", "${serviceTicket}") //
        .queryParam("cust_login", true)
        .check(status.in(302))
        .check(headerRegex("Set-Cookie", "CASTGC=([^;]+)").exists.saveAs("tgtTicket"))
      )
      .exec { session =>
        LoggerFactory.getLogger("CAS").info("Created TGT Ticket [{}]", session("tgtTicket").as[String])
        session
      }
      .exec(http("GET CAS login auth - ${platform}")
        .get(authURL + "/cas/login")
        .queryParam("service", nativeAuthURL)
        .queryParam("cust_login", true)
        .queryParam("gateway", true)
        .disableFollowRedirect
        .check(status.in(302))
        .check(headerRegex("Location", "(.*)").saveAs("location")))
      .exec(http("CAS login location url - ${platform}")
        .get("${location}")
        .disableFollowRedirect
        .check(status.in(301))
        .check(headerRegex("Set-Cookie", "cust_ssl_login=([^;]+)").exists.saveAs("sslLogin"))
        .check(headerRegex("Set-Cookie", "cust_auth=([^;]+)").exists.saveAs("custAuth"))
        .check(headerRegex("Set-Cookie", "cust_login=([^;]+)").exists.saveAs("custLogin"))
        .check(headerRegex("Set-Cookie", "CSRF_COOKIE=([^;]+)").exists.saveAs("csrf_token")))
      .repeat(numberOfAccountCalls)(exec(getBalance))
      .exec(getAccountFields)
      .pause(1,2)

  def logoutCas: ChainBuilder =
    exec(http("Logout CAS - ${platform}")
      .get(authURL + "/cas/logout")
      .headers(headersLogin)
      .header("CASTGC", "${tgtTicket}")
      .queryParam("service", nativeAuthURL)
      .check(status.is(200))
      .check(headerRegex("Set-Cookie", "cust_login=([^;]+)").is("\"\""))
    ).exec { session =>
      LoggerFactory.getLogger("CAS").info("Destroyed TGT Ticket [{}]", session("tgtTicket").as[String])
      session
    }


  def whapiWithoutCas(numberOfaccountcall: Int) =
    exec(http("GET V2 Session InternalTicket - ${platform}")
      .post(whapiHostUrl + v2SessionTicketsURL + "?fields=location,ticket,expiryDateTime,temporaryPassword,temporaryPasswordUrl")
      .headers(headersV2)
      .header(HttpHeaderNames.ContentType, HttpHeaderValues.ApplicationFormUrlEncoded)
      .body(StringBody("{\"username\":\"${Username}\",\"password\":\"${Password}\"}")).asJson
      .check(status.is(200))
      .check(jsonPath("$.ticket").saveAs("tgtTicket"))
      .check(jsonPath("$.location").saveAs("location")))
      .exec(http("GET V2 Session Tickets - ${platform}")
        .get("${location}" + "/serviceTicket")
        .queryParam("target", nativeAuthURL)
        .queryParam("fields", "location,ticket")
        .headers(headersV2)
        .header(HttpHeaderNames.ContentType, HttpHeaderValues.ApplicationFormUrlEncoded)
        .check(status.is(200))
        .check(jsonPath("$.ticket").saveAs("stTicket"))
      )
      .repeat(numberOfaccountcall)(exec(getBalance))
      .exec(getAccountFields)
      .pause(1,2)

}