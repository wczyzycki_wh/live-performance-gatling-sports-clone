package com.williamhill.whnft.sports.requestTrafagarObjects

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.helpers.Common._
import io.gatling.core.Predef._
import io.gatling.core.body.StringBody
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._
import org.slf4j.LoggerFactory

object Login {

  val securebaseURL : String = Common.getConfigFromFile("environment.conf", "securebaseURL")
  val baseTFURL: String = securebaseURL + "/betting/en-gb"
  val OBsecurebaseURL : String = Common.getConfigFromFile("environment.conf", "OBsecurebaseURL")
  val authURL: String = Common.getConfigFromFile("environment.conf", "authURL")
  val myaccountURL : String =Common.getConfigFromFile("environment.conf", "myaccountURL")
  val transactHost : String =Common.getConfigFromFile("environment.conf", "transactHost")
  val streamingHost : String =Common.getConfigFromFile("environment.conf", "streamingHost")

  def timestamp: Long = System.currentTimeMillis / 1000

  val headersLogin = Map(
    HttpHeaderNames.AcceptEncoding -> "gzip, deflate",
    HttpHeaderNames.Origin -> securebaseURL,
    HttpHeaderNames.UserAgent -> "${UAStrings}",
    HttpHeaderNames.Accept -> "*/*",
    HttpHeaderNames.ContentType -> "application/x-www-form-urlencoded; charset=UTF-8",
    HttpHeaderNames.Referer -> baseTFURL,
    HttpHeaderNames.AcceptLanguage -> "en-GB,en;q=0.8,en-US;q=0.6,it;q=0.4,es;q=0.2"
  )

  val headersCasJSON = Map(
    "Accept-Encoding" -> "gzip, deflate",
    "Accept" -> "application/json",
    "Accept-Language" -> "en-US,en;q=0.9,et;q=0.8,ru;q=0.7,es;q=0.6",
    "Origin" -> securebaseURL,
    "User-Agent" -> "${userAgentCode}")

  val headersCas = Map(
    "Accept-Encoding" -> "gzip, deflate",
    "Accept" -> "*/*",
    "Accept-Language" -> "en-US,en;q=0.9,et;q=0.8,ru;q=0.7,es;q=0.6",
    "Origin" -> securebaseURL,
    "User-Agent" -> "${userAgentCode}")

  val headerSSL = Map("Login-Authentication-SSL" -> "${sslLogin}",
    "Host" -> "sports.williamhill.com")

  def loginBetSlipV2(numberOfAccountCall: Int): ChainBuilder =
    exec(
      initialLogin(2),
      getSessionTicket,
      getSession
    )

  def initialLogin(numberOfAccountCall: Int): ChainBuilder =
    tryMax(2) {
      pause(2, 8)
        .exec(flushHttpCache)
        .exec(flushCookieJar)
        .exec(
          http("Login BetSlip V2 - ${device}")
            .post(securebaseURL + "/bet/en-gb")
            .disableFollowRedirect
            .formParam("username", "${Username}")
            .formParam("password", "${Password}")
            .formParam("action", "DoLogin")
            .formParam("login_uid", session => timestamp)
            .formParam("target_page", securebaseURL + "/bet/en-gb")
            .formParam("responseType", "json")
            .formParam("remember_me_value", "1")
            .headers(headersLogin)
            .check(status.is(200))
            .check(headerRegex("Set-Cookie", "cust_auth=([^;]+)").exists.saveAs("custAuth"))
            .check(headerRegex("Set-Cookie", "cust_login=([^;]+)").exists.saveAs("custLogin"))
            .check(headerRegex("Set-Cookie", "cust_ssl_login=([^;]+)").exists.saveAs("sslLogin"))
            .check(headerRegex("Set-Cookie", "CSRF_COOKIE=([^;]+)").exists.saveAs("csrf_token"))
        )
    }

  def getSessionTicket: ChainBuilder =
    exec(
      http("Get Session Ticket - ${device}")
        .get(authURL + "/cas/login")
        .disableFollowRedirect
        .queryParam("cust_login", "true")
        .queryParam("service", "https://transact.williamhill.com/betslip/login/cas?target=%2Fapi%2Fbets%2Flogin%3Flang%3Den")
        .queryParam("gateway", "true")
        .check(status.is(302))
        .check(headerRegex("location", "ticket=([^;]+)").saveAs("ST"))
    )

  def getSession: ChainBuilder =
    exec(
      http("Get BetSlip Session - ${device}")
        .get(transactHost + "/betslip/login/cas")
        .disableFollowRedirect
        .disableUrlEncoding
        .queryParam("target", "/api/bets/login?lang=en")
        .queryParam("ticket", "${ST}")
        .check(status.is(302))
        .check(headerRegex("set-cookie", "SESSION=([^;]+)").exists.saveAs("session"))
    )

  def login(numberOfaccountcall: Int) =
    tryMax(2) {
      pause(2, 8)
        .exec(http("Login - ${platform}")
          .post(securebaseURL + "/bet/en-gb")
          .formParam("username", "${Username}")
          .formParam("password", "${Password}")
          .formParam("action", "DoLogin")
          .formParam("login_uid", session => timestamp)
          .formParam("target_page", securebaseURL + "/bet/en-gb")
          .formParam("responseType", "json")
          .formParam("remember_me_value", "1")
          .headers(headersLogin)
          .check(status.is(200))
          .check(headerRegex("Set-Cookie", "cust_auth=([^;]+)").exists.saveAs("custAuth"))
          .check(headerRegex("Set-Cookie", "cust_login=([^;]+)").exists.saveAs("custLogin"))
          .check(headerRegex("Set-Cookie", "cust_ssl_login=([^;]+)").exists.saveAs("sslLogin"))
          .check(headerRegex("Set-Cookie", "CSRF_COOKIE=([^;]+)").exists.saveAs("csrf_token")))
      /*  .exec(http("CAS transact betslip Call")
          .get(OBsecurebaseURL)
          .headers(headersLogin)
          .queryParam("action", "GetSslXLoginForm")
          .queryParam("LastLogin", "1")
          .check(status.is(200))
          .check(headerRegex("Set-Cookie", "CSRF_COOKIE=(.*); path=/").saveAs("csrf_token"))
        )
        .exec(http("streaming Cas Call")
          .get(authURL + "/cas/login")
          .headers(headersCas)
          .queryParam("cust_login", true)
          .queryParam("service", streamingHost + "/login/cas?target=%2Fapi%2Fstreams%2Fevents%3FresponseFormat%3DIDS")
          .queryParam("gateway", true)
          .check(status.in(200, 302))
        )
        .exec(http("Cas aoa orchestrate")
          .get(authURL + "/cas/login")
          .headers(headersCasJSON)
          .queryParam("cust_login", true)
          .queryParam("service", myaccountURL + "/aoa/orchestrate")
          .check(status.is(200))
          .check(jsonPath("$.serviceTicket").saveAs("serviceTicket"))
          .check(jsonPath("$.next").saveAs("next"))
        )
        .exec(http("CAS myaccount aoa OPTIONS")
          .options("${next}")
          .headers(headersCas)
          .check(status.is(200))
        )


     // exec(flushSessionCookies)
        .exec(http("CAS myaccount aoa POST")
          .post("${next}")
          .body(StringBody("{\"locale\":\"en-gb\",\"services\":[\"pluscard\"]}")).asJSON
          .headers(headersCas)
          .check(status.is(200))
        )
        .exec(http("CAS transact betslip Call")
          .get(authURL + "/cas/login")
          .headers(headersCas)
          .queryParam("cust_login", true)
          .queryParam("service", transactHost + "/betslip/login/cas?target=%2Fapi%2Fbets%2Foffers")
          .queryParam("gateway", true)
          .check(status.in(200, 302))
        )

       */
        .exec(session => {
          LoggerFactory.getLogger("Check for Username: " + session("Username").as[String] + " logged in").info("logging info")
          session
        })
        .repeat(numberOfaccountcall)(
          exec(getBalanceSSL))
        }
        .pause(1, 5)

  def getBalanceSSL =
    exec(flushSessionCookies)
      .exec(addCookie(Cookie("cust_login", "${custLogin}").withDomain(".williamhill.com")))
      .exec(addCookie(Cookie("cust_ssl_login", "${sslLogin}").withDomain(".williamhill.com")))
      .exec(addCookie(Cookie("cust_auth", "${custAuth}").withDomain(".williamhill.com")))
      .exec(addCookie(Cookie("CSRF_COOKIE", "${csrf_token}").withDomain(".williamhill.com")))
      .exec(http("Get user balance - ${platform}")
        .get(securebaseURL + "/bal/en-gb")
        .queryParam("action", "GetSslBalance")
        .queryParam("external", "false")
        .headers(headersLogin)
        .check(status.is(200))
      //  .check(jsonPath("$.amount").saveAs("balanceAmount"))
        .check(headerRegex("Set-Cookie", "CSRF_COOKIE=(.*); path=/").saveAs("csrf_token_balance"))
      )
      .pause(1, 5)
      .exec(session => {
  //      LoggerFactory.getLogger("Account amount balance: " + session("balanceAmount").as[String]).info("logging info")
        session
      })

  private def dynamicTry(numRetry: Int, minPause: Int, maxPause: Int, chain: ChainBuilder, pauseBetweenRetries: Boolean) = {
    tryMax(numRetry) {
      doIfOrElse(pauseBetweenRetries) {
        pause(minPause, maxPause).exec(chain)
      } {
        exec(chain)
      }
    }
  }

  def freebetsCall =
    exec(flushSessionCookies)
      .exec(addCookie(Cookie("cust_login", "${custLogin}")))
      .exec(addCookie(Cookie("cust_auth", "${custAuth}")))
      .exec(addCookie(Cookie("cust_ssl_login", "${sslLogin}")))
      .exec(addCookie(Cookie("CSRF_COOKIE", "${csrf_token}")))
      .exec(http("FreeBets bet boost API call")
        .post(securebaseURL + "/freebets/qualification/freebets/tokens")
        .queryParam("triggerType", "ACTIVEUSER")
        .queryParam("channel", "W")
        .queryParam("accountNo", "${AccNumber}")
        .headers(headerSSL)
        .check(status.is(202))
      )
      .pause(1, 3)

}