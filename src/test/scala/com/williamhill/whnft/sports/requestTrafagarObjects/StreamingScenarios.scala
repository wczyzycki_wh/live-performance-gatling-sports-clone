package com.williamhill.whnft.sports.requestTrafagarObjects

import java.text.SimpleDateFormat
import java.time.LocalDate

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.requestTrafagarObjects.BetSlip.BetType
import io.gatling.core.Predef._
import io.gatling.http.Predef._

/**
  * Update by Ana Isabel Fernandez on 11/03/2019.
  */
object StreamingScenarios {

  val headersAll = Map(
    "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
    "User-Agent" -> "${userAgentCode}")
  val headersFragment = Map(
    "Accept" -> "text/html, */*; q=0.01",
    "Accept-Encoding" -> "gzip, deflate, sdch",
    "User-Agent" -> "${userAgentCode}")

  val eventUrlPartial : String = Common.getConfigFromFile("environment.conf", "eventPartialURL")
  val marketFragmentUrl : String = Common.getConfigFromFile("environment.conf", "marketFragmentURL")
  val eventUrl: String = Common.getConfigFromFile("environment.conf", "eventURL")
  val numBets: String = Common.getConfigFromFile("environment.conf", "NumTopBets")
  val sportIds: String = Common.getConfigFromFile("environment.conf", "SportIds")
  val categoryStreamingLogs: String = Common.getConfigFromFile("environment.conf", "categoryStreamingLogsURL")
  val connDetailsStreamingLogs: String = Common.getConfigFromFile("environment.conf", "connDetailsStreamingLogsURL")
  val streamingEventsAllowedToSee: String = Common.getConfigFromFile("environment.conf", "streamingEventsAllowedToSee")
  val streamingEventsAll: String = Common.getConfigFromFile("environment.conf", "streamingEventsAll")
  val streamingEvent: String = Common.getConfigFromFile("environment.conf", "streamingEvent")
  val securebaseURL: String = Common.getConfigFromFile("environment.conf", "securebaseURL")
  val racingTVUrl: String = Common.getConfigFromFile("environment.conf", "racingTV")



  def getEvent =
    exec(http("Open Horse Racing Event")
      .get(eventUrl.replace("[sportName]", "${Sport}").replace("[eventID]", "${EventID}"))
      .headers(headersAll)
      .check(status.in(200,304)))
      .pause(1,5)
//
//  def getHorseRacingAntePost =
//    exec(http("Open Horse Racing ante-post - ${platform}")
//      .get(securebaseURL + "/betting/en-gb/horse-racing/ante-post")
//      .headers(headersAll)
//      .check(status.is(200)))
//      .pause(1,5)
//
//  def getHorseRacingAntePostPartial =
//    exec(http("Open Horse Racing ante-post part - ${platform}")
//      .get(securebaseURL + "/betting/en-gb/horse-racing/ante-post.partial")
//      .headers(headersAll)
//      .check(status.is(200)))
//      .pause(1,5)
//
//  def getHorseRacingSpecials =
//    exec(http("Open Horse Racing specials - ${platform}")
//      .get(securebaseURL + "/betting/en-gb/horse-racing/specials")
//      .headers(headersAll)
//      .check(status.is(200)))
//      .pause(1,5)
//
//  def getHorseRacingSpecialsPartial =
//    exec(http("Open Horse Racing specials part - ${platform}")
//      .get(securebaseURL + "/betting/en-gb/horse-racing/specials.partial")
//      .headers(headersAll)
//      .check(status.is(200)))
//      .pause(1,5)
//
//  def getHorseRacingMeetings =
//    exec(http("Open horse in-play - ${platform}")
//      .get(securebaseURL + "/betting/en-gb/horse-racing/meetings")
//      .headers(headersAll)
//      .check(status.is(200)))
//      .pause(1,5)
//
//  def getHorseRacingMeetingsPartial =
//    exec(http("Open horse in-play part - ${platform}")
//      .get(securebaseURL + "/betting/en-gb/horse-racing/meetings.partial")
//      .headers(headersAll)
//      .check(status.is(200)))
//      .pause(1,5)
//
//  def getTopBetsHorseRacing =
//    exec(http("Get Top Bets Horse Racing - ${platform}")
//      .get(securebaseURL + "/betting/en-gb/horse-racing/top-bets")
//      .headers(headersAll)
//      .check(status.in(200)))
//      .pause(1,5)

  val format = new SimpleDateFormat("yyyy-mm-dd")
  val today = LocalDate.now()
  val tomorrow = LocalDate.now().plusDays(1)


  def getStreamingEvents =
    getEvent
//      .exec(http("Get Category streaming logs - ${platform}")
//        .get(categoryStreamingLogs)
//        .headers(headersAll)
//        .check(status.in(200,304)))
//      .pause(1,5)
//      .exec(http("Get ConnDetails streaming logs - ${platform}")
//        .get(connDetailsStreamingLogs)
//        .headers(headersAll)
//        .check(status.is(200)))
//      .pause(1,5)
      .exec(http("Get Streaming Events All")
        .get(streamingEventsAll.replace("[dateFrom]", today.toString).replace("[dateTo]", tomorrow.toString))
        .headers(headersAll)
        .check(status.is(200)))
      .pause(1,5)

  def loopStreamingSteps(times: Int) =
  repeat(times)(
  //  exec(getStreamingEvents)
      exec(BetSlip.placeBet("mbt_slp", BetType.Single, 1))
//      .exec(getStreamingEventsAllowedToSeeByCustomer)
      .forever("loop") {
        exec(flushHttpCache)
        //  .exec(flushCookieJar)
        //  .exec(flushSessionCookies)
          .exec(getStreamingEvents)
      }
  )

  def getStreamingEventsAllowedToSeeByCustomer =
    getEvent
        .exec(http("Get Streaming Events Allowed To See")
          .get(streamingEventsAllowedToSee.replace("[dateFrom]", today.toString).replace("[dateTo]", tomorrow.toString))
          .headers(headersAll)
          .check(status.is(200))
          .check(regex("${EventID}").exists))
        .pause(1,5)

  def getStreamingEvent =
    getEvent
      .exec(http("Get Streaming Event")
        .get(streamingEvent + "${EventIDApp}")
        .headers(headersAll)
        .check(status.is(200)))
      .pause(1,5)

  def getRacingTVStream =
    getEvent
    .exec(http("Get RacingTV Stream")
      .get(racingTVUrl)
      .headers(headersAll)
      .check(status.is(200)))
    .pause(1,5)
}