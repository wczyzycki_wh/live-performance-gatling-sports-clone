package com.williamhill.whnft.sports.requestTrafagarObjects

import com.williamhill.whnft.sports.helpers.Common
import io.gatling.core.Predef._
import io.gatling.http.Predef._

/**
  * Created by Ana Isabel Fernandez Gomez on 12/02/2019.
  */
object LoginSCC {

  val riga: String = Common.getConfigFromFile("environment.conf", "riga")
  val apiKeyRiga: String = Common.getConfigFromFile("environment.conf", "apiKeyRiga")
  val secretRiga: String = Common.getConfigFromFile("environment.conf", "apiSecretRiga")
  val whapiHostSCC: String = Common.getConfigFromFile("environment.conf", "whapiHostSCC")
  val whapiHostSCCUrl = "https://" + whapiHostSCC

  val headersV2 = Map(
    "who-apiKey" -> apiKeyRiga,
    "content-type" -> "application/xml",
    "Accept" -> "application/xml",
    "who-secret" -> secretRiga)


  def getAccount =
    exec(http("GET WHAPI Riga Account  - ${platform}")
      .get(whapiHostSCCUrl + riga + "${AccNumber}")
      .headers(headersV2)
      .check(status.in(200, 400)))
      .pause(1, 2)


  def getBalance =
    exec(http("GET WHAPI Riga Account Balance - ${platform}")
      .get(whapiHostSCCUrl + riga + "${AccNumber}" + "/wallets/balances")
      .headers(headersV2)
      .check(status.in(200, 400)))
      .pause(1, 2)


  def getToken =
    exec(http("GET WHAPI Riga Token  - ${platform}")
      .get(whapiHostSCCUrl + riga + "token/" + "${AccNumber}")
      .headers(headersV2)
      .check(status.is(200)))
      .pause(1, 2)


  def getSystemConfiguration =
    exec(http("GET WHAPI Riga System Configuration - ${platform}")
      .get(whapiHostSCCUrl + "/riga/systemConfiguration")
      .headers(headersV2)
      .check(status.is(200)))
      .pause(1, 2)

  def getExchangeRatesEurGib=
    exec(http("GET WHAPI Riga Exchange Rates - ${platform}")
      .get(whapiHostSCCUrl + "/riga/exchangerates/from/EUR/to/GBP")
      .headers(headersV2)
      .check(status.is(200)))
      .pause(1, 2)

  def getExchangeRatesGibEur=
    exec(http("GET WHAPI Riga Exchange Rates - ${platform}")
      .get(whapiHostSCCUrl + "/riga/exchangerates/to/EUR/from/GBP")
      .headers(headersV2)
      .check(status.is(200)))
      .pause(1, 2)

  def getHealthcheck=
    exec(http("GET WHAPI Riga HealthCheck - ${platform}")
      .get(whapiHostSCCUrl + "/riga/healthcheck")
      .headers(headersV2)
      .check(status.is(200)))
      .pause(1, 2)

  def userLoggedIn(session: Session): Boolean =
    session.contains("custLogin") && !session("custLogin").as[String].isEmpty

}