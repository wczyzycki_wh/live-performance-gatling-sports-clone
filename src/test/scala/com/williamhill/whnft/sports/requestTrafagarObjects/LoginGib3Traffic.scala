package com.williamhill.whnft.sports.requestTrafagarObjects

import com.williamhill.whnft.sports.helpers.Common
import io.gatling.core.Predef._
import io.gatling.http.Predef._

/**
  * Created by Ana Isabel on 27/02/2019.
  */
object LoginGib3Traffic {

  val v2SessionTicketsURL: String = Common.getConfigFromFile("environment.conf", "v2SessionTicketsURL")
  val v2AccountsMeURL: String = Common.getConfigFromFile("environment.conf", "v2AccountsMeURL")
  val apiKey: String = Common.getConfigFromFile("environment.conf", "apiKey")
  val secret: String = Common.getConfigFromFile("environment.conf", "apiSecret")
  val whapiHost: String = Common.getConfigFromFile("environment.conf", "whapiGib3Host")
  val whapiHostUrl = "https://" + whapiHost

  val headersV2 = Map(
    "apikey" -> "l7xx5a894120685d42b2ac16d489050a4e6d",
    //"apikey" -> apiKey,
    "content-type" -> "application/json",
    "apisecret" -> "9b3da4b991174351af2c26631e1005e7",
    //"apisecret" -> secret,
    "Host" -> whapiHost,
    )

  def games =
    exec(http("GET V2 Gamesdata Jackpots")
      .get(whapiHostUrl + "/v2/gamesdata/games/jackpots")
      .headers(headersV2)
      .header(HttpHeaderNames.ContentType, HttpHeaderValues.ApplicationFormUrlEncoded)
      .check(status.is(200)))
      .pause(1, 2)
}