package com.williamhill.whnft.sports.requestTrafagarObjects

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import com.williamhill.whnft.sports.helpers.Common
import io.gatling.core.action.builder.ActionBuilder
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.request.builder.HttpRequestBuilder

/**
  * Update by Juri Boiko on 21/12/2018.
  */
object HomePage {

  val securebaseUri: String = Common.getConfigFromFile("environment.conf", "securebaseURL")
  val baseTFUri = securebaseUri + "/betting/en-gb"
  val wsportsUri: String = Common.getConfigFromFile("environment.conf", "wsportsURL")

  val headersNative = Map(
    HttpHeaderNames.Accept -> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    HttpHeaderNames.UserAgent -> "${userAgentCode}",
    "x-wh-bet-code" -> "800",
    "x-wh-wrapped" -> "YES",
    "x-wh-added-headers" -> "YES",
    "x-wh-hiddenlive" -> "YES",
    "x-wh-appversion" -> "10.1"
  )

  val headersAll = Map(
    HttpHeaderNames.Accept -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
    HttpHeaderNames.UserAgent -> "${userAgentCode}",
    HttpHeaderNames.AcceptEncoding -> "gzip, deflate, sdch",
    HttpHeaderNames.AcceptLanguage -> "en-GB,en;q=0.8,en-US;q=0.6,it;q=0.4,es;q=0.2",
    HttpHeaderNames.CacheControl -> "max-age=0"
  )


  val headersFragment = Map(
    HttpHeaderNames.Accept -> "text/html, */*; q=0.01",
    HttpHeaderNames.UserAgent -> "${UAStrings}",
    HttpHeaderNames.AcceptEncoding -> "gzip, deflate, sdch",
    HttpHeaderNames.AcceptLanguage -> "en-GB,en;q=0.8,en-US;q=0.6,it;q=0.4,es;q=0.2",
    HttpHeaderNames.CacheControl -> "max-age=0",
    HttpHeaderNames.Origin -> securebaseUri,
    HttpHeaderNames.Referer -> baseTFUri
  )

  def getNative =
    exec(http("Navigate to home - ${platform}")
      .get(securebaseUri + "/betting/en-gb")
      .headers(headersNative)
      .check(status.is(200))
    )
      .pause(1, 2)
      .exec(http("Navigate to football - ${platform}")
        .get(securebaseUri + "/betting/en-gb/football")
        .headers(headersNative)
        .check(status.is(200))
      )
      .pause(1, 2)
      .exec(http("Navigate to horse-racing - ${platform}")
        .get(securebaseUri + "/betting/en-gb/horse-racing/meetings/all/today")
        .headers(headersNative)
        .check(status.is(200))
      )
      .pause(1, 2)


  def navigateToSportsHomeBetSlipV2: ChainBuilder =
    tryMax(3) {
      exec(http("Navigate to home - ${device}")
        .get(securebaseUri + "/betting/en-gb?betslipVersion=whbetslip")
        .headers(headersAll)
        .check(status.in(200, 304)))
        .pause(1, 2)
    }

  def get =
    tryMax(3) {
      //  exec(addCookie(Cookie("wh_device", "{\"is_native\":false,\"device_os\":\"ios\",\"os_version\":\"12.4\",\"is_tablet\":false}")))
      exec(http("Navigate to home - ${platform}")
        .get(securebaseUri + "/betting/en-gb") //+ "/betting/en-gb"
        .headers(headersAll)
        .check(status.in(200, 304)))
        .pause(1, 2)
      //        .exec(http("Navigate to football - ${platform}")
      //          .get(securebaseUri + "/betting/en-gb/football")
//          .headers(headersAll)
//          .check(status.is(200))
//        )
//        .pause(1, 2)
//        .exec(http("Navigate to horse-racing - ${platform}")
//          .get(securebaseUri + "/betting/en-gb/horse-racing/meetings/all/today")
//          .headers(headersAll)
//          .check(status.is(200))
//        )
//        .pause(1, 2)
    }

  def getPostLogin =
    doIf(session => userLoggedIn(session)) {
      exec(http("Navigate to home - ${platform}")
        .get(securebaseUri + "/betting/en-gb")
        .headers(headersAll)
        .check(status.in(200, 304)))
        //      .check(regex("<title>Online Betting from William Hill - The Home of Betting</title>").count.is(1)))
        //      .exec(getSslXLogin)
        .exec(http("Get bal-GetBalance - ${platform}")
        .get(securebaseUri + "/bal/en-gb?action=GetSslBalance&external=false")
        .headers(headersFragment)
        .check(status.is(200)))
        .exec(http("Get slp js - ${platform}")
          .get(securebaseUri + "/slp/en-gb/custom/initAPI.js")
          .headers(headersFragment)
          .header(HttpHeaderNames.XRequestedWith, HttpHeaderValues.XmlHttpRequest)
          .check(status.in(302, 200)))
        .exec(http("Get initApJs - ${platform}")
          .get(securebaseUri + "/bet/en-gb/custom/initAPI.js")
          .headers(headersFragment)
          .header(HttpHeaderNames.XRequestedWith, HttpHeaderValues.XmlHttpRequest)
          .check(status.is(200)))
        .exec(initPayment)
        .pause(1, 5)
    }

  def getFragments = {
    exec(http("Get inplay fragment - ${platform}")
      .get(wsportsUri + "/fragments/home/inPlay/en-gb/all")
      .headers(headersFragment)
      .check(status.in(304, 200)))
      .exec(http("Get highlights fragment - ${platform}")
        .get(wsportsUri + "/fragments/home/highlights/en-gb")
        .headers(headersFragment)
        .check(status.in(304, 200)))
      .exec(http("Get enhanced fragment - ${platform}")
        .get(wsportsUri + "/fragments/home/enhanced/en-gb")
        .headers(headersFragment)
        .check(status.in(304, 200)))
      .exec(http("Get nextOff fragment - ${platform}")
        .get(wsportsUri + "/fragments/home/nextOff/en-gb")
        .headers(headersFragment)
        .check(status.in(304, 200)))
      .exec(http("Get topBets fragment - ${platform}")
        .get(wsportsUri + "/fragments/topBets/en-gb/homepage")
        .headers(headersFragment)
        .check(status.in(304, 200)))
      .exec(http("Get sidebarLeft football fragment - ${platform}")
        .get(wsportsUri + "/fragments/sidebarLeft/en-gb/football/event")
        .headers(headersFragment)
        .check(status.in(304, 200)))
      .exec(http("Get sidebarLeft horse-racing fragment - ${platform}")
        .get(wsportsUri + "/fragments/sidebarLeft/en-gb/horse-racing/meetings")
        .headers(headersFragment)
        .check(status.in(304, 200)))
      .pause(1, 5)
  }

  def getSslXLogin = {
    exec(http("Post GetSslXLoginForm - ${platform}")
      .post(securebaseUri + "/bet/en-gb?action=GetSslXLoginForm&LastLogin=1")
      .headers(headersFragment)
      .check(status.is(200)))
      .pause(1, 5)
  }

  def initPayment = {
    exec(http("Get initPaymentApi - ${platform}")
      .get(securebaseUri + "/ext/en-gb?action=InitPaymentAPI&jsonApi=1")
      .headers(headersFragment)
      .check(status.is(200)))
      .pause(1, 5)
  }

  def userLoggedIn(session: Session): Boolean =
    session.contains("custLogin") && !session("custLogin").as[String].isEmpty

  def getDataFilesNative =
    tryMax(3) {
    //  exec(addCookie(Cookie("wh_device", "{\"app_version\":\"10.1\",\"is_native\":true,\"device_os\":\"ios\",\"os_version\":\"12.2\",\"is_tablet\":false}")))
        exec(http("Navigate to data games lobby - ${platform}")
          .get(securebaseUri + "/data/xss01/data/games-lobby/bootstrap/en-gb") //+ "/betting/en-gb"
          .headers(headersNative)
          .check(status.in(200, 304)))
        .pause(1, 2)
        .exec(http("Navigate to data in play - ${platform}")
          .get(securebaseUri + "/data/sns01/en-gb/ios/in-play/sts/1569932260")
          .headers(headersNative)
          .check(status.is(200))
        )
        .pause(1, 2)
        .exec(http("Navigate to data highlights - ${platform}")
          .get(securebaseUri + "/data/sns01/en-gb/ios/sts/1569932260")
          .headers(headersNative)
          .check(status.is(200))
        )
        .pause(1, 2)
    }

  def getDataFiles =
    tryMax(3) {
      //   exec(addCookie(Cookie("wh_device", "{\"is_native\":false,\"device_os\":\"ios\",\"os_version\":\"12.4\",\"is_tablet\":false}")))
      exec(addCookie(Cookie("x-apm-brtm-bt-p", "Chrome")))
        .exec(addCookie(Cookie("x-apm-brtm-bt-pv", "77")))
        .exec(http("Navigate to data games lobby - ${platform}")
          .get(securebaseUri + "/data/xss01/data/games-lobby/bootstrap/en-gb") //+ "/betting/en-gb"
          .headers(headersAll)
          .check(status.in(200, 304)))
        .pause(1, 2)
        .exec(http("Navigate to data in play - ${platform}")
          .get(securebaseUri + "/data/sea01/data/en-gb/football/inPlay")
          .headers(headersAll)
          .check(status.is(200))
        )
        .pause(1, 2)
        .exec(http("Navigate to data highlights - ${platform}")
          .get(securebaseUri + "/data/sea01/data/en-gb/football/highlights")
          .headers(headersAll)
          .check(status.is(200))
        )
        .pause(1, 2)
    }


}