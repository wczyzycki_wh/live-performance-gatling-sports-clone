package com.williamhill.whnft.sports.requestTrafagarObjects

import com.williamhill.whnft.sports.helpers.Common
import io.gatling.core.Predef._
import io.gatling.core.body.CompositeByteArrayBody
import io.gatling.http.Predef._
import org.slf4j.LoggerFactory


object BuildYourOdds {

  val byoUrl: String = Common.getConfigFromFile("environment.conf", "byoUrl") //byoHost
  val byoHost: String = Common.getConfigFromFile("environment.conf", "byoHost")
  val transactHost: String = Common.getConfigFromFile("environment.conf", "transactHost")
  val authURL: String = Common.getConfigFromFile("environment.conf", "authURL")
  val securebaseURL : String = Common.getConfigFromFile("environment.conf", "securebaseURL")

  val selectionBYO = csv ("SelectionsBYO.csv")
  val selectionBYO2 = csv ("SelectionsBYO2.csv")
  val selectionBYO3 = csv ("SelectionsBYO3.csv")
  val selectionBYO4 = csv ("SelectionsBYO4.csv")


  val headersAll = Map(
    "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
    "Accept-Encoding" -> "gzip, deflate, br",
    "User-Agent" -> "${userAgentCode}")
  val headersJson = Map(
    "Accept" -> "application/json",
    "Content-type" -> "application/json",
    "User-Agent" -> "${userAgentCode}",
    "Host" -> transactHost.replace("https://", ""))

  val headersCas = Map(
    "Accept-Encoding" -> "gzip, deflate",
    "Accept" -> "*/*",
    "Accept-Language" -> "en-US,en;q=0.9,et;q=0.8,ru;q=0.7,es;q=0.6",
    "Origin" -> securebaseURL,
    "User-Agent" -> "${UAStrings}")


  def getBYOdata =
    exec(http("getBYO")
      .get(byoUrl.replace("[eventID]", "${EventID}"))
      .headers(headersAll)
      .check(status.is(200)))
      .pause(1,3)

  def postBYOdata(requestBody: CompositeByteArrayBody) =
    repeat(10) {
      exec(session => {
        session.set("byoGetSelection", false)
      })

        .feed(selectionBYO.random)
        .feed(selectionBYO2.random)
        .feed(selectionBYO3.random)
    //    .feed(selectionBYO4.random)

      .exec(http("makeBYO")
        .post(byoHost)
        .body(requestBody).asJson
        .headers(headersJson)
        .check(status.in(200)))
        .pause(1, 3)
        .exec(session => {
          session.set("byoGetSelection", true)
        })
        .exec(http("addBYO")
          .post(byoHost)
          .body(requestBody).asJson
          .headers(headersJson)
          .check(status.in(200))
          .check(jsonPath("$.selections[0].byoSelection").saveAs("byoSelection")))
        .pause(1, 3)
        .exec(session => {
          LoggerFactory.getLogger("Bet byo: " + session("byoSelection").as[String] + " For Account: " + session("Username").as[String]).info("logging info")
          session
        })
    }

   def makeCasBYOauth =
      exec(http("streaming Cas Call")
        .get(authURL + "/cas/login")
        .headers(headersCas)
        .queryParam("cust_login", true)
        .queryParam("service", transactHost + "/betslip/login/cas?target=%2Fapi%2Fbets%2FbuildSlip")
        .queryParam("gateway", true)
        .check(status.in(200, 302))
        .disableFollowRedirect
        .check(header("Location").find.saveAs("locationUrl"))
      )
        .exec(http("streaming Cas Call")
          .get("${locationUrl}")
          .headers(headersCas)
          .disableFollowRedirect
       //   .queryParam("target", "/api/bets/buildSlip")
        //  .queryParam("ticket", transactHost + "/betslip/login/cas?target=%2Fapi%2Fbets%2FbuildSlip")
          .check(status.in(200, 302)))




  //

}