package com.williamhill.whnft.sports.requestTrafagarObjects

import com.williamhill.whnft.sports.helpers.Common
import io.gatling.core.Predef._
import io.gatling.http.Predef._

/**
  * Created by Ana Isabel on 27/02/2019.
  */
object LoginSCCWithoutRiga {

  val v2SessionTicketsURL: String = Common.getConfigFromFile("environment.conf", "v2SessionTicketsURL")
  val v2AccountsMeURL: String = Common.getConfigFromFile("environment.conf", "v2AccountsMeURL")
  val apiKey: String = Common.getConfigFromFile("environment.conf", "apiKey")
  val secret: String = Common.getConfigFromFile("environment.conf", "apiSecret")
  val whapiHostSCC: String = Common.getConfigFromFile("environment.conf", "whapiHostSCC")
  val whapiHostSCCUrl = "https://" + whapiHostSCC

  val headersV2 = Map(
    "apikey" -> apiKey,
    "content-type" -> "application/json",
    "apisecret" -> secret,
    "User-Agent" -> "${userAgentCode}",
    "Host" -> whapiHostSCC,
    )

  def v2Accounts(numberOfBets: Int) =

    repeat(numberOfBets) {
      exec(flushHttpCache)
        .exec(
      getAccountFields,
      getBalance)

    }

  def login =
    exec(http("GET V2 Session InternalTicket - ${platform}")
      .post(whapiHostSCCUrl + "/v2/sessions/tickets")
      .headers(headersV2)
      .header(HttpHeaderNames.ContentType, HttpHeaderValues.ApplicationFormUrlEncoded)
      .body(StringBody("{\"username\":\"${Username}\",\"password\":\"${Password}\"}")).asJson
      .check(status.is(200))
      .check(jsonPath("$.ticket").saveAs("tgtTicket")))
      .pause(1, 2)

  def getAccountFields =
    exec(http("GET WHAPI Account Fields - ${platform}")
      .get(whapiHostSCCUrl + "/v2/accounts/account")
      .headers(headersV2)
      .header("apiTicket", "${tgtTicket}")
      .check(status.is(200)))
      .pause(1, 2)

  def getBalance =
    exec(http("GET WHAPI Account Balance - ${platform}")
      .get(whapiHostSCCUrl + v2AccountsMeURL + "/balance")
      .headers(headersV2)
      .header("apiTicket", "${tgtTicket}")
      .check(status.is(200)))
      .pause(1, 2)
}