package com.williamhill.whnft.sports.requestTrafagarObjects

import com.williamhill.whnft.sports.helpers.Common
import io.gatling.core.Predef._
import io.gatling.http.Predef._

object Rewards {

  val baseURL: String = Common.getConfigFromFile("environment.conf", "baseURL")
  val baseTFURL: String = baseURL + "/betting/en-gb"
  val securebaseURL: String = Common.getConfigFromFile("environment.conf", "securebaseURL")
  val rewardsURL: String = Common.getConfigFromFile("environment.conf", "rewardsURL")

  def timestamp: Long = System.currentTimeMillis / 1000

  val headers = Map(
    HttpHeaderNames.AcceptEncoding -> "gzip, deflate",
    HttpHeaderNames.Origin -> baseURL,
    HttpHeaderNames.UserAgent -> "${UAStrings}",
    HttpHeaderNames.Accept -> "*/*",
    HttpHeaderNames.ContentType -> "application/x-www-form-urlencoded; charset=UTF-8",
    HttpHeaderNames.Referer -> baseTFURL,
    HttpHeaderNames.AcceptLanguage -> "en-GB,en;q=0.8,en-US;q=0.6,it;q=0.4,es;q=0.2"
  )

  def accountRewards =
    pause(2, 8)
      .exec(addCookie(Cookie("cust_login", "${custLogin}")))
      .exec(addCookie(Cookie("cust_ssl_login", "${sslLogin}")))
      .exec(addCookie(Cookie("cust_auth", "${custAuth}")))
      .exec(addCookie(Cookie("CSRF_COOKIE", "${csrf_token}")))
      .exec(http("Get apps/rewards - ${platform}")
        .get(baseURL + "/betting/en-gb/apps/rewards")
        .headers(headers)
        check(status.is(200))
      )
      .exec(addCookie(Cookie("cust_login", "${custLogin}")))
      .exec(addCookie(Cookie("cust_ssl_login", "${sslLogin}")))
      .exec(addCookie(Cookie("cust_auth", "${custAuth}")))
      .exec(addCookie(Cookie("CSRF_COOKIE", "${csrf_token}")))
      .exec(http("Get account/status - ${platform}")
        .get(rewardsURL + "/account/status")
        .headers(headers)
        .check(status.is(200))
       // .check(regex("status\":\"optedIn\""))
        .check(jsonPath("$[?(@.status == 'optedIn')]"))
      )
      .exec(addCookie(Cookie("cust_login", "${custLogin}")))
      .exec(addCookie(Cookie("cust_ssl_login", "${sslLogin}")))
      .exec(addCookie(Cookie("cust_auth", "${custAuth}")))
      .exec(addCookie(Cookie("CSRF_COOKIE", "${csrf_token}")))
      .exec(http("Get account - ${platform}")
        .get(rewardsURL + "/account")
        .headers(headers)
        .check(status.is(200))
        .check(jsonPath("$.profile[?(@.points == 500)]"))
      )
      .exec(addCookie(Cookie("cust_login", "${custLogin}")))
      .exec(addCookie(Cookie("cust_ssl_login", "${sslLogin}")))
      .exec(addCookie(Cookie("cust_auth", "${custAuth}")))
      .exec(addCookie(Cookie("CSRF_COOKIE", "${csrf_token}")))
      .exec(http("Get account/eligible - ${platform}")
        .get(rewardsURL + "/account/eligible")
        .headers(headers)
        .check(status.is(200))
     //   .check(regex("eligible\":true"))
        .check(jsonPath("$[?(@.eligible == true)]")))
      .pause(1, 5)

  def accountNotEligible =
    pause(2, 8)
      .exec(addCookie(Cookie("cust_login", "${custLogin}")))
      .exec(addCookie(Cookie("cust_ssl_login", "${sslLogin}")))
      .exec(addCookie(Cookie("cust_auth", "${custAuth}")))
      .exec(addCookie(Cookie("CSRF_COOKIE", "${csrf_token}")))
      .exec(http("Get account/NOT eligible - ${platform}")
        .get(rewardsURL + "/account/eligible")
        .headers(headers)
        .check(status.is(200))
      //  .check(regex("eligible\": false"))
        .check(jsonPath("$[?(@.eligible == false)]"))
         )
      .pause(1, 5)

}