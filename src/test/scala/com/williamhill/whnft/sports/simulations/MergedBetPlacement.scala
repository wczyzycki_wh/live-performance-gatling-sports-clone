package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.requestTrafagarObjects.BetSlip.BetType
import com.williamhill.whnft.sports.scenarios.bets.DesktopBetPlacement._
import com.williamhill.whnft.sports.scenarios.bets.MobileBetPlacement._
import com.williamhill.whnft.sports.scenarios.bets.NativeBetPlacement._
import io.gatling.core.Predef._

import scala.concurrent.duration._

class MergedBetPlacement extends Simulation with SimulationSettingsTrait {

  betType match {
    case BetType.Single => {
      if (singleRun) {
        setUp(
          placeBetDesktop
            .inject(atOnceUsers(vU)),
          placeBetNative
            .inject(atOnceUsers(vU1)),
          placeBetMobile
            .inject(atOnceUsers(vU2))

        )
            .protocols(httpProtocol)
          .maxDuration(runDurationMinutes minutes)
      } else {
        setUp(
          placeBetDesktop
            .inject(rampProfile(vU)),
          placeBetNative
            .inject(atOnceUsers(vU1)),
          placeBetMobile
            .inject(atOnceUsers(vU2))
        )
            .protocols(httpProtocol)
          .maxDuration(runDurationMinutes minutes)
      }
    }
    case BetType.Double => {
      if (singleRun) {
        setUp(
          placeDoubleBetDesktop
            .inject(atOnceUsers(vU)),
          placeDoubleBetNative
            .inject(atOnceUsers(vU1)),
          placeDoubleBetMobile
            .inject(atOnceUsers(vU2))
        )
            .protocols(httpProtocol)
          .maxDuration(runDurationMinutes minutes)
      } else {
        setUp(
          placeDoubleBetDesktop
            .inject(atOnceUsers(vU)),
          placeDoubleBetNative
            .inject(atOnceUsers(vU1)),
          placeDoubleBetMobile
            .inject(atOnceUsers(vU2)))
            .protocols(httpProtocol)
          .maxDuration(runDurationMinutes minutes)
      }
    }
    case BetType.Treble => {
      if (singleRun) {
        setUp(
          placeTrebleBetDesktop
            .inject(atOnceUsers(vU)),
          placeTrebleBetNative
            .inject(atOnceUsers(vU1)),
          placeTrebleBetMobile
            .inject(atOnceUsers(vU2))
        )
            .protocols(httpProtocol)
          .maxDuration(runDurationMinutes minutes)
      } else {
        setUp(
          placeTrebleBetDesktop
            .inject(atOnceUsers(vU)),
          placeTrebleBetNative
            .inject(atOnceUsers(vU1)),
          placeTrebleBetMobile
            .inject(atOnceUsers(vU2)))
            .protocols(httpProtocol)
          .maxDuration(runDurationMinutes minutes)
      }
    }
  }
}
