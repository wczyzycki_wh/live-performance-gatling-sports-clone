package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.pages.NavigateToFootballPages._
import com.williamhill.whnft.sports.scenarios.bets.NativeBetPlacement._
import com.williamhill.whnft.sports.scenarios.pages.NavigateToHorseRacingPages._
import com.williamhill.whnft.sports.scenarios.services.StreamingFullTest._
import io.gatling.core.Predef._

import scala.concurrent.duration._

class NavigateAndBetFootballHorseRacing extends Simulation with SimulationSettingsTrait {

  setUp(
    navigateToFootballPages.inject(atOnceUsers(vU)),
    placeBetNative.inject(atOnceUsers(vU)),
    navigateToHorsePages.inject(atOnceUsers(vU1)),
    placeBetNative_HorseRacing.inject(atOnceUsers(vU1)),
    streamingFullTest.inject(atOnceUsers(vU1))

  ).protocols(httpProtocol).maxDuration(runDurationMinutes minutes)

}
