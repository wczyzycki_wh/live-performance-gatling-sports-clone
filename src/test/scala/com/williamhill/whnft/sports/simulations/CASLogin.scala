package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.accounts.DesktopLoginCAS.desktopLoginCas
import com.williamhill.whnft.sports.scenarios.accounts.NativeLoginCAS.nativeLoginCas
import io.gatling.core.Predef._

import scala.concurrent.duration._

class CASLogin extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(
      nativeLoginCas.inject(atOnceUsers(vU)),
      desktopLoginCas.inject(atOnceUsers(vU1))
        .protocols(httpProtocol))
      .maxDuration(runDurationMinutes minutes)
  }
else {
  setUp(
    nativeLoginCas.inject(rampProfile(vU)),
    desktopLoginCas.inject(rampProfile(vU1))
      .protocols(httpProtocol))
    .maxDuration(runDurationMinutes minutes)
}


}
