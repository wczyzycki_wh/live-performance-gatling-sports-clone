package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.accounts.NativeLogin._
import com.williamhill.whnft.sports.scenarios.accounts.OBLogin._
import com.williamhill.whnft.sports.scenarios.accounts.DesktopLogin._
import com.williamhill.whnft.sports.scenarios.accounts.MobileLogin._
import com.williamhill.whnft.sports.scenarios.accounts.NativeLoginCAS._
import io.gatling.core.Predef._

import scala.concurrent.duration._

/**
  * Update by Juri Boiko on 21/12/2018.
  */
class AllLogins extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(
      oBLogin.inject(atOnceUsers(vU)),
      mobileLogin.inject(atOnceUsers(vU1)),
      desktopLogin.inject(atOnceUsers(vU2)),
      nativeLogin.inject(atOnceUsers(vU3))
    .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(
//      oBLogin.inject(rampProfile(vU)),
      mobileLogin.inject(rampProfile(vU1)),
      desktopLogin.inject(rampProfile(vU2)),
      nativeLogin.inject(rampProfile(vU3))
//      nativeLoginOldCas.inject(rampProfile(vU4))
    .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }

}
