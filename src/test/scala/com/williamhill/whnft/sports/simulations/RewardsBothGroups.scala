package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.services.DesktopRewards._
import com.williamhill.whnft.sports.scenarios.services.DesktopRewardsNotEligible._
import io.gatling.core.Predef._

import scala.concurrent.duration._

class RewardsBothGroups extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(
      desktopRewards.inject(atOnceUsers(vU)),
      desktopRewardsNotEligible.inject(atOnceUsers(vU1))
        .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(
      desktopRewards.inject(rampProfile(vU)).protocols(httpProtocol),
      desktopRewardsNotEligible.inject(atOnceUsers(vU1)).protocols(httpProtocol)
    ).maxDuration(runDurationMinutes minutes)
  }

}