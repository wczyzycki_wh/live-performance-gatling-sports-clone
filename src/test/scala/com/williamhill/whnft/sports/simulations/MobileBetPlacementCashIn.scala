package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.bets.MobileBetPlacementCashIn._
import io.gatling.core.Predef._

import scala.concurrent.duration._

/**
  * Created by juri on 21/12/2018.
  */
class MobileBetPlacementCashIn extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(
      cashInplaceBetMobile.inject(atOnceUsers(vU))
      .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(
      cashInplaceBetMobile.inject(rampProfile(vU))
    .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }

}
