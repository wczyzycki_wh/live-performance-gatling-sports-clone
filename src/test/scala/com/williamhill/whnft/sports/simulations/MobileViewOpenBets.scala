package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.bets.MobileViewOpenBets._
import io.gatling.core.Predef._

import scala.concurrent.duration._

class MobileViewOpenBets extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(
      mobileViewOpenBets.inject(atOnceUsers(vU))
      .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(
      mobileViewOpenBets.inject(rampProfile(vU))
    .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }

}
