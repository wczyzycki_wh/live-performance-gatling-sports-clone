package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.accounts.DesktopLoginSCCWithoutRiga._
import io.gatling.core.Predef._

import scala.concurrent.duration._

/**
  * Update by Ana Isabel on 27/02/2019.
  */
class DesktopLoginSCCWithoutRiga extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(desktopLoginSCCWithoutRiga.inject(atOnceUsers(vU)).protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(desktopLoginSCCWithoutRiga.inject(rampProfile(vU)).protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }

}
