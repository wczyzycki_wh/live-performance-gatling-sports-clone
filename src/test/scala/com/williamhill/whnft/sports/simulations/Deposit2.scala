package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.services.DesktopDeposit._
import com.williamhill.whnft.sports.scenarios.services.MobileDeposit._
import com.williamhill.whnft.sports.scenarios.services.NativeDeposit._
import com.williamhill.whnft.sports.scenarios.services.OBDesktopDeposit._
import io.gatling.core.Predef._

import scala.concurrent.duration._

/**
  * Update by Juri Boiko on 21/12/2018.
  */
class Deposit2 extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(
      desktopDeposit.inject(atOnceUsers(vU)),
      mobileDeposit.inject(atOnceUsers(vU1)),
      nativeDeposit.inject(atOnceUsers(vU2)),
      desktopDepositOB.inject(atOnceUsers(vU3))
      .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(
  //    desktopDeposit.inject(rampProfile(vU))
  //    mobileDeposit.inject(rampProfile(vU1)),
      nativeDeposit.inject(rampProfile(vU2))
  //    desktopDepositOB.inject(rampProfile(vU3))
    .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }

}
