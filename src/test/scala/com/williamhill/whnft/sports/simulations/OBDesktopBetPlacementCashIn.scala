package com.williamhill.whnft.sports.simulations

import io.gatling.core.Predef._
import com.williamhill.whnft.sports.scenarios.bets.OBDesktopBetPlacementCashIn._
import scala.concurrent.duration._

/**
  * Created by Juri Boiko on 17/05/2019.
  */
class OBDesktopBetPlacementCashIn extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(placeBetDesktopCashInOB.inject(atOnceUsers(vU)).protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(placeBetDesktopCashInOB.inject(rampProfile(vU)).protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }

}




