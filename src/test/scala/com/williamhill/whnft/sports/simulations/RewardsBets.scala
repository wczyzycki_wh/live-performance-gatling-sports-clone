package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.services.DesktopRewards._
import com.williamhill.whnft.sports.scenarios.services.DesktopRewardsNotEligible._
import io.gatling.core.Predef._

import scala.concurrent.duration._

class RewardsBets extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(
      desktopRewards.inject(atOnceUsers(vU))
        .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(
      desktopRewards.inject(rampProfile(vU))
        .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }

}