package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.accounts.NativeAccountRegistration._
import com.williamhill.whnft.sports.scenarios.accounts.MobileAccountRegistration._
import com.williamhill.whnft.sports.scenarios.accounts.DesktopAccountRegistration._
import com.williamhill.whnft.sports.scenarios.accounts.DesktopAccountStatements._
import com.williamhill.whnft.sports.scenarios.accounts.OBAccountHistory._
import com.williamhill.whnft.sports.scenarios.accounts.OBAccountRegistration._
import io.gatling.core.Predef._

import scala.concurrent.duration._

/**
  * Created by Juri Boiko on 17/05/2019.
  */
class AccountMix extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(
      desktopAccountStatements.inject(rampProfile(vU)),
//      registerMobileAccount.inject(atOnceUsers(vU1)),
//      registerNativeAccount.inject(atOnceUsers(vU2)),
//      registerDesktopAccount.inject(rampProfile(vU3)),
//      accountRegistrationOB.inject(rampProfile(vU4)),
      obAccountStatement.inject(rampProfile(vU5))
    .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(
      desktopAccountStatements.inject(rampProfile(vU)),
      registerMobileAccount.inject(rampProfile(vU1)),
      registerNativeAccount.inject(rampProfile(vU2)),
      registerDesktopAccount.inject(rampProfile(vU3)),
//      accountRegistrationOB.inject(rampProfile(vU4)),
      obAccountStatement.inject(rampProfile(vU5))
    .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }

}




