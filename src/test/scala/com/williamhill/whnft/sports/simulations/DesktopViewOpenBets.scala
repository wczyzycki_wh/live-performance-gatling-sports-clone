package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.bets.DesktopViewOpenBets._
import io.gatling.core.Predef._

import scala.concurrent.duration._

class DesktopViewOpenBets extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(
      desktopViewOpenBets.inject(atOnceUsers(vU))
      .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(
      desktopViewOpenBets.inject(rampProfile(vU))
    .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }

}
