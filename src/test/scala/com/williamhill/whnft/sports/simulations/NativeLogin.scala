package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.accounts.NativeLogin._
import io.gatling.core.Predef._
import scala.concurrent.duration._
/**
  * Update by Juri Boiko on 21/12/2018.
  */
class NativeLogin extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(nativeLogin.inject(rampProfile(vU)).protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(nativeLogin.inject(rampProfile(vU)).protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }

}
