package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.accounts.OxiScenario
import io.gatling.core.Predef._

import scala.concurrent.duration._

class OxiAcceptTnC extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(
      OxiScenario.Scn_reqAcceptTermsAndConditions.inject(atOnceUsers(vU))
        .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(

      OxiScenario.Scn_reqAcceptTermsAndConditions.inject(rampProfile(vU3))

        .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }

}
