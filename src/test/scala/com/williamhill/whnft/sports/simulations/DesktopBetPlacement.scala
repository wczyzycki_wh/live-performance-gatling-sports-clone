package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.requestTrafagarObjects.BetSlip.BetType
import com.williamhill.whnft.sports.scenarios.bets.DesktopBetPlacement._
import io.gatling.core.Predef._

import scala.concurrent.duration._

/**
 * Update by Juri Boiko on 21/12/2018.
 */
class DesktopBetPlacement extends Simulation with SimulationSettingsTrait {

  betType match {
    case BetType.Single => {
      if (singleRun) {
        setUp(
          placeBetDesktop
            .inject(atOnceUsers(vU))
            .protocols(httpProtocol))
          .maxDuration(runDurationMinutes minutes)
      } else {
        setUp(
          placeBetDesktop
            .inject(rampProfile(vU))
            .protocols(httpProtocol))
          .maxDuration(runDurationMinutes minutes)
      }
    }
    case BetType.Double => {
      if (singleRun) {
        setUp(
          placeDoubleBetDesktop
            .inject(atOnceUsers(vU))
            .protocols(httpProtocol))
          .maxDuration(runDurationMinutes minutes)
      } else {
        setUp(
          placeDoubleBetDesktop
            .inject(rampProfile(vU))
            .protocols(httpProtocol))
          .maxDuration(runDurationMinutes minutes)
      }
    }
    case BetType.Treble => {
      if (singleRun) {
        setUp(
          placeTrebleBetDesktop
            .inject(atOnceUsers(vU))
            .protocols(httpProtocol))
          .maxDuration(runDurationMinutes minutes)
      } else {
        setUp(
          placeTrebleBetDesktop
            .inject(rampProfile(vU))
            .protocols(httpProtocol))
          .maxDuration(runDurationMinutes minutes)
      }
    }
  }
}
