package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.pages.NavigateToFootballTransPages._
import io.gatling.core.Predef._

import scala.concurrent.duration._

/**
  * Update by Juri Boiko on 21/12/2018.
  */
class NavigateToTransPages extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(
      navigateToFootballTransPages.inject(atOnceUsers(vU)).protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(
      navigateToFootballTransPages.inject(rampProfile(vU)).protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }

}
