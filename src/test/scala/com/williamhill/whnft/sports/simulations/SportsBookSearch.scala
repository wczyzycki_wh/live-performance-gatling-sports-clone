package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.services.Searches._
import io.gatling.core.Predef._

import scala.concurrent.duration._

class SportsBookSearch extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(
      sportsBookSearch.inject(atOnceUsers(vU))
    .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(
      sportsBookSearch.inject(rampProfile(vU))
    .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }

}