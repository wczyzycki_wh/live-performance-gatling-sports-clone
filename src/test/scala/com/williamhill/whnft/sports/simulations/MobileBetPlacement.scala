package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.requestTrafagarObjects.BetSlip.BetType
import com.williamhill.whnft.sports.scenarios.bets.MobileBetPlacement._
import io.gatling.core.Predef._

import scala.concurrent.duration._

class MobileBetPlacement extends Simulation with SimulationSettingsTrait {

  betType match {
    case BetType.Single => {
      if (singleRun) {
        setUp(
          placeBetMobile
            .inject(atOnceUsers(vU))
            .protocols(httpProtocol))
          .maxDuration(runDurationMinutes minutes)
      } else {
        setUp(
          placeBetMobile
            .inject(rampProfile(vU))
            .protocols(httpProtocol))
          .maxDuration(runDurationMinutes minutes)
      }
    }
    case BetType.Double => {
      if (singleRun) {
        setUp(
          placeDoubleBetMobile
            .inject(atOnceUsers(vU))
            .protocols(httpProtocol))
          .maxDuration(runDurationMinutes minutes)
      } else {
        setUp(
          placeDoubleBetMobile
            .inject(rampProfile(vU))
            .protocols(httpProtocol))
          .maxDuration(runDurationMinutes minutes)
      }
    }
    case BetType.Treble => {
      if (singleRun) {
        setUp(
          placeTrebleBetMobile
            .inject(atOnceUsers(vU))
            .protocols(httpProtocol))
          .maxDuration(runDurationMinutes minutes)
      } else {
        setUp(
          placeTrebleBetMobile
            .inject(rampProfile(vU))
            .protocols(httpProtocol))
          .maxDuration(runDurationMinutes minutes)
      }
    }
  }
}
