package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.services.StreamingFullTest._
import io.gatling.core.Predef._

import scala.concurrent.duration._

/**
  * Update by Ana Isabel Fernandez Gomez on 11/03/2019.
  */
class StreamingTest extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(streamingFullTest.inject(atOnceUsers(vU)).protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(streamingFullTest.inject(rampProfile(vU)).protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }

}
