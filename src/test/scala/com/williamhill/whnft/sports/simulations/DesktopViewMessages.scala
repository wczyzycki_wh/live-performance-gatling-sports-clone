package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.services.DesktopViewMessages._
import io.gatling.core.Predef._

import scala.concurrent.duration._

class DesktopViewMessages extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(
      desktopViewMessages.inject(atOnceUsers(vU))
      .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(
      desktopViewMessages.inject(rampProfile(vU))
    .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }

}
