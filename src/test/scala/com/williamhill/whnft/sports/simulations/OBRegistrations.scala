package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.accounts.DesktopAccountRegistration._
import com.williamhill.whnft.sports.scenarios.accounts.MobileAccountRegistration._
import com.williamhill.whnft.sports.scenarios.accounts.NativeAccountRegistration._
import com.williamhill.whnft.sports.scenarios.accounts.OBAccountRegistration._
import io.gatling.core.Predef._

import scala.concurrent.duration._

class OBRegistrations extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(
      accountRegistrationOB.inject(atOnceUsers(vU))
    .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(
      accountRegistrationOB.inject(rampProfile(vU))
    .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }

}