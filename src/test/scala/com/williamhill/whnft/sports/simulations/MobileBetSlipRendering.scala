package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.services.BetSlipRendering._
import io.gatling.core.Predef._

import scala.concurrent.duration._

class MobileBetSlipRendering extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(
      betSlipRenderingMobile.inject(atOnceUsers(vU))
      .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(
      betSlipRenderingMobile.inject(rampProfile(vU))
    .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }

}
