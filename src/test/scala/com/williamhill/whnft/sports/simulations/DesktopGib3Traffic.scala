package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.accounts.DesktopLoginGib3Traffic._
import io.gatling.core.Predef._

import scala.concurrent.duration._

/**
  * Created by Ana Isabel on 28/02/2019.
  */
class DesktopGib3Traffic extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
//    setUp(
//      desktopLoginSCCWithoutRiga.inject(rampProfile(vU)),
//      desktopLoginSCC.inject(rampProfile(vU1))
//    .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(
      desktopLoginGib3.inject(rampProfile(vU))
      //desktopLoginSCC.inject(rampProfile(vU1))
    .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }

}




