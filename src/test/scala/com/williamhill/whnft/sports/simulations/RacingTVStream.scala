package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.services.StreamingFullTest._
import io.gatling.core.Predef._

import scala.concurrent.duration._

class RacingTVStream extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(racingTVStreamTest.inject(atOnceUsers(vU)).protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(racingTVStreamTest.inject(rampProfile(vU)).protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }

}
