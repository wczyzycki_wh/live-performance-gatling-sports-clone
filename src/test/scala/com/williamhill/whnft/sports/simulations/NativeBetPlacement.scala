package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.requestTrafagarObjects.BetSlip.BetType
import com.williamhill.whnft.sports.scenarios.bets.NativeBetPlacement._
import io.gatling.core.Predef._

import scala.concurrent.duration._

class NativeBetPlacement extends Simulation with SimulationSettingsTrait {

  betType match {
    case BetType.Single => {
      if (singleRun) {
        setUp(
          placeBetNative
            .inject(atOnceUsers(vU))
            .protocols(httpProtocol))
          .maxDuration(runDurationMinutes minutes)
      } else {
        setUp(
          placeBetNative
            .inject(rampProfile(vU))
            .protocols(httpProtocol))
          .maxDuration(runDurationMinutes minutes)
      }
    }
    case BetType.Double => {
      if (singleRun) {
        setUp(
          placeDoubleBetNative
            .inject(atOnceUsers(vU))
            .protocols(httpProtocol))
          .maxDuration(runDurationMinutes minutes)
      } else {
        setUp(
          placeDoubleBetNative
            .inject(rampProfile(vU))
            .protocols(httpProtocol))
          .maxDuration(runDurationMinutes minutes)
      }
    }
    case BetType.Treble => {
      if (singleRun) {
        setUp(
          placeTrebleBetNative
            .inject(atOnceUsers(vU))
            .protocols(httpProtocol))
          .maxDuration(runDurationMinutes minutes)
      } else {
        setUp(
          placeTrebleBetNative
            .inject(rampProfile(vU))
            .protocols(httpProtocol))
          .maxDuration(runDurationMinutes minutes)
      }
    }
  }

}
