package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.services.BetSlipRendering._
import io.gatling.core.Predef._

import scala.concurrent.duration._

class DesktopBetSlipRendering extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(
      betSlipRenderingDesktop.inject(atOnceUsers(vU))
      .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(
      betSlipRenderingDesktop.inject(rampProfile(vU))
    .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }

}
