package com.williamhill.whnft.sports.simulations

import com.typesafe.config.ConfigFactory
import com.williamhill.whnft.sports.helpers.Common
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder

import scala.concurrent.duration._

trait SimulationSettingsTrait {

  val environmentConfig = "environment.conf"

  val environmentUnderTest = Common.prop("sut", "live")
  val vU = Common.prop("vu", 30)
  val vU1 = Common.prop("vu1", 30)
  val vU2 = Common.prop("vu2", 1)
  val vU3 = Common.prop("vu3", 1)
  val vU4 = Common.prop("vu4", 1)
  val vU5 = Common.prop("vu5", 1)
  val vU6 = Common.prop("vu6", 1)
  val vU7 = Common.prop("vu7", 1)
  val vU8 = Common.prop("vu8", 1)
  val vU9 = Common.prop("vu9", 1)
  val vU10 = Common.prop("vu10", 1)
  val vU11 = Common.prop("vu11", 1)
  val vU12 = Common.prop("vu12", 1)
  val vU13 = Common.prop("vu13", 1)
  val vU14 = Common.prop("vu14", 1)
  val vU15 = Common.prop("vu15", 1)

  val singleRun = Common.prop("single_run", false)
  val rampDurationMinutes = Common.prop("ramp_duration", 2)
  val paceDurationMinutes = Common.prop("pace_duration", 2)
  val runDurationMinutes = Common.prop("run_duration", 90)
  val warmUpTimeSeconds = Common.prop("warm_up_time", 4)

  val betType = Common.getBetType()
  val inPlay = Common.isInPlay()
  val numberOfBetsPerUser = Common.prop("bets_per_user", 10)
  val numberOfaccountcall = Common.prop("native_account_peruser",3)
  val numberOfBetBoostPerUser = Common.prop("bet_boosts_per_user", 5)
  val numberOfAccountStatements = Common.prop("acc_statements_per_user", 2)
  val numberOfDepositsPerUser = Common.prop("deposits_per_user", 10)

  val targetCompetitionPage = Common.prop("target_comp_page", "25084")

  val conf = ConfigFactory.load(environmentConfig)
  val baseUri: String = Common.getConfigFromFile(environmentConfig, "baseURL")


  def rampProfile(scenarioVU: Int) =
    List(
      nothingFor(warmUpTimeSeconds seconds),
      rampUsers((scenarioVU * 0.33).ceil.toInt) during (rampDurationMinutes minutes),
      nothingFor(paceDurationMinutes minutes),
      rampUsers((scenarioVU * 0.33).ceil.toInt) during (rampDurationMinutes minutes),
      nothingFor(paceDurationMinutes minutes),
      rampUsers((scenarioVU * 0.33).ceil.toInt) during (rampDurationMinutes minutes),
      nothingFor(paceDurationMinutes minutes),
      rampUsers((scenarioVU * 0.05).ceil.toInt) during (rampDurationMinutes minutes),
      nothingFor(paceDurationMinutes minutes),
      rampUsers((scenarioVU * 0.05).ceil.toInt) during (rampDurationMinutes minutes),
      nothingFor(paceDurationMinutes minutes),
      rampUsers((scenarioVU * 0.05).ceil.toInt) during (rampDurationMinutes minutes),
      nothingFor(paceDurationMinutes minutes)
    )

//  def rampProfile(scenarioVU: Int) =
//    List(
//      nothingFor(warmUpTimeSeconds seconds),
//      rampUsers(scenarioVU) during (rampDurationMinutes minutes),
//      nothingFor(paceDurationMinutes minutes),
//      rampUsers(scenarioVU) during (rampDurationMinutes minutes),
//      nothingFor(paceDurationMinutes minutes),
//      rampUsers(scenarioVU) during (rampDurationMinutes minutes),
//      nothingFor(paceDurationMinutes minutes)
//    )

  def httpProtocol: HttpProtocolBuilder = http
    .baseUrl(baseUri)
//    .proxy(Proxy("localhost", 8889)
//      .httpsPort(8889))
    .inferHtmlResources(BlackList(
      """.*metrics.williamhill.com.*""",
      """.*nexus.ensighten.com.*""",
      """.*\.(jpg|css|JPG|jpeg|gif|ico|png|map|PNG|svg|woff|woff2|((t|o)tf)|js).*"""
    ))
    .silentResources
    .maxConnectionsPerHost(32)
}
