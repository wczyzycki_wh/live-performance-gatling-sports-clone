package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.accounts.DesktopLoginSCC.desktopLoginSCC
import com.williamhill.whnft.sports.scenarios.accounts.DesktopLoginSCCWithoutRiga.desktopLoginSCCWithoutRiga
import io.gatling.core.Predef._

import scala.concurrent.duration._

/**
  * Created by Ana Isabel on 28/02/2019.
  */
class DesktopSCCMix extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(
      desktopLoginSCCWithoutRiga.inject(rampProfile(vU)),
      desktopLoginSCC.inject(rampProfile(vU1))
    .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(
      desktopLoginSCCWithoutRiga.inject(rampProfile(vU)),
      desktopLoginSCC.inject(rampProfile(vU1))
    .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }

}




