package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.services.DesktopRewardsNoBets._
import io.gatling.core.Predef._

import scala.concurrent.duration._

class RewardsNoBets extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(
      desktopRewardsNoBets.inject(atOnceUsers(vU)).protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(
      desktopRewardsNoBets.inject(rampProfile(vU)).protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
}