package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.pages.NavigateToFootballPages._
import com.williamhill.whnft.sports.scenarios.pages.NavigateToFootballPagesNative._
import com.williamhill.whnft.sports.scenarios.pages.NavigateToVirtualPages._
import com.williamhill.whnft.sports.scenarios.pages.NavigateToHorseRacingPages._
import com.williamhill.whnft.sports.scenarios.pages.NavigateToFootballTransPages._
import com.williamhill.whnft.sports.scenarios.services.GetStreamingLogs._
import com.williamhill.whnft.sports.scenarios.pages.NavigateToFragmentPages._
import com.williamhill.whnft.sports.requestTrafagarObjects._
import io.gatling.core.Predef._

import scala.concurrent.duration._

/**
  * Update by Juri Boiko on 21/12/2018.
  */
class NavigateToPages extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(
      navigateToFootballPages.inject(atOnceUsers(vU))
//      navigateToHorsePages.inject(atOnceUsers(vU1)),
//      getStreamingLogs.inject(atOnceUsers(vU6)),
//      navigateToFootballTransPages.inject(atOnceUsers(vU7)),
//      navigateToVirtualPages.inject(atOnceUsers(vU8)),
//      navigateToFragmentPages.inject(atOnceUsers(vU9))
     .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(
      navigateToFootballPages.inject(rampProfile(vU)),
      navigateToFootballPagesNative.inject(rampProfile(vU)),
     navigateToHorsePages.inject(rampProfile(vU1)),
      getStreamingLogs.inject(rampProfile(vU2)),
      navigateToFragmentPages.inject(rampProfile(vU3))
    .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }

}
