package com.williamhill.whnft.sports.simulations


import com.williamhill.whnft.sports.scenarios.accounts.OxiScenario
import io.gatling.core.Predef._

import scala.concurrent.duration._

/**
 * Created by Smija Alex on 12/02/2020.
 */
class OxiSimulation extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(
      OxiScenario.Scn_reqAccountGetDetail.inject(atOnceUsers(vU))
        .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(

      OxiScenario.Scn_OBAccountValidate2.inject(rampProfile(vU3))

        .protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }

}
