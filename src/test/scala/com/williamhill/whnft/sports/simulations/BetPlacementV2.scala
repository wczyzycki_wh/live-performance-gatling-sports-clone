package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.bets.BetPlacementV2._
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation

import scala.concurrent.duration._

class BetPlacementV2 extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(
      placeBetV2Desktop.inject(atOnceUsers(vU)),
      placeBetV2Mobile.inject(atOnceUsers(vU)),
      placeBetV2Native.inject(atOnceUsers(vU))
        .protocols(httpProtocol)
    )
      .maxDuration(runDurationMinutes minutes)
  } else {
    setUp(
      placeBetV2Desktop.inject(rampProfile(vU)),
      placeBetV2Mobile.inject(rampProfile(vU)),
      placeBetV2Native.inject(rampProfile(vU))
        .protocols(httpProtocol)
    )
      .maxDuration(runDurationMinutes minutes)
  }

}
