package com.williamhill.whnft.sports.simulations

import com.williamhill.whnft.sports.scenarios.services.BYO._
import io.gatling.core.Predef._

import scala.concurrent.duration._

/**
  * Update by Juri Boiko on 10/07/2019.
  */
class BYOfeature extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(buildYourOdds.inject(atOnceUsers(vU)).protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(buildYourOdds.inject(rampProfile(vU)).protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }

}
