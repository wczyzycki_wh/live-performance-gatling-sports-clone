package com.williamhill.whnft.sports.simulations

import io.gatling.core.Predef._

import scala.concurrent.duration._
import com.williamhill.whnft.sports.scenarios.bets.OBDesktopBetBoostPlacement._

/**
  * Created by Juri Boiko on 17/05/2019.
  */
class OBDesktopBetBoostPlacement extends Simulation with SimulationSettingsTrait {

  if (singleRun) {
    setUp(placeBetBoostDesktopOB.inject(atOnceUsers(vU)).protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }
  else {
    setUp(placeBetBoostDesktopOB.inject(rampProfile(vU)).protocols(httpProtocol)).maxDuration(runDurationMinutes minutes)
  }

}




