package com.williamhill.whnft.sports.scenarios.bets

import java.util.Calendar

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.helpers.UserAgents.setAgentFeedDesktop
import com.williamhill.whnft.sports.requestOBObjects._
import com.williamhill.whnft.sports.requestTrafagarObjects.{HomePage, Login}
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

/**
  * Created by lorenzo on 25/04/2016.
  */

object OBDesktopBetBoostPlacement extends Simulation {

 // val uaListFeeder = csv("UADesktop.csv", escapeChar = '\\')
  val ualistfeeder = csv("UAMobile.csv")
  val usersfeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + ".csv")
  val events = csv("Events.csv")
  val selections = csv("Selections.csv")
  val day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
  val month = Calendar.getInstance().get(Calendar.MONTH) + 1

  val isPT1 : Boolean =
    Common.environmentUnderTest.toUpperCase == "PT1"

  val betURL : String =
    if (isPT1)
      "mbt_slp_gib"
    else
      "mbt_slp"

  val obURL : String =
    if (isPT1)
      "bet_gib"
    else
      "bet"


  val ajaxURL = "mbt_ajax"

  val placeBetBoostDesktopOB =
    scenario("OB Desktop bet placement")
      .forever {
        exec(session => session.reset)
          .exec(flushHttpCache)
          .exec(flushCookieJar)
          .exec(flushSessionCookies)
          .feed(ualistfeeder.random)
          .feed(usersfeeder.random)
          //  .feed(userFeeder.circular)
          .feed(selections.random)
          .exec(session => setAgentFeedDesktop(session))

          // BET_BOOST
          .exec(HomePage.get, Login.login(2),
          BetSlipOBBetBoost.placebetWithBoost(obURL, Common.numberOfBetBoostPerUser))
     //     BetSlipOBBetBoost.doOpenBetsCall("Single To Win (Boosted)"))




        //  .exec(HomePageOB.get(true), LoginOB.login, BetSlipOBBetBoost.placeDoubleBetBoost(betURL, Common.numberOfBetsPerUser), BetSlipOBBetBoost.doOpenBetsCall("Double To Win (Boosted)")) // Single To Win (Boosted)
        //  .exec(HomePageOB.get(true), LoginOB.login, BetSlipOBBetBoost.placeDoubleBetBoost(betURL, Common.numberOfBetsPerUser))
        //  .exec(HomePageOB.get(true), LoginOB.login, BetSlipOBBetBoost.addBoost(betURL, obURL, Common.numberOfBetsPerUser))
        //  .exec(HomePageOB.get(true), LoginOB.login, BetSlipOBBetBoost.addBoostDoEW(betURL, obURL, Common.numberOfBetsPerUser), BetSlipOBBetBoost.doOpenBetsCall("Single EW (Boosted)"))
      }

  def setBetCode (session : Session) : Session = {
    val userAgent = session("UAStrings").toString()
    var betCode : String = ""

    if (userAgent.contains("iPhone")) {
      betCode = "852"
    } else {
      if (userAgent.contains("iPad")) {
        betCode = "851"
      } else {
        if (userAgent.contains("Android 7.0")) {
          betCode = "852"
        } else {
          betCode = "851"
        }
      }
    }
    session.setAll(Map(
      "betCode" -> betCode)
    )
  }


}