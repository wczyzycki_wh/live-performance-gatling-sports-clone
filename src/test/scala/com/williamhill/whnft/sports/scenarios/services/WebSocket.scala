package com.williamhill.whnft.sports.scenarios.services

import io.gatling.core.Predef.{exec, _}
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

object WebSocket extends Simulation{

  def timestamp: Long = System.currentTimeMillis
  val data = Array[Byte]('(')
  val bytes = Array[Byte]('>','s','c','o','r','e','b','o','a','r','d','s','/','v','1','/','O','B','_','E','V','1','3','8','6','8','3','7','4','/','i','n','c','i','d','e','n','t','s')


//  val diffusionPds =
//    scenario("WebSocket diffusion PDS")
//      .exec(ws("Connect WS").open("/pds/diffusion")
//        .queryParam("v", "4")
//        .queryParam("ty", "WB"))
//      .pause(1)
//      .repeat(1000, "i") {
//        exec(ws("Say Topic WS")
//      //    .sendText(session => timestamp.toString)) // PDS/OB_EV13768871
//          .sendText(" PDS/OB_EV*"))
//          .pause(30)
//      }
//      .exec(ws("Close WS").close)

//  val diffusionScoreboard =
//    scenario("WebSocket diffusion Scoreboard")
//      .exec(ws("Connect WS").open("ws://scoreboards-push.williamhill-pp1.com/diffusion")
//      //  .queryParam("v", "9")
//      //  .queryParam("ty", "WB")
//      //  .queryParam("ca", "8")
//      //  .queryParam("r", "60000")
//        )
//      .pause(1)
//      .repeat(1000, "i") {
//        exec(ws("Say Topic WS")
//            .sendBytes(bytes))
//          .pause(30)
//      }
//      .exec(ws("Close WS").close)

      /*
     curl -H 'Host: scoreboardslauncher.williamhill.com'
     cust_auth=6d5d56277a0ea4ca913659f9b21497ac008d65c5f71f0051066aa406288922720c9173959660204c3ac66c6f07;
     username_cookie=WHITA_lurbini;
     CasUsername=WHITA_lurbini;
     wh_device={"is_native":"false","device_os":"ios","os_version":11,"is_tablet":false};
     cust_login=lQzENd6diG0Wz1SN0J4f5qbfdMhXdd2R4dERFmNWvcSwSFUo7+YIv58ls88oc9jz4Q+Y6zE7+0lEli9/4EKQcrZS7Y5vhkqqEGyD+1yxtVBkgyKjlqAjd7uw30MEVEzULEB5n5h8ctSEptzdS9jhgXZWJ8HFJO2n8wVNBHzguHtf5k2ZJE47rXy4Q1XfzWuiWEEP/g==;
     cust_ssl_login=7/MAd9lYVm2PSEnUKLcUzlJc2Y6PzcPJtnE+fXaQ1LpWOjn7aeI/meLa+e6I1vjilBQ4E+dXqWFa7GmteQCLf2BPoFuzZuR8LRas4RGYB9k3OLKk8r9j4C1r;
     CSRF_COOKIE=4a3a8f413c4bde8bf638;
     -H 'user-agent: Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1' -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,
     -H 'referer: https://sports.williamhill.com/betting/en-gb/football/OB_EV13856113/crewe-u23-vs-sheffield-wed-u23'
     -H 'accept-language: en-US,en;q=0.9,et;q=0.8,ru;q=0.7,es;q=0.6'
     --compressed 'https://scoreboardslauncher.williamhill.com/scoreboards/events/OB_EV13856113/launch?lang=en-gb&showSuggestions=true'
       */

}
