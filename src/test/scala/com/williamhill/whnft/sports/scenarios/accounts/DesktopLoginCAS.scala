package com.williamhill.whnft.sports.scenarios.accounts

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.helpers.UserAgents.setAgentFeedDesktop
import com.williamhill.whnft.sports.requestTrafagarObjects._
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

object DesktopLoginCAS extends Simulation {

  val uaListFeeder = csv("UADesktop.csv")
  val userFeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + ".csv")


  val desktopLoginCas =
    scenario("CAS Desktop Login")
      .forever {
        exec(session => session.reset)
          .exec(flushHttpCache)
          .exec(flushCookieJar)
          .exec(flushSessionCookies)
          .feed(uaListFeeder.random)
          .feed(userFeeder.random)
          .exec(session => setAgentFeedDesktop(session))
          .exec(HomePage.getNative)
          .exec(flushHttpCache)
          .exec(flushCookieJar)
          .exec(flushSessionCookies)
          .exec(
            LoginNative.loginNativeGetCas,
            LoginNative.loginNativePostCas(2),
            LoginNative.logoutCas
          )
    }
}

