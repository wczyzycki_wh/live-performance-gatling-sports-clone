package com.williamhill.whnft.sports.scenarios.bets

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.helpers.UserAgents.setAgentFeedMobileWeb
import com.williamhill.whnft.sports.requestTrafagarObjects.BetSlipCashIn.{getAjaxOpenOtherBets, loopAjaxGetBet}
import com.williamhill.whnft.sports.requestTrafagarObjects.{Login, MbtAjax}
import com.williamhill.whnft.sports.scenarios.bets.MobileBetPlacement.ajaxURL
import io.gatling.core.Predef._
import io.gatling.http.Predef.{flushCookieJar, flushHttpCache, flushSessionCookies}


object MobileViewOpenBets {
  val ualistfeeder = csv("UAMobile.csv")
  val usersfeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + ".csv")
  val ajaxURL = "mbt_ajax"

  val mobileViewOpenBets = scenario("View Open Bets - mobile")
    .forever(
      exec(session => session.reset)
        .exec(flushHttpCache)
        .exec(flushCookieJar)
        .exec(flushSessionCookies)
        .feed(ualistfeeder.random)
        .feed(usersfeeder.random)
        .exec(session => setAgentFeedMobileWeb(session))
        .exec(
          Login.login(1),
          MbtAjax.getAjaxOpenBets(ajaxURL)
        )
    )
}
