package com.williamhill.whnft.sports.scenarios.services

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.requestTrafagarObjects.{BuildYourOdds, Login}
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

object BYO extends Simulation{

val ualistfeeder = csv("UANative.csv")
  val usersfeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + ".csv")
  val events = csv("Events.csv")



  val buildYourOdds =
    scenario("BYO")
    .forever{
      exec(session => session.reset)
      .exec(flushHttpCache)
      .exec(flushCookieJar)
      .exec(flushSessionCookies)
        .feed(ualistfeeder.random)
        .feed(usersfeeder.random)
      .feed(events.random)
      .exec(
//        BuildYourOdds.getBYOdata,
        Login.login(1),
        BuildYourOdds.makeCasBYOauth
//        BuildYourOdds.postBYOdata(ElFileBody("byo_add.json")) // http://ctgdata.williamhill-pp1.com/pds2/catalogs/OB_CG0/sports/OB_SP9/byoEventIds

      )
    }

}




