package com.williamhill.whnft.sports.scenarios.services

import com.williamhill.whnft.sports.helpers.{Common, UserAgents}
import com.williamhill.whnft.sports.requestTrafagarObjects.BetSlip
import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import io.gatling.http.Predef._

object BetSlipRendering {

  val uaListFeederDesktop = csv("UADesktop.csv")
  val uaListFeederMobile = csv("UAMobile.csv")
  val baseUri: String = Common.getConfigFromFile("environment.conf", "securebaseURL") + "/data/bss01/skeleton/"

  val betSlipRenderingDesktop: ScenarioBuilder = {
    scenario("Bet Slip Rendering - Desktop")
      .forever {
        exec(session => session.reset)
        exec(flushHttpCache)
          .exec(flushCookieJar)
          .exec(flushSessionCookies)
        feed(uaListFeederDesktop.random)
        .exec(session => UserAgents.setAgentFeedDesktop(session))
          .exec(BetSlip.render(baseUri + "desktop/en-gb"),
            BetSlip.render(baseUri + "desktop/de-de"),
            BetSlip.render(baseUri + "desktop/ja-jp"),
            BetSlip.render(baseUri + "desktop/ru-ru"))
          .pause(2, 3)
      }
  }

  val betSlipRenderingMobile: ScenarioBuilder =
  {
    scenario("Bet Slip Rendering - Mobile")
      .forever {
        exec(session => session.reset)
        exec(flushHttpCache)
          .exec(flushCookieJar)
          .exec(flushSessionCookies)
        feed(uaListFeederMobile.random)
        .exec(session => UserAgents.setAgentFeedMobileWeb(session))
          .exec(session =>
            if (session("platform").as[String].equals("ipad") || session("platform").as[String].equals("android_tablet"))
              session.set("isTablet", true) else session.set("isTablet", false)
          )
          .doIf(session => !session("isTablet").as[Boolean]){
            exec(BetSlip.render(baseUri + "mobile/en-gb"),
            BetSlip.render(baseUri + "mobile/de-de"),
            BetSlip.render(baseUri + "mobile/ja-jp"),
            BetSlip.render(baseUri + "mobile/ru-ru"))
          }
          .doIf(session => session("isTablet").as[Boolean]){
            exec(BetSlip.render(baseUri + "tablet/en-gb"),
              BetSlip.render(baseUri + "tablet/de-de"),
              BetSlip.render(baseUri + "tablet/ja-jp"),
              BetSlip.render(baseUri + "tablet/ru-ru"))
          }
          .pause(2, 3)
      }
  }
}
