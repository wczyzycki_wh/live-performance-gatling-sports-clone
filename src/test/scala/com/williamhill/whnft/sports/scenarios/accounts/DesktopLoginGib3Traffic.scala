package com.williamhill.whnft.sports.scenarios.accounts

import com.williamhill.whnft.sports.requestTrafagarObjects._
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

/**
  * Created by Ana Isabel Fernandez Gomez on 27/02/2019.
  */
object DesktopLoginGib3Traffic extends Simulation{

  //val uaListFeeder = csv("UADesktopSCC.csv", escapeChar = '\\')
  //val userFeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + ".csv")

  val desktopLoginGib3 =
    scenario("Desktop Games")
      .forever {
        exec(session => session.reset)
          .exec(flushHttpCache)
          .exec(flushCookieJar)
          .exec(flushSessionCookies)
          //.feed(uaListFeeder.random)
          //.feed(userFeeder.random)
          .exec(LoginGib3Traffic.games)
      }
}
