package com.williamhill.whnft.sports.scenarios.services

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.requestTrafagarObjects.{HomePage, Login, Messages}
import com.williamhill.whnft.sports.scenarios.bets.DesktopBetPlacement.{ualistfeeder, usersfeeder}
import io.gatling.core.Predef.csv
import io.gatling.core.Predef._
import io.gatling.http.Predef.{flushCookieJar, flushHttpCache, flushSessionCookies}


object DesktopViewMessages {

  val ualistfeeder = csv("UADesktop.csv")
  val usersfeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + ".csv")

  val desktopViewMessages = scenario("View messages - desktop")
    .forever
    {
      exec(session => session.reset)
        .exec(flushHttpCache)
        .exec(flushCookieJar)
        .exec(flushSessionCookies)
        .feed(ualistfeeder.random)
        .feed(usersfeeder.random)
        .exec(
          Login.login(1),
          Messages.goToInbox)
    }

}
