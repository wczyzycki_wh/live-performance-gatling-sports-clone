package com.williamhill.whnft.sports.scenarios.pages

import com.williamhill.whnft.sports.helpers.Common
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

object NavigateToOfferClubPage extends Simulation{

  val ualistfeeder = csv("UADesktop.csv")
  val usersfeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + ".csv")

  val navigateToOfferClubPage =
    scenario("Navigate to offer-club page")
    .forever{
      exec(session => session.reset)
      .exec(flushHttpCache)
      .exec(flushCookieJar)
      .exec(flushSessionCookies)
      .feed(ualistfeeder.random)
      .feed(usersfeeder.random)
      .exec(
        // HomePage.get,
        // Login.login,
        // PageViews.getOfferClubFlow
      )
    }

}




