package com.williamhill.whnft.sports.scenarios.accounts

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.helpers.UserAgents.setAgentFeedNative
import com.williamhill.whnft.sports.requestTrafagarObjects._
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

/**
  * Created by Juri Boiko on 25/04/2019.
  */
object NativeLoginCAS extends Simulation {

  val ualistfeeder = csv("UANative.csv")
  val usersfeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + ".csv")


  val nativeLoginCas =
    scenario("Native Login CAS")
    .forever {
      exec(session => session.reset)
      .exec(flushHttpCache)
      .exec(flushCookieJar)
      .exec(flushSessionCookies)
      .feed(ualistfeeder.random)
      .feed(usersfeeder.random)
        .exec(session => setAgentFeedNative(session))
      .exec(HomePage.getNative)
        .exec(flushHttpCache)
          .exec(flushCookieJar)
          .exec(flushSessionCookies)
          .exec(LoginNative.loginNativeGetCas, LoginNative.loginNativePostCas(2))
    }
}

