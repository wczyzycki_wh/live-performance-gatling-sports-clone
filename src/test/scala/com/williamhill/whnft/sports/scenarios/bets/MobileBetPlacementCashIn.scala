package com.williamhill.whnft.sports.scenarios.bets

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.helpers.Common._
import com.williamhill.whnft.sports.helpers.UserAgents.setAgentFeedMobileWeb
import com.williamhill.whnft.sports.requestTrafagarObjects._
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

/**
  * Created by juri on 25/12/2018.
  */

object MobileBetPlacementCashIn extends Simulation {


  val ualistfeeder = csv("UAMobile.csv")
  val usersfeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + ".csv")
  val selections = csv("Selections.csv")

  val betURL = "mbt_slp"
  val ajaxURL = "mbt_ajax"

  val cashInplaceBetMobile =
    scenario("CashIn bet placement")
      .forever {
        exec(session => session.reset)
          .exec(flushHttpCache)
          .exec(flushCookieJar)
          .exec(flushSessionCookies)
          .feed(ualistfeeder.random)
          .feed(usersfeeder.random)
          .feed(selections.random)
          .exec(session => setBetCode(session))
          .exec(session => setAgentFeedMobileWeb(session))
          .exec(HomePage.get, Login.login(2))
          .doIf(session => userLoggedIn(session)) {
            exec(
              BetSlipCashIn.placeBet(betURL,
                Common.numberOfBetsPerUser, ajaxURL)
            )
              .repeat(3) {
                exec(Login.getBalanceSSL)
              }
          }
      }

  def setBetCode (session : Session) : Session = {
    val userAgent = session("UAStrings").toString()
    var betCode : String = ""

    if (userAgent.contains("iPhone")) {
      betCode = "852"
    } else {
      if (userAgent.contains("iPad")) {
        betCode = "851"
      } else {
        if (userAgent.contains("Android 7.0")) {
          betCode = "852"
        } else {
          betCode = "851"
        }
      }
    }
    session.setAll(Map(
      "betCode" -> betCode)
    )
  }
}
