package com.williamhill.whnft.sports.scenarios.bets

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.helpers.UserAgents.setAgentFeedDesktop
import com.williamhill.whnft.sports.requestTrafagarObjects.BetSlip.BetType
import com.williamhill.whnft.sports.requestTrafagarObjects._
import com.williamhill.whnft.sports.requestTrafagarObjects.BetSlipV2
import com.williamhill.whnft.sports.simulations.SimulationSettingsTrait
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

object DesktopBetPlacement extends Simulation with SimulationSettingsTrait {

  val ualistfeeder = csv("UADesktop.csv")
  val usersfeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + ".csv")
  val selections = sys.env.get("inPlay") match {
    case Some("true") => csv("InPlaySelections.csv")
    case _ => csv("Selections.csv")
  }
  val multiEventSelections = csv("multi-event-selections.csv")

  val betURL = "slp"
  val fromDate = 0

  val ajaxURL = "ajax"

  val placeBetV2Desktop =
    scenario("BetSlip v2 bet placement for " + betType)
      .repeat(1)(
        exec(session => session.reset)
          .exec(flushHttpCache)
          .exec(flushCookieJar)
          .exec(flushSessionCookies)
          .feed(ualistfeeder.random)
          .feed(usersfeeder.random)
          .feed(selections.queue.circular)
          .exec(session => setBetCode(session))
          .exec(session => setBetType(session, betType))
          .exec(session => setAgentFeedDesktop(session))
          .exec(
            HomePage.navigateToSportsHomeBetSlipV2,
            Login.loginBetSlipV2(2),
            BetSlipV2.makeBet(betType)
          )
      )

  val placeBetDesktop =
    scenario("Desktop bet placement")
      .forever {
        exec(session => session.reset)
          .exec(flushHttpCache)
          .exec(flushCookieJar)
          .exec(flushSessionCookies)
          .feed(ualistfeeder.random)
          .feed(usersfeeder.random)
          .feed(selections.queue.circular)
          .exec(session => setBetCode(session))
          .exec(session => setAgentFeedDesktop(session))
          .exec(
            HomePage.get,
            HomePage.getDataFiles,
            Login.login(2),
            //        MbtAjax.getAjaxOpenBets(ajaxURL),
            BetSlip.placeBet(betURL, BetType.Single, Common.numberOfBetsPerUser)
          )
          .exec(Login.getBalanceSSL)
      }

  val placeDoubleBetDesktop =
    scenario("Desktop bet placement for doubles")
      .forever {
        exec(session => session.reset)
          .exec(flushHttpCache)
          .exec(flushCookieJar)
          .exec(flushSessionCookies)
          .feed(ualistfeeder.random)
          .feed(usersfeeder.random)
          .feed(multiEventSelections.queue.circular, 2)
          .exec(session => setBetCode(session))
          .exec(session => setAgentFeedDesktop(session))
          .exec(HomePage.get,
            Login.login(2),
            BetSlip.placeBet(betURL, BetType.Double, Common.numberOfBetsPerUser))
          .exec(Login.getBalanceSSL)
      }

  val placeTrebleBetDesktop =
    scenario("Desktop bet placement for trebles")
      .forever {
        exec(session => session.reset)
          .exec(flushHttpCache)
          .exec(flushCookieJar)
          .exec(flushSessionCookies)
          .feed(ualistfeeder.random)
          .feed(usersfeeder.random)
          .feed(multiEventSelections.queue.circular, 3)
          .exec(session => setBetCode(session))
          .exec(session => setAgentFeedDesktop(session))
          .exec(HomePage.get,
            Login.login(2),
            BetSlip.placeBet(betURL, BetType.Treble, Common.numberOfBetsPerUser))
          .exec(Login.getBalanceSSL)
      }

  def setBetCode (session : Session) : Session = {
    session.setAll(Map(
      "betCode" -> "850")
    )
  }

  def setBetType (session : Session, betType: BetType.Value): Session = {
    session.setAll(Map{
      "betType" -> betType.toString.toUpperCase
    })
  }
}
