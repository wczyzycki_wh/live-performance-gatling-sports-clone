package com.williamhill.whnft.sports.scenarios.accounts

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.requestOBObjects.OBOxiCall
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

/**
 * Created by Smija Alex on 12/02/2020.
 */
object OxiScenario extends Simulation{


  val usersfeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + ".csv")

  val Scn_OBAccountValidate2 =
    scenario("OB reqAccountValidate2")
      .exec(session => session.reset)
      .exec(flushHttpCache)
      .exec(flushCookieJar)
      .exec(flushSessionCookies)
      .feed(usersfeeder.random)
      .exec( OBOxiCall.reqAccountValidate2)

  val Scn_reqAccountGetDetail =
    scenario("OB reqAccountGetDetail")
      .exec(session => session.reset)
      .exec(flushHttpCache)
      .exec(flushCookieJar)
      .exec(flushSessionCookies)
      .feed(usersfeeder.random)
      .exec( OBOxiCall.reqAccountValidate2, OBOxiCall.reqAccountGetDetail)

  val Scn_reqAcceptTermsAndConditions =
    scenario("OB reqAccountAcceptTnC")
      .exec(session => session.reset)
      .exec(flushHttpCache)
      .exec(flushCookieJar)
      .exec(flushSessionCookies)
      .feed(usersfeeder.random)
      .exec(OBOxiCall.reqAccountValidate2,
            OBOxiCall.reqSetAccountStatusFlag("TNC", "A", "TNC Flag"),
            OBOxiCall.reqAccountAcceptTnC)



}
