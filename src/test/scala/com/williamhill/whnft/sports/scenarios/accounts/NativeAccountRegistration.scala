package com.williamhill.whnft.sports.scenarios.accounts

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.helpers.UserAgents.setAgentFeedNative
import com.williamhill.whnft.sports.requestTrafagarObjects._
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

object NativeAccountRegistration extends Simulation {

  val uaListFeeder = csv("UANative.csv")
  val userFeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + ".csv")

  var productSource = "SM" //IPhone

  if (uaListFeeder.toString().contains(("iPad")))
    productSource = "ST"

  val registerNativeAccount =
    scenario("Account registration - Native")
      .forever {
        exec(session => session.reset)
          .exec(flushHttpCache)
          .exec(flushCookieJar)
          .exec(flushSessionCookies)
          .feed(uaListFeeder.random)
          .feed(userFeeder.random)
          .exec(session => setAgentFeedNative(session))
          .exec(AccountRegistration.clickJoin(productSource, "native"), AccountRegistration.accountRegistration(productSource))
      }



}
