package com.williamhill.whnft.sports.scenarios.bets

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.helpers.UserAgents._
import com.williamhill.whnft.sports.requestTrafagarObjects.BetSlip.BetType
import com.williamhill.whnft.sports.requestTrafagarObjects.{BetSlipV2, _}
import com.williamhill.whnft.sports.simulations.SimulationSettingsTrait
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._


object BetPlacementV2 extends Simulation with SimulationSettingsTrait {

  val uaDesktopListFeeder = csv("UADesktop.csv")
  val uaMobileFeeder = csv("UAMobile.csv")
  val uaNativeFeeder = csv("UADesktop.csv")
  val userFeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + ".csv")
  val selectionFeeder = csv("Selections.csv")
  val multipleSelections = csv("multi-event-selections.csv")
  val inPlaySelections = csv("InPlaySelections.csv")

  val placeBetV2Desktop =
    scenario("Bet Placement Desktop")
      .feed(uaDesktopListFeeder)
      .exec(session => session.set("device", "desktop"))
      .exec(setAgentFeedDesktop(_))
      .exec(setBetCodeDesktop(_))
      .exec(placeBetV2)

  val placeBetV2Mobile =
    scenario("Bet Placement Mobile")
      .feed(uaMobileFeeder)
      .exec(session => session.set("device", "mobile"))
      .exec(setAgentFeedMobileWeb(_))
      .exec(setBetCodeMobile(_))
      .exec(placeBetV2)

  val placeBetV2Native =
    scenario("Bet Placement Native")
      .feed(uaNativeFeeder)
      .exec(session => session.set("device", "native"))
      .exec(setAgentFeedNative(_))
      .exec(setBetCodeNative(_))
      .exec(placeBetV2)


  def placeBetV2: ChainBuilder =
    exec(flushHttpCache)
      .exec(flushCookieJar)
      .exec(flushSessionCookies)
      .feed(userFeeder.random)
      .doIf(inPlay) {
        feed(inPlaySelections.circular)
      }
      .doIf(BetType.Single.equals(betType) && !inPlay) {
        feed(selectionFeeder.circular)
      }
      .doIf(BetType.Double.equals(betType)) {
        feed(multipleSelections.circular, 2)
      }
      .doIf(BetType.Treble.equals(betType)) {
        feed(multipleSelections.circular, 3)
      }
      .exec(session =>
        session.set("selectionArray", buildSelectionArray(session))
      )
      .exec(setBetType(_, betType))
      .exec(
        HomePage.navigateToSportsHomeBetSlipV2,
        Login.loginBetSlipV2(2),
        BetSlipV2.makeBet(betType)
      )

  def setBetCodeDesktop(session: Session): Session = {
    session.setAll(Map(
      "betCode" -> "850")
    )
  }

  def buildSelectionArray(session: Session): String = {
    var selectionIds: String = ""
    session("SelectionID").asOption[String] match {
      case Some(s: String) => selectionIds = selectionIds.concat(s"""["${s}"]""")
      case None => ""
    }
    session("SelectionID1").asOption[String] match {
      case Some(s: String) => selectionIds = selectionIds.concat(s"""["${s}"]""")
      case None => ""
    }
    session("SelectionID2").asOption[String] match {
      case Some(s: String) => selectionIds = selectionIds.concat(s""",["${s}"]""")
      case None => ""
    }
    session("SelectionID3").asOption[String] match {
      case Some(s: String) => selectionIds = selectionIds.concat(s""",["${s}"]""")
      case None => ""
    }
    selectionIds
  }

  def setBetCodeMobile(session: Session): Session = {
    val userAgent = session("UAStrings").toString()
    var betCode: String = ""

    if (userAgent.contains("iPhone")) {
      betCode = "852"
    } else {
      if (userAgent.contains("iPad")) {
        betCode = "851"
      } else {
        if (userAgent.contains("tab")) {
          betCode = "851"
        } else {
          betCode = "852"
        }
      }
    }
    session.setAll(Map(
      "betCode" -> betCode
    )
    )
  }

  def setBetCodeNative(session: Session): Session = {
    val userAgent = session("UAStrings").toString()
    var betCode: String = ""

    if (userAgent.contains("iPhone")) {
      betCode = "800"
    } else {
      if (userAgent.contains("iPad")) {
        betCode = "801"
      } else {
        if (userAgent.contains("Android 7.0")) {
          betCode = "810"
        } else {
          betCode = "811"
        }
      }
    }
    session.setAll(Map(
      "betCode" -> betCode)
    )
  }

  def setBetType(session: Session, betType: BetType.Value): Session = {
    session.setAll(Map {
      "betType" -> betType.toString.toUpperCase
    })
  }

}
