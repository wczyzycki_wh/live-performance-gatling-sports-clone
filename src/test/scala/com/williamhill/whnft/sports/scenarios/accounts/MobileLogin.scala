package com.williamhill.whnft.sports.scenarios.accounts

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.helpers.UserAgents.setAgentFeedMobileWeb
import com.williamhill.whnft.sports.requestTrafagarObjects._
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

object MobileLogin extends Simulation{

  val uaListFeeder = csv("UAMobile.csv")
  val userFeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + ".csv")

  val mobileLogin =
    scenario("Mobile Login")
      .forever {
        exec(session => session.reset)
          .exec(flushHttpCache)
          .exec(flushCookieJar)
          .exec(flushSessionCookies)
          .feed(uaListFeeder.random)
          .feed(userFeeder.random)
        .exec(session => setAgentFeedMobileWeb(session))
      .exec(HomePage.getNative, Login.login(2)
     //   AccountCasCalls.casAoaOrchestrate,
     //   AccountCasCalls.streamingCasCall,
     //   AccountCasCalls.transactCasCall
      )
    }

}
