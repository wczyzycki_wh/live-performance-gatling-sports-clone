package com.williamhill.whnft.sports.scenarios.services

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.helpers.Common.userLoggedIn
import com.williamhill.whnft.sports.helpers.UserAgents.setAgentFeedDesktop
import com.williamhill.whnft.sports.requestTrafagarObjects.BetSlip.BetType
import com.williamhill.whnft.sports.requestTrafagarObjects._
import com.williamhill.whnft.sports.simulations.SimulationSettingsTrait
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

/**
  * Created by Juri Boiko on 25/04/2019.
  */
object DesktopRewards extends Simulation with SimulationSettingsTrait {

  val ualistfeeder = csv("UADesktop.csv")
  val usersfeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + ".csv")
  val selections = csv("Selections.csv")

  var betCode = "850"
  val betURL = "slp"

  val desktopRewards =
    scenario("Desktop Rewards and bet placement")
      .forever {
        exec(session => session.reset)
          .exec(flushHttpCache)
          .exec(flushCookieJar)
          .exec(flushSessionCookies)
          .feed(ualistfeeder.random)
          .feed(usersfeeder.random)
          .feed(selections.random)
          .exec(session => setAgentFeedDesktop(session))
          .exec(HomePage.get, Login.login(2))
          .doIf(session => userLoggedIn(session)) {
            exec(
              Rewards.accountRewards,
              BetSlip.placeBet(betURL, BetType.Single, Common.numberOfBetsPerUser)
            )
          }
      }
}
