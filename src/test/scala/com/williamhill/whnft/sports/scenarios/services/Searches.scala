package com.williamhill.whnft.sports.scenarios.services

import com.williamhill.whnft.sports.helpers.UserAgents.setAgentFeedDesktop
import com.williamhill.whnft.sports.requestTrafagarObjects._
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

object Searches extends Simulation{

  val ualistfeeder = csv("UADesktop.csv")

  val mixedSearchScenarios =
    scenario("Various search scenarios - Desktop")
    .forever{
      exec(session => session.reset)
        .exec(flushHttpCache)
        .exec(flushCookieJar)
        .exec(flushSessionCookies)
        .feed(ualistfeeder.random)
        .exec(session => setAgentFeedDesktop(session))
        .exec(
          SearchFunctionality.makeRealisticSearch,
          SearchFunctionality.generateCacheMisses
        )
    }

  val sportsBookSearch =
    scenario("SportsBook search")
      .exec(session => session.reset)
      .exec(flushHttpCache)
      .exec(flushCookieJar)
      .exec(flushSessionCookies)
      .feed(ualistfeeder.random)
      .exec(session => setAgentFeedDesktop(session))
      .repeat(2) {
        exec(
          SearchFunctionality.sportsBookSearch(sys.env.getOrElse("searchQuery", "horse"))
        )
      }

}




