package com.williamhill.whnft.sports.scenarios.services

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.helpers.UserAgents.setAgentFeedNative
import com.williamhill.whnft.sports.requestTrafagarObjects._
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

object StreamingFullTest extends Simulation {

val ualistfeeder = csv("UANative.csv")
  val usersfeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + ".csv")
  val events = csv("HorseRacingEvents.csv")
  val selections = csv("HorseRacingSelections.csv")


  val streamingFullTest =
    scenario(" Horse Racing ")
    .forever{
      exec(session => session.reset)
      .exec(flushHttpCache)
      .exec(flushCookieJar)
      .exec(flushSessionCookies)
      .feed(ualistfeeder.random)
      .feed(usersfeeder.random)
      .feed(events.random)
        .feed(selections.random)
        .exec(session => setBetCode(session))
        .exec(session => setAgentFeedNative(session))
      .exec(
        Login.login(1),
//        getHorseRacingAntePost,
//        getHorseRacingAntePostPartial,
//        getHorseRacingSpecials,
//        getHorseRacingSpecialsPartial,
//        getHorseRacingMeetings,
//        getHorseRacingMeetingsPartial,
//        getTopBetsHorseRacing,
        StreamingScenarios.loopStreamingSteps(1)
      )
    }


  val streamingCheckAccess =
    scenario ( "Check Horse Racing stream access" )
    .forever{
      exec(session => session.reset)
        .exec(flushHttpCache)
        .exec(flushCookieJar)
        .exec(flushSessionCookies)
        .feed(ualistfeeder.random)
        .feed(usersfeeder.random)
        .feed(events.random)
        .exec(session => setAgentFeedNative(session))
        .exec(
          Login.login(1),
          StreamingScenarios.getStreamingEvent
        )
    }


  val racingTVStreamTest =
    scenario("Access Racing TV")
    .forever {
      exec(session => session.reset)
        .exec(flushHttpCache)
        .exec(flushCookieJar)
        .exec(flushSessionCookies)
        .feed(ualistfeeder.random)
        .feed(usersfeeder.random)
        .exec(session => setAgentFeedNative(session))
        .exec(
          Login.login(1),
          StreamingScenarios.getRacingTVStream
        )
    }

  def setBetCode (session : Session) : Session = {
    val userAgent = session("UAStrings").toString()
    var betCode : String = ""

    if (userAgent.contains("iPhone")) {
      betCode = "852"
    } else {
      if (userAgent.contains("iPad")) {
        betCode = "851"
      } else {
        if (userAgent.contains("tab")) {
          betCode = "851"
        } else {
          betCode = "852"
        }
      }
    }
    session.setAll(Map(
      "betCode" -> betCode
    )
    )
  }

}




