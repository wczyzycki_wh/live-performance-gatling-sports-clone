package com.williamhill.whnft.sports.scenarios.accounts

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.helpers.UserAgents.setAgentFeedDesktop
import com.williamhill.whnft.sports.requestTrafagarObjects._
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

object DesktopLoginSCC extends Simulation {

  val uaListFeeder = csv("UADesktopSCC.csv")
  val userFeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + ".csv")

  val desktopLoginSCC =
    scenario("Desktop Riga SCC")
      .forever {
        exec(session => session.reset)
          .exec(flushHttpCache)
          .exec(flushCookieJar)
          .exec(flushSessionCookies)
          .feed(uaListFeeder.random)
          .feed(userFeeder.random)
          .exec(session => setAgentFeedDesktop(session))
          .exec(LoginSCC.getAccount, LoginSCC.getBalance, LoginSCC.getToken,
            LoginSCC.getSystemConfiguration, LoginSCC.getExchangeRatesGibEur,
            LoginSCC.getExchangeRatesEurGib, LoginSCC.getHealthcheck)
      }
}
