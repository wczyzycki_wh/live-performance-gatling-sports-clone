package com.williamhill.whnft.sports.scenarios.pages

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.helpers.UserAgents.setAgentFeedNative
import com.williamhill.whnft.sports.requestTrafagarObjects._
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

object NavigateToFootballPagesNative extends Simulation{

val ualistfeeder = csv("UANative.csv")
  val usersfeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + ".csv")
  val events = csv("Events.csv")

  val navigateToFootballPagesNative =
    scenario("Navigate to Football pages Native")
    .forever{
      exec(session => session.reset)
      .exec(flushHttpCache)
      .exec(flushCookieJar)
      .exec(flushSessionCookies)
      .feed(ualistfeeder.random)
      .feed(usersfeeder.random)
      .feed(events.random)
        .exec(session => setAgentFeedNative(session))
      .exec(
        HomePage.getNative,
        PageViews.getFootballHighlights,
        PageViews.getFootballMatches,
        PageViews.getFootballCompetitions,
        PageViews.getEvent,
        PageViews.getInplay,

      )
    }

}




