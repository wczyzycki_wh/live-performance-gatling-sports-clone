package com.williamhill.whnft.sports.scenarios.accounts

import java.util.Calendar

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.helpers.UserAgents.setAgentFeedDesktop
import com.williamhill.whnft.sports.requestOBObjects._
import com.williamhill.whnft.sports.requestTrafagarObjects._
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

/**
  * Created by Juri Boiko on 25/04/2019.
  */

object OBAccountHistory extends Simulation {

  val ualistfeeder = csv("UADesktop.csv")
  val usersfeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + "OB.csv")

  val day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
  val month = Calendar.getInstance().get(Calendar.MONTH) + 1

  val obAccountStatement =
    scenario("OB Account Statement")
    .forever {
      exec(session => session.reset)
      .exec(flushHttpCache)
      .exec(flushCookieJar)
      .exec(flushSessionCookies)
      .feed(ualistfeeder.random)
      .feed(usersfeeder.random)
        .exec(session => setAgentFeedDesktop(session))
      .exec(
        HomePage.get, Login.login(2)
      )
      .doIf(session => session.contains("custLogin")) {
        exec(
          HomePage.get,
          AccountStatementsOB.clickMyAccount,
          AccountStatementsOB.getAccHistory(day, month, Common.numberOfAccountStatements),
          LogoutOB.logout
        )
      }
    }
}
