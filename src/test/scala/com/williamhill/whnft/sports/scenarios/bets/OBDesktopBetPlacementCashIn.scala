package com.williamhill.whnft.sports.scenarios.bets

import java.util.Calendar

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.helpers.UserAgents.setAgentFeedDesktop
import com.williamhill.whnft.sports.requestOBObjects._
import com.williamhill.whnft.sports.requestTrafagarObjects._
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

/**
  * Created by Juri Boiko on 25/04/2019.
  */

object OBDesktopBetPlacementCashIn extends Simulation {

  val ualistfeeder = csv("UADesktop.csv")
  val usersfeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + "OB.csv")
  val events = csv("Events.csv")
  val selections = csv("Selections.csv")
  val day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
  val month = Calendar.getInstance().get(Calendar.MONTH) + 1

  val isPT1 : Boolean =
    Common.environmentUnderTest.toUpperCase == "PT1"

  val betURL : String =
    if (isPT1)
      "slp_gib"
    else
      "slp"

  val ajaxURL = "ajax"

  val placeBetDesktopCashInOB =
    scenario("OB Desktop bet placement and Cash In")
      .forever {
        exec(session => session.reset)
          .exec(flushHttpCache)
          .exec(flushCookieJar)
          .exec(flushSessionCookies)
          .feed(ualistfeeder.random)
          .feed(usersfeeder.random)
          .feed(events.random)
          .feed(selections.random)
          .exec(session => setAgentFeedDesktop(session))
          .exec(HomePage.get, Login.login(2),
          //  BetSlipOB.placebet(betURL, Common.numberOfBetsPerUser),
            BetSlipCashInOB.placeBet(betURL, Common.numberOfBetsPerUser, ajaxURL),
            LogoutOB.logout
          )
      }
}
