package com.williamhill.whnft.sports.scenarios.pages

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.helpers.UserAgents.setAgentFeedMobileWeb
import com.williamhill.whnft.sports.requestTrafagarObjects._
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

object NavigateToFootballPages extends Simulation{

  val ualistfeeder = csv("UAMobile.csv")
  val usersfeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + ".csv")
  val events = csv("Events.csv")

  val navigateToFootballPages =
    scenario("Navigate to Football pages")
    .forever{
      exec(session => session.reset)
      .exec(flushHttpCache)
      .exec(flushCookieJar)
      .exec(flushSessionCookies)
      .feed(ualistfeeder.random)
      .feed(usersfeeder.random)
      .feed(events.random)
        .exec(session => setAgentFeedMobileWeb(session))
      .exec(
        HomePage.get,
        PageViews.getFootballHighlights,
        PageViews.getFootballHighlightsPartial,
        PageViews.getFootballMatches,
        PageViews.getFootballMatchesPartial,
        PageViews.getFootballCompetitions,
        PageViews.getFootballCompetitionsPartial,
        PageViews.getEvent,
        PageViews.getEventPartial,
        PageViews.getInplay,
        PageViews.getInplayPartial,
        PageViews.getTopBetsApps,
        PageViews.getTopBets
    //    PageViews.getStaticBetting30,
    //    PageViews.getGrandNational,
    //    PageViews.getAuthRSP

      )
    }

}




