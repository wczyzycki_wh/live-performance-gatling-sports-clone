package com.williamhill.whnft.sports.scenarios.bets

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.helpers.UserAgents.setAgentFeedNative
import com.williamhill.whnft.sports.requestTrafagarObjects.BetSlip.BetType
import com.williamhill.whnft.sports.requestTrafagarObjects._
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

object NativeBetPlacement extends Simulation {

val ualistfeeder = csv("UANative.csv")
  val usersfeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + ".csv")
  val selections = sys.env.get("inPlay") match {
    case Some("true") => csv("InPlaySelections.csv")
    case _ => csv("Selections.csv")
  }
  val horseRacingSelections = csv("HorseRacingSelections.csv")
  val multiEventSelections = csv("multi-event-selections.csv")

  val betURL = "mbt_slp"
  val ajaxURL = "mbt_ajax"

  val placeBetNative =
    scenario("Native bet placement")
      .forever("loop") {
        exec(session => session.reset)
          .exec(flushHttpCache)
          .exec(flushCookieJar)
          .exec(flushSessionCookies)
          .feed(ualistfeeder.random)
          .feed(usersfeeder.random)
          .feed(selections.queue.circular)
          .exec(session => setBetCode(session))
          .exec(session => setAgentFeedNative(session))
          .exec(HomePage.getNative,
            Login.login(2),
            BetSlip.placeBet(betURL, BetType.Single, Common.numberOfBetsPerUser),
            Login.getBalanceSSL)
      }

  val placeDoubleBetNative =
    scenario("Native bet placement for doubles")
      .forever("loop") {
        exec(session => session.reset)
          .exec(flushHttpCache)
          .exec(flushCookieJar)
          .exec(flushSessionCookies)
          .feed(ualistfeeder.random)
          .feed(usersfeeder.random)
          .feed(multiEventSelections.queue.circular, 2)
          .exec(session => setBetCode(session))
          .exec(session => setAgentFeedNative(session))
          .exec(HomePage.getNative,
            Login.login(2),
            BetSlip.placeBet(betURL, BetType.Double, Common.numberOfBetsPerUser),
            Login.getBalanceSSL)
      }

  val placeTrebleBetNative =
    scenario("Native bet placement for trebles")
      .forever("loop") {
        exec(session => session.reset)
          .exec(flushHttpCache)
          .exec(flushCookieJar)
          .exec(flushSessionCookies)
          .feed(ualistfeeder.random)
          .feed(usersfeeder.random)
          .feed(multiEventSelections.queue.circular, 3)
          .exec(session => setBetCode(session))
          .exec(session => setAgentFeedNative(session))
          .exec(HomePage.getNative,
            Login.login(2),
            BetSlip.placeBet(betURL, BetType.Treble, Common.numberOfBetsPerUser),
            Login.getBalanceSSL)
      }

  val placeBetNative_HorseRacing =
    scenario("Native bet placement - Horse Racing")
      .forever("loop") {
        exec(session => session.reset)
          .exec(flushHttpCache)
          .exec(flushCookieJar)
          .exec(flushSessionCookies)
          .feed(ualistfeeder.random)
          .feed(usersfeeder.random)
          .feed(horseRacingSelections.random)
          .exec(session => setBetCode(session))
          .exec(session => setAgentFeedNative(session))
          .exec(HomePage.getNative,
            Login.login(2),
            //            MbtAjax.getAjaxOpenBets(ajaxURL),
            BetSlip.placeBet(betURL, BetType.Single, Common.numberOfBetsPerUser),
            Login.getBalanceSSL)
      }

  def setBetCode (session : Session) : Session = {
    val userAgent = session("UAStrings").toString()
    var betCode : String = ""

    if (userAgent.contains("iPhone")) {
      betCode = "800"
    } else {
      if (userAgent.contains("iPad")) {
        betCode = "801"
      } else {
        if (userAgent.contains("Android 7.0")) {
          betCode = "810"
        } else {
          betCode = "811"
        }
      }
    }
    session.setAll(Map(
      "betCode" -> betCode)
    )
  }

}
