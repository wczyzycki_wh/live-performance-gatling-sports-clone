package com.williamhill.whnft.sports.scenarios.services

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.helpers.UserAgents.setAgentFeedDesktop
import com.williamhill.whnft.sports.requestTrafagarObjects._
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

object DesktopDeposit extends Simulation {

  val ualistfeeder = csv("UADesktop.csv")
  val usersfeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + ".csv")

  val desktopDeposit =
    scenario("Make Deposit - desktop")
      .forever {
        exec(session => session.reset)
        .exec(flushHttpCache)
        .exec(flushCookieJar)
        .exec(flushSessionCookies)
        .feed(ualistfeeder.random)
        .feed(usersfeeder.random)
          .exec(session => setAgentFeedDesktop(session))
        .exec(HomePage.get, Login.login(2), Deposit.deposit("acc", "DP", "ext", Common.numberOfDepositsPerUser))
        // TODO: Make these calls only for PP!, Deposit.depositAfterCall1, Deposit.depositAfterCall2)
      }

}
