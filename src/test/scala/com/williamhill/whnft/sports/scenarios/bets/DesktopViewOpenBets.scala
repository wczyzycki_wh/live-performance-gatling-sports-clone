package com.williamhill.whnft.sports.scenarios.bets

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.requestTrafagarObjects.BetSlipCashIn.{getAjaxOpenOtherBets, loopAjaxGetBet}
import com.williamhill.whnft.sports.requestTrafagarObjects.Login
import io.gatling.core.Predef._
import io.gatling.http.Predef.{flushCookieJar, flushHttpCache, flushSessionCookies}


object DesktopViewOpenBets {
  val ualistfeeder = csv("UADesktop.csv")
  val usersfeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + ".csv")
  val ajaxURL = "ajax"

  val desktopViewOpenBets = scenario("View Open Bets - desktop")
    .forever(
      exec(session => session.reset)
        .exec(flushHttpCache)
        .exec(flushCookieJar)
        .exec(flushSessionCookies)
        .feed(ualistfeeder.random)
        .feed(usersfeeder.random)
        .exec(
          Login.login(1),
          loopAjaxGetBet(3, ajaxURL),
          getAjaxOpenOtherBets(ajaxURL)
        )
    )
}
