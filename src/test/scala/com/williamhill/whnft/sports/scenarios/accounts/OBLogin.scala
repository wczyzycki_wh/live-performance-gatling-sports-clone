package com.williamhill.whnft.sports.scenarios.accounts

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.helpers.UserAgents.setAgentFeedDesktop
import com.williamhill.whnft.sports.requestOBObjects.{HomePageOB, LoginOB, LogoutOB}
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

/**
  * Created by Juri Boiko on 25/04/2019.
  */
object OBLogin extends Simulation{

  val ualistfeeder = csv("UADesktop.csv")
  val usersfeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + "OB.csv")

  val oBLogin =
    scenario("OB Login")
    .forever {
      exec(session => session.reset)
      .exec(flushHttpCache)
      .exec(flushCookieJar)
      .exec(flushSessionCookies)
      .feed(ualistfeeder.random)
      .feed(usersfeeder.random)
        .exec(session => setAgentFeedDesktop(session))
      .exec(HomePageOB.get(true), LoginOB.login, LogoutOB.logout)
    }

}
