package com.williamhill.whnft.sports.scenarios.pages

import com.williamhill.whnft.sports.helpers.UserAgents.setAgentFeedDesktop
import com.williamhill.whnft.sports.requestTrafagarObjects._
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

/**
  * Created by Juri Boiko on 22/02/2017.
  */
object DesktopHomepage extends Simulation{

  val ualistfeeder = csv("UADesktop.csv")
//  val userFeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + ".csv")

  val desktopHomePage =
    scenario("Desktop Homepage")
//    .forever {
      .exec(session => session.reset)
      .exec(flushHttpCache)
      .exec(flushCookieJar)
      .exec(flushSessionCookies)
      .feed(ualistfeeder.random)
      .exec(session => setAgentFeedDesktop(session))
//      .feed(userFeeder.random)
      .exec(HomePage.get)
//    }
}
