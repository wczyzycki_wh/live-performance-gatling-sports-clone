package com.williamhill.whnft.sports.scenarios.accounts

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.helpers.UserAgents.setAgentFeedMobileWeb
import com.williamhill.whnft.sports.requestTrafagarObjects._
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

object MobileAccountRegistration extends Simulation{

  val uaListFeeder = csv("UAMobile.csv")
  val userFeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + ".csv")

  var productSource = "SM" // Android

  if (uaListFeeder.toString().contains(("iPhone"))) {
    productSource = "SM"
  } else if (uaListFeeder.toString().contains(("iPad"))) {
    productSource = "ST"
  } else if (uaListFeeder.toString().contains(("Nexus 7"))) {
    productSource = "ST"
  }

  val registerMobileAccount =
    scenario("Account registration - Mobile")
    .forever {
      exec(session => session.reset)
        .exec(flushHttpCache)
        .exec(flushCookieJar)
        .exec(flushSessionCookies)
        .feed(uaListFeeder.random)
        .feed(userFeeder.random)
        .exec(session => setAgentFeedMobileWeb(session))
      .exec(AccountRegistration.clickJoin(productSource, "mobile"), AccountRegistration.mobileAccountRegistration(productSource))
    }



}
