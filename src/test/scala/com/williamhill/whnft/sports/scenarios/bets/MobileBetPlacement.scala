package com.williamhill.whnft.sports.scenarios.bets

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.helpers.UserAgents.setAgentFeedMobileWeb
import com.williamhill.whnft.sports.requestTrafagarObjects.BetSlip.BetType
import com.williamhill.whnft.sports.requestTrafagarObjects._
import io.gatling.core.Predef._
import io.gatling.core.scenario.Simulation
import io.gatling.http.Predef._

object MobileBetPlacement extends Simulation{

  val ualistfeeder = csv("UAMobile.csv")
  val usersfeeder = csv("UserAccounts" + Common.environmentUnderTest.toUpperCase + ".csv")
  val selections = sys.env.get("inPlay") match {
    case Some("true") => csv("InPlaySelections.csv")
    case _ => csv("Selections.csv")
  }
  val multiEventSelections = csv("multi-event-selections.csv")

  val betURL = "mbt_slp"
  val ajaxURL = "mbt_ajax"

  val placeBetMobile =
    scenario("Mobile bet placement")
    .forever {
      exec(session => session.reset)
      .exec(flushHttpCache)
      .exec(flushCookieJar)
      .exec(flushSessionCookies)
      .feed(ualistfeeder.random)
      .feed(usersfeeder.random)
      .feed(selections.queue.circular)
        .exec(session => setBetCode(session))
        .exec(session => setAgentFeedMobileWeb(session))
      .exec(HomePage.get, HomePage.getDataFiles,
        Login.login(2), MbtAjax.getAjaxOpenBets(ajaxURL),
        BetSlip.placeBet(betURL, BetType.Single, Common.numberOfBetsPerUser))
        .repeat(2)(exec(Login.getBalanceSSL)
      )
    }

  val placeDoubleBetMobile =
    scenario("Mobile bet placement for doubles")
      .forever {
        exec(session => session.reset)
          .exec(flushHttpCache)
          .exec(flushCookieJar)
          .exec(flushSessionCookies)
          .feed(ualistfeeder.random)
          .feed(usersfeeder.random)
          .feed(multiEventSelections.queue.circular, 2)
          .exec(session => setBetCode(session))
          .exec(session => setAgentFeedMobileWeb(session))
          .exec(HomePage.get, HomePage.getDataFiles,
            Login.login(2), MbtAjax.getAjaxOpenBets(ajaxURL),
            BetSlip.placeBet(betURL, BetType.Double, Common.numberOfBetsPerUser))
          .repeat(2)(exec(Login.getBalanceSSL)
          )
      }

  val placeTrebleBetMobile =
    scenario("Mobile bet placement for trebles")
      .forever {
        exec(session => session.reset)
          .exec(flushHttpCache)
          .exec(flushCookieJar)
          .exec(flushSessionCookies)
          .feed(ualistfeeder.random)
          .feed(usersfeeder.random)
          .feed(multiEventSelections.queue.circular, 3)
          .exec(session => setBetCode(session))
          .exec(session => setAgentFeedMobileWeb(session))
          .exec(HomePage.get, HomePage.getDataFiles,
            Login.login(2), MbtAjax.getAjaxOpenBets(ajaxURL),
            BetSlip.placeBet(betURL, BetType.Treble, Common.numberOfBetsPerUser))
          .repeat(2)(exec(Login.getBalanceSSL)
          )
      }

  def setBetCode (session : Session) : Session = {
    val userAgent = session("UAStrings").toString()
    var betCode : String = ""

    if (userAgent.contains("iPhone")) {
      betCode = "852"
    } else {
      if (userAgent.contains("iPad")) {
        betCode = "851"
      } else {
        if (userAgent.contains("tab")) {
          betCode = "851"
        } else {
          betCode = "852"
        }
      }
    }
    session.setAll(Map(
      "betCode" -> betCode
    )
    )
  }
}
