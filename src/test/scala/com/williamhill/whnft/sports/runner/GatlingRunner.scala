package com.williamhill.whnft.sports.runner

import com.williamhill.whnft.sports.simulations.{CASLogin, DesktopBetPlacement, NavigateToPages, StreamingTest}
import io.gatling.app.Gatling
import io.gatling.core.config.GatlingPropertiesBuilder

object GatlingRunner {

  def main(args: Array[String]): Unit = {

    // this is where you specify the class you want to run
    val simClass = classOf[NavigateToPages].getName

    val props = new GatlingPropertiesBuilder
    props.simulationClass(simClass)

    Gatling.fromMap(props.build)

  }

}
