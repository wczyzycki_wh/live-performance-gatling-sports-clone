package com.williamhill.whnft.sports.requestOBObjects

import com.williamhill.whnft.sports.helpers.Common
import io.gatling.core.Predef._
import io.gatling.core.structure.ChainBuilder
import io.gatling.http.Predef._

import scala.util.Random

object OBOxiCall {

  val baseURL: String = Common.getConfigFromFile("environment.conf", "OxiUrl")
  val oxiUser: String = Common.getConfigFromFile("environment.conf", "OxiUser")
  val oxiPassword: String = Common.getConfigFromFile("environment.conf", "OxiPassword")
  val debugFlag = System.getProperty("debugFlag")

  val headersEventspartial = Map(
    "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")

  def setVariables(session: Session) = {
    session.set("OxiUser", oxiUser).set("OxiPassword", oxiPassword)
  }


  def reqAccountValidate2  =
    exec(session => {
      setVariables(session)
    }).
      exec(http("reqAccountValidate2")
        .post(baseURL)
        .body(ElFileBody("reqAccountValidate2.xml")).asXml
        .headers(headersEventspartial)
        .check(status.is(200))
        .check(regex(""" <token>(.*)</token>""").find.saveAs("token"))
      )
      .exec(
        session => {
          if ( debugFlag == "on") {
//            println("AccNumber  =" + session.("AccNumber").as[String])
//            println("token  =" + session.get("token").as[String])
          }
          session
        })

  def reqAccountGetDetail  =
    exec(session => {
      setVariables(session)
    }).
      exec(http("reqAccountGetDetail")
        .post(baseURL)
        .body(ElFileBody("reqAccountGetDetail.xml")).asXml
        .headers(headersEventspartial)
        //        .header("ob_authorization",betslip_authorization)
        .check(status.is(200))
        .check(regex(""" <accountId>(.*)</accountId>""").find.saveAs("accountId"))
        .check(bodyString.saveAs("AccresponseBody"))
      )
      .exec(
        session => {
          if ( debugFlag == "on") {
//            println("account No =" + session.get("AccresponseBody").as[String])
          }
          session
        })

  def reqSetAccountStatusFlag(flagName: String, flagStatus: String, flagReason: String): ChainBuilder =
  {
    exec(session =>
      session.setAll(Map(
        "flagName" -> flagName,
        "flagStatus" -> flagStatus,
        "flagReason" -> flagReason
      ))
    )
      .exec(
        http("Set Account Flag")
          .post(baseURL)
          .body(ElFileBody("oxi_reqAccountSetStatusFlag.xml"))
          .check(status is 200)
      )
  }

  def reqAccountAcceptTnC : ChainBuilder =
  {
      exec(session =>
        session.set("channel", Seq("I", "M", "W")(new Random().nextInt(2)))
      )
      .exec(
        http("Accept Terms and Conditions")
          .post(baseURL)
          .body(ElFileBody("oxi_reqAccountAcceptTnC.xml"))
          .check(status is 200)
          .check(xpath("//response/returnStatus/message") is "success")
      )
  }

}
