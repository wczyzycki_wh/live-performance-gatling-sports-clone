package com.williamhill.whnft.sports.requestOBObjects

import com.williamhill.whnft.sports.helpers.Common
import com.williamhill.whnft.sports.helpers.Common.userLoggedIn
import io.gatling.core.Predef._
import io.gatling.http.Predef._
import org.slf4j.LoggerFactory

object LoginOB {

  def timestamp: Long = System.currentTimeMillis / 1000

  val oBbaseurl: String = Common.getConfigFromFile("environment.conf", "OBbaseURL")
  val OBsecurebaseURL: String = Common.getConfigFromFile("environment.conf", "OBsecurebaseURL")
  val securebaseURL: String = Common.getConfigFromFile("environment.conf", "securebaseURL")
  val casLoginURL: String = Common.getConfigFromFile("environment.conf", "casLoginURL")
  val OBAccURL: String = Common.getConfigFromFile("environment.conf", "OBAccURL")

  val headersLoginNewCas = Map(
    "Accept-Encoding" -> "gzip, deflate",
    "Accept" -> "application/json",
    "Content-Type" -> "application/x-www-form-urlencoded",
    "Accept-Language" -> "ql",
    "Origin" -> oBbaseurl,
    "User-Agent" -> "${userAgentCode}")


  val headersLogin = Map(
    "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "Accept-Encoding" -> "gzip, deflate, br",
    "Content-Type" -> "application/x-www-form-urlencoded",
    "User-Agent" -> "${userAgentCode}")

  val headerSSL = Map("Login-Authentication-SSL" -> "${sslLogin}",
    "Host" -> "sports.williamhill.com")

  def login =
    tryMax(2) {
      pause(2, 8)
        .exec(http("Login OB - ${platform}")
          .post(OBsecurebaseURL)
          .formParam("username", "${Username}")
          .formParam("password", "${Password}")
          .formParam("is_ob_form", "1")
          .formParam("action", "DoLoginForm")
          .formParam("login_uid", "${loginUID}")
          .formParam("target_page", oBbaseurl)
          //  .formParam("ioBlackBoxCopy", "")
          .formParam("remember_me_value", "-1")
          .formParam("cas_login_status", "")
          .formParam("cas_service", securebaseURL + "/acc/en-gb?action=HandleServiceTicket")
          .formParam("cas_next", "")
          .formParam("cas_error", "")
          .disableFollowRedirect
          .headers(headersLogin)
          .check(status.in(200, 301))
          .check(headerRegex("Set-Cookie", "cust_ssl_login=([^;]+)").exists.saveAs("sslLogin"))
          .check(headerRegex("Set-Cookie", "cust_auth=([^;]+)").exists.saveAs("custAuth"))
          .check(headerRegex("Set-Cookie", "cust_login=([^;]+)").exists.saveAs("custLogin"))
          .check(headerRegex("Set-Cookie", "CSRF_COOKIE=(.*); path=/").saveAs("csrf_token")))
        //        .exec(http("Get Home")
        //          .get("http://sports.williamhill-pt1.com/bet_gib/en-gb")
        //          .check(regex("AccountID=\"(.*)\"").saveAs("Number"))
        //        )
      .exec(session => {
        LoggerFactory.getLogger("Check for Username: " + session("Username").as[String] + " logged in").info("logging info")
        session
      })
        .doIf(session => !userLoggedIn(session)) {
        exec(session => {
          LoggerFactory.getLogger("Username: " + session("Username").as[String] + " not logged in successfully.").error("logging info")
          session
        })
      }
    }
      .pause(2, 5)

  def loginNewCas =
    exec(addCookie(Cookie("is_cas", "Y").withMaxAge(86400).withDomain(".williamhill.com").withPath("/")))
      .exec(http("GET to CAS")
        .get(casLoginURL)
        .queryParam("service", OBAccURL.replace("_gib", "") + "?action=HandleServiceTicket")
        //        .queryParam("action", "HandleServiceTicket")
        .headers(headersLoginNewCas)
        .check(status.is(200))
        .check(bodyString.saveAs("responseBody"))
        .check(jsonPath("$.form_defaults.lt").saveAs("lt")))
      .pause(1, 5)
      .exec(http("POST to CAS")
        .post(casLoginURL)
        .queryParam("service", OBAccURL.replace("_gib", "") + "?action=HandleServiceTicket")
        //        .queryParam("action", "HandleServiceTicket")
        .formParam("username", "${Username}")
        .formParam("password", "${Password}")
        .formParam("_eventId", "submit")
        .formParam("_rememberUsername", "off")
        .formParam("_rememberMe", "off")
        .formParam("execution", "e1s1")
        .formParam("lt", "${lt}")
        .headers(headersLoginNewCas)
        .check(status.is(200))
        .check(jsonPath("$.serviceTicket").saveAs("serviceTicket"))
        .check(jsonPath("$.next").saveAs("next")))
      .exec(http("POST to Sportsbook")
        .post(oBbaseurl)
        .disableFollowRedirect
        .header(HttpHeaderNames.UserAgent, "${UAStrings}")
        .header(HttpHeaderNames.Accept, HttpHeaderValues.ApplicationXhtml)
        .headers(headersLoginNewCas)
        .queryParam("login_uid", session => timestamp)
        .queryParam("cas_server_url", casLoginURL + "?service=")
        .queryParam("is_ob_form", "1")
        .queryParam("password", "${Password}")
        .queryParam("remember_me_value", "-1")
        .queryParam("tmp_user_name", "Username")
        .queryParam("username", "${Username}")
        .queryParam("cas_login_status", "true")
        .queryParam("cas_next", "${next}")
        .queryParam("cas_error", "")
        .queryParam("action", "DoLogin")
        .queryParam("cas_ticket", "${serviceTicket}")
        .queryParam("target_page", oBbaseurl)
        .queryParam("cas_service", "https://sports.williamhill.com/acc/en-gb?action=HandleServiceTicket")
        .check(status.is(301))
        .check(headerRegex("Set-Cookie", "cas_ssl_login=([^;]+)").exists.saveAs("casSslLogin"))
        .check(headerRegex("Set-Cookie", "cas_login=([^;]+)").exists.saveAs("casLogin"))
        .check(headerRegex("Set-Cookie", "CSRF_COOKIE=(.*); path=/").saveAs("csrf_token")))
      //TODO: Add login check
      //        .exec(http("Get Home")
      //          .get("http://sports.williamhill.com/bet_gib/en-gb")
      //          .check(regex("AccountID=\"(.*)\"").saveAs("Number"))
      //        )
      //        .doIf(session => !userLoggedIn(session)) {
      //          exec(session => {
      //            println("Username: " + session("Username").as[String] + " not logged in successfully.")
      //            session
      //          })
      //        }
      //      })
      .pause(1, 5)

  def loginGetAccountNumber =
    tryMax(2) {
      pause(1, 4)
        .exec(http("Login OB - ${platform}")
          .post(securebaseURL + "/bet_gib/en-gb")
          .formParam("username", "${Username}")
          .formParam("password", "${Password}")
          .formParam("tmp_username", "Username")
          .formParam("action", "DoLogin")
          .formParam("login_uid", "${loginUID}")
          .formParam("target_page", securebaseURL)
          //  .formParam("ioBlackBoxCopy", "")
          .formParam("remember_me_value", "-1")
          .disableFollowRedirect
          .headers(headersLogin)
          .check(status.is(301))
          .check(headerRegex("Set-Cookie", "cust_ssl_login=([^;]+)").exists.saveAs("sslLogin"))
          .check(headerRegex("Set-Cookie", "cust_auth=([^;]+)").exists.saveAs("custAuth"))
          .check(headerRegex("Set-Cookie", "cust_login=([^;]+)").exists.saveAs("custLogin"))
          .check(headerRegex("Set-Cookie", "CSRF_COOKIE=(.*); path=/").saveAs("csrf_token")))
        .exec(http("Get Home")
          .get(oBbaseurl)
          .disableFollowRedirect
          .check(regex("AccountID=\"(.*)\"").saveAs("Number"))
        )
    }
      .exec(session => {
        LoggerFactory.getLogger("Account used: " + session("Username").as[String]).info("logging info")
        session
      })
      .pause(1, 4)

  def freebetsCall =
    exec(flushSessionCookies)
      .exec(addCookie(Cookie("cust_login", "${custLogin}")))
      .exec(addCookie(Cookie("cust_auth", "${custAuth}")))
      .exec(addCookie(Cookie("cust_ssl_login", "${sslLogin}")))
      .exec(addCookie(Cookie("CSRF_COOKIE", "${csrf_token}")))
      .exec(http("FreeBets bet boost API call")
        .post(securebaseURL + "/freebets/qualification/freebets/tokens")
        .queryParam("triggerType", "ACTIVEUSER")
        .queryParam("channel", "W")
        .queryParam("accountNo", "${Number}")
        .headers(headerSSL)
        .check(status.is(202))
      )
      .pause(1, 3)

}