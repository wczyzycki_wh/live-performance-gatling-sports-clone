package com.williamhill.whnft.sports.requestOBObjects

import com.williamhill.whnft.sports.helpers.Common
import io.gatling.core.Predef._
import io.gatling.http.Predef._


/**
  * Update by Juri Boiko on 21/12/2018.
  */
object BetSlipCashInOB {

  val baseURL: String = Common.getConfigFromFile("environment.conf", "baseURL")
  val securebaseURL: String = Common.getConfigFromFile("environment.conf", "securebaseURL")


  def placeBet(betURL: String, numberOfBets: Int, ajaxURL: String) =
    repeat(numberOfBets) {
      exec(session => {
        if (!session.contains("csrf_token"))
          session.set("csrf_token", "empty")
        else session
      })
        .exec(flushSessionCookies)
        .exec(flushHttpCache)
        .exec(flushCookieJar)
        .exec(
          getAjaxGoBets(ajaxURL),
          getAjaxOpenBets(ajaxURL),
          cashoutPlacedBet(betURL)
        )
    }

  def getAjaxOpenBets (ajaxURL: String) =
    tryMax(3) {
      exec(flushSessionCookies)
        .exec(addCookie(Cookie("cust_login", "${custLogin}")))
        .exec(addCookie(Cookie("cust_auth", "${custAuth}")))
        .exec(addCookie(Cookie("CSRF_COOKIE", "${csrf_token}")))
        .exec(http("Ajax Open bets - ${platform}")
          .post(baseURL + "/" + ajaxURL + "/en-gb")
          .formParam("action", "GoOpenBets")
          .formParam("blockbuster_id", "-1")
          .formParam("csrf_token", "${csrf_token}")
          .check(status.is(200))
          .check(regex("\"bet_id\": \"(.*?)\"").saveAs("bet_id"))
          .check(regex("\"cr_date\": \"(.*?)\"").saveAs("cr_date"))
          .check(regex("\"num_open_bets\": \"(.*?)\"").saveAs("open_bets_num_bets"))
          .check(regex("\"cashout_value\": \"(.*?)\"").saveAs("cashoutValue"))
        )
        .pause(1, 5)
    }

  def getAjaxGoBets(ajaxURL: String) =
    exec(flushSessionCookies)
      .exec(addCookie(Cookie("cust_login", "${custLogin}")))
      .exec(addCookie(Cookie("cust_auth", "${custAuth}")))
      .exec(addCookie(Cookie("CSRF_COOKIE", "${csrf_token}")))
      .exec(http("Ajax Go bets - ${platform}")
        .post(baseURL + "/" + ajaxURL + "/en-gb")
        .formParam("action", "GoBets")
        .formParam("blockbuster", "-1")
        .formParam("csrf_token", "${csrf_token}")
        .check(status.is(200))
        .check(headerRegex("Set-Cookie", "CSRF_COOKIE=([^;]+)").exists.saveAs("csrf_token"))
      )
      .pause(1, 5)


  def cashoutPlacedBet(betURL: String) =
    tryMax(3) {
      exec(flushSessionCookies)
        .exec(addCookie(Cookie("cust_login", "${custLogin}")))
        .exec(addCookie(Cookie("cust_ssl_login", "${sslLogin}")))
        .exec(addCookie(Cookie("cust_auth", "${custAuth}")))
        .exec(addCookie(Cookie("CSRF_COOKIE", "${csrf_token}")))
        .exec(http("Cashout Placed Bet - ${platform}")
          .post(securebaseURL + "/" + betURL + "/en-gb")
          .formParam("action", "DoCashOutBet")
          .formParam("bet_id", "${bet_id}")
          .formParam("cashout_price", "${cashoutValue}")
          .formParam("target_page", baseURL + "/" + betURL + "/en-gb?action=PlayIfrOpenBets")
          .formParam("csrf_token", "${csrf_token}")
          .formParam("partialCashout", "")
          .check(status.in(200, 301))
          .check(headerRegex("Set-Cookie", "CSRF_COOKIE=([^;]+)").exists.saveAs("csrf_token"))
        )
    }
      .pause(1, 5)

}