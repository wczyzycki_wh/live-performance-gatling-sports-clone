package com.williamhill.whnft.sports.requestOBObjects

import com.williamhill.whnft.sports.helpers.Common
import io.gatling.core.Predef._
import io.gatling.http.Predef._

/**
 * Update by Juri Boiko on 21/12/2018.
 */
object OBPageViews {

  val headers = Map(
    "Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
    "User-Agent" -> "${userAgentCode}",
    "Accept-Encoding" -> "gzip, deflate, sdch")

  val OBFootballUrl: String = Common.getConfigFromFile("environment.conf", "OBFootballURL")
  val OBTennisUrl: String = Common.getConfigFromFile("environment.conf", "OBTennisURL")
  val OBHorseUrl: String = Common.getConfigFromFile("environment.conf", "OBHorseURL")

  def clickFootball =
    exec(http("OB - open Football Highlights - ${platform}")
    .get(OBFootballUrl)
    .headers(headers)
    .check(status.is(200)))
    .pause(1,5)

  def clickTennis =
    exec(http("OB - open Tennis Highlights - ${platform}")
    .get(OBTennisUrl)
    .headers(headers)
    .check(status.is(200))
    .check(regex("(Tennis|tennis)").findAll.exists))
    .pause(1,5)

  def clickHorse =
    exec(http("OB - open Horse Highlights - ${platform}")
    .get(OBHorseUrl)
    .headers(headers)
    .check(status.is(200))
    .check(regex("(Horse|horse)").findAll.exists))
    .pause(1,5)
}